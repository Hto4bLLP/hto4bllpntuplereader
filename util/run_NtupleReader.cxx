#include "NtupleReader.h"
#include "Tools/Params.h"
#include "Tools/MCConfig.h"
#include "util.h"
#include <TProof.h>
#include <TDSet.h>

#include <boost/program_options.hpp>
#include <vector>
#include <fstream>
#include <wordexp.h>
#include <cstdio>

#include <TH1.h>
#include <TFile.h>
#include <TFileCollection.h>
#include <TChain.h>
#include <TSelector.h>
#include <TList.h>

namespace po = boost::program_options;

int main(int argc, char* argv[]) {

    std::string output_file;
    std::string input_files;
    bool isMC     = false;
    bool doTruth  = false;
    bool doSkim   = false;
    bool doPhotonSkim   = false;
    bool unblind  = false;
    bool useProof = false;
   
    try
    {
        po::options_description desc("Allowed options");
        desc.add_options()
            ("help"          , "produce help message")
            ("output"        , po::value(&output_file)    , "output root file")
            ("input_files"   , po::value(&input_files)    , "path to text file with samples")
            ("isMC,f"        , po::bool_switch(&isMC)     , "flag for running over MC")
            ("doTruth,f"     , po::bool_switch(&doTruth)   , "flag for saving jet truth info")
            ("doSkim,f"      , po::bool_switch(&doSkim)   , "flag for skimming by trig/filter")
            ("doPhotonSkim,f", po::bool_switch(&doPhotonSkim)   , "flag for skimming by photon selection")
            ("useProof,f"    , po::bool_switch(&useProof)  , "flag for turning on proof")
            ("unblind,f"     , po::bool_switch(&unblind)  , "flag for unblinding data")
            ;

        po::variables_map vm;
        po::store(parse_command_line(argc, argv, desc), vm);

        if (argc == 1 or vm.count("help")) {
            std::cout << desc << "\n";
            return EXIT_SUCCESS;
        }

        po::notify(vm);
    }
    catch(std::exception& e) {
        util::FATAL_ERROR(e.what());
    }

    if(isMC) unblind = true;


    util::MSG("run_NtupleMaker","INFO","Loading Params...");
    Params* run_params = new Params("m_params",output_file,isMC,doTruth,doSkim,doPhotonSkim,useProof,unblind);
    util::MSG("run_NtupleMaker","INFO","Finished loading Params. Good to go!");
    MCConfig* mc_db = run_params->GetMCConfig();

    int mcChannelNumber = 0;
    std::ifstream in{input_files};
    std::string input;
    if(isMC) {
      while (in >> input) {
        TFile* input_file = TFile::Open(input.c_str());
        TH1D* metadata = dynamic_cast<TH1D*>(input_file->Get("MetaData_EventCount"));

        TTree* ntuple = dynamic_cast<TTree*>(input_file->Get("outTree"));
        ntuple->SetBranchStatus("*", 0);
        ntuple->SetBranchStatus("mcChannelNumber", 1);
        ntuple->SetBranchAddress("mcChannelNumber", &mcChannelNumber);
        ntuple->GetEntry(0);
        std::cout << mcChannelNumber << std::endl;
        (*mc_db)[mcChannelNumber].generated_raw += metadata->GetBinContent(1);
        (*mc_db)[mcChannelNumber].generated_weighted += metadata->GetBinContent(3);
        delete metadata;
        delete ntuple;
        delete input_file;
      }
    }

    // update preprocessor variable definition
    // (a bit hack-y, maybe a better way?)
    wordexp_t wexpOld{};
    wordexp_t wexpNew{};
    wordexp("$SrcDir/hto4bllpntuplereader/NtupleReader.h", &wexpOld, WRDE_SHOWERR | WRDE_UNDEF | WRDE_NOCMD);
    wordexp("$SrcDir/hto4bllpntuplereader/NtupleReaderNew.h", &wexpNew, WRDE_SHOWERR | WRDE_UNDEF | WRDE_NOCMD);
    std::ifstream nominal;
    std::ofstream update;
    nominal.open(wexpOld.we_wordv[0]);
    update.open(wexpNew.we_wordv[0]);
    std::string tmp;
    while(std::getline(nominal,tmp)) {
      if(isMC and tmp == "#undef IS_MC") {
        tmp = "#define IS_MC";
      }
      else if(!isMC and tmp == "#define IS_MC") {
        tmp = "#undef IS_MC";
      }
      if(doTruth and tmp == "#undef DO_TRUTH") {
        tmp = "#define DO_TRUTH";
      }
      else if(!doTruth and tmp == "#define DO_TRUTH") {
        tmp = "#undef DO_TRUTH";
      }
      if(doPhotonSkim and tmp == "#undef DO_PHOTON") {
        tmp = "#define DO_PHOTON";
      }
      else if(!doPhotonSkim and tmp == "#define DO_PHOTON") {
        tmp = "#undef DO_PHOTON";
      }
      update << tmp << std::endl;
    }
    nominal.close();
    update.close();
    std::rename(wexpNew.we_wordv[0], wexpOld.we_wordv[0]);
    
    wordexp("$SrcDir/hto4bllpntuplereader/NtupleReader.C", &wexpOld, WRDE_SHOWERR | WRDE_UNDEF | WRDE_NOCMD);
    wordexp("$SrcDir/hto4bllpntuplereader/NtupleReaderNew.C", &wexpNew, WRDE_SHOWERR | WRDE_UNDEF | WRDE_NOCMD);
    nominal.open(wexpOld.we_wordv[0]);
    update.open(wexpNew.we_wordv[0]);
    while(std::getline(nominal,tmp)) {
      if(doTruth and tmp == "#undef DO_TRUTH") {
        tmp = "#define DO_TRUTH";
      }
      else if(!doTruth and tmp == "#define DO_TRUTH") {
        tmp = "#undef DO_TRUTH";
      }
      if(doPhotonSkim and tmp == "#undef DO_PHOTON") {
        tmp = "#define DO_PHOTON";
      }
      else if(!doPhotonSkim and tmp == "#define DO_PHOTON") {
        tmp = "#undef DO_PHOTON";
      }
      update << tmp << std::endl;
    }
    nominal.close();
    update.close();
    std::rename(wexpNew.we_wordv[0], wexpOld.we_wordv[0]);
    
    util::MSG("run_NtupleMaker","INFO","Loading TSelector...");

    // Gather the input files
    TFileCollection* files = new TFileCollection("samples", "", input_files.c_str());

    if(useProof) {
      auto dset = new TDSet("TTree", "outTree");
      dset->Add(files->GetList());
      auto fProof = TProof::Open("workers=20");
      fProof->SetProgressDialog(kFALSE);
      fProof->AddInput(run_params);
      fProof->Process(dset,"$SrcDir/hto4bllpntuplereader/NtupleReader.C+");
    }
    else {
      TChain* chain = new TChain("outTree");
      chain->AddFileInfoList(files->GetList());
      TSelector* selector = TSelector::GetSelector("$SrcDir/hto4bllpntuplereader/NtupleReader.C+");
      TList* input_list = new TList();
      input_list->Add(run_params);
      selector->SetInputList(input_list);
      chain->Process(selector);
      delete selector;
      delete chain;
      delete input_list;
    }

    delete mc_db;
    delete files;
    delete run_params;

    return EXIT_SUCCESS;

}
