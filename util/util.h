#ifndef util_h
#define util_h

#include <iostream>
#include <iomanip>

namespace util {
  inline void MSG(std::string func, std::string level, std::string msg) {
      std::cout << std::left << std::setw(25) << func
                << std::left << std::setw(10)  << level
                << std::left << std::setw(20) << msg << std::endl;
  }
  inline void FATAL_ERROR(const std::string& msg) {
    std::cout << "ERROR: " << msg << std::endl;
    exit(EXIT_FAILURE);
  }
}

#endif // util_h
