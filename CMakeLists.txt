################################################################################
# Package: Hto4bLLP
################################################################################

# Declare the package name:
atlas_subdir( NtupleReader )

# Declare the package's dependencies:
atlas_depends_on_subdirs( PUBLIC
                          Control/AthToolSupport/AsgTools
                          Control/xAODRootAccess
                          Event/xAOD/xAODEventInfo
                          Event/xAOD/xAODJet
                          Event/xAOD/xAODMissingET
                          PhysicsAnalysis/D3PDTools/EventLoop
                          PhysicsAnalysis/D3PDTools/RootCoreUtils
                          PRIVATE
                          Event/xAOD/xAODEgamma
                          Event/xAOD/xAODMuon )

# external dependencies
find_package( ROOT COMPONENTS Core RIO Hist Tree )

# build a dictionary for the library
atlas_add_root_dictionary ( NtupleReaderLib NtupleReaderDictSource
                            ROOT_HEADERS Tools/*.h LinkDef.h
                            EXTERNAL_PACKAGES ROOT )

# Component(s) in the package:
atlas_add_library( NtupleReaderLib Tools/*.h Tools/*.cxx ${NtupleReaderDictSource}
  PUBLIC_HEADERS Tools
  INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
  LINK_LIBRARIES ${ROOT_LIBRARIES}
  AsgTools
  EventLoop
  xAODEgamma
  xAODEventInfo
  xAODJet
  xAODMissingET
  xAODMuon
  xAODRootAccess )

target_compile_features(NtupleReaderLib PUBLIC cxx_std_17)
atlas_add_executable(run_NtupleReader  util/run_NtupleReader.cxx
  LINK_LIBRARIES NtupleReaderLib 
  )
