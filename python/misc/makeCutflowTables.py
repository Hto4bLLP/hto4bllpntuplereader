from ROOT import TFile

ZHa55_10 = TFile("/data/jburzyns/NtupleReader/ZH_a55a55_4b_ctau10.root")
ZHa35_10 = TFile("/data/jburzyns/NtupleReader/ZH_a35a35_4b_ctau10.root")
ZHa25_10 = TFile("/data/jburzyns/NtupleReader/ZH_a25a25_4b_ctau10.root")
ZHa15_10 = TFile("/data/jburzyns/NtupleReader/ZH_a15a15_4b_ctau10.root")
ZHa55_100 = TFile("/data/jburzyns/NtupleReader/ZH_a55a55_4b_ctau100.root")
ZHa35_100 = TFile("/data/jburzyns/NtupleReader/ZH_a35a35_4b_ctau100.root")
ZHa25_100 = TFile("/data/jburzyns/NtupleReader/ZH_a25a25_4b_ctau100.root")
ZHa15_100 = TFile("/data/jburzyns/NtupleReader/ZH_a15a15_4b_ctau100.root")
ZHa55_1000 = TFile("/data/jburzyns/NtupleReader/ZH_a55a55_4b_ctau1000.root")
ZHa35_1000 = TFile("/data/jburzyns/NtupleReader/ZH_a35a35_4b_ctau1000.root")
ZHa25_1000 = TFile("/data/jburzyns/NtupleReader/ZH_a25a25_4b_ctau1000.root")
ZHa15_1000 = TFile("/data/jburzyns/NtupleReader/ZH_a15a15_4b_ctau1000.root")

h_ZHa55_10 = ZHa55_10.Get("cutflow")
h_ZHa35_10 = ZHa35_10.Get("cutflow")
h_ZHa25_10 = ZHa25_10.Get("cutflow")
h_ZHa15_10 = ZHa15_10.Get("cutflow")

h_ZHa55_100 = ZHa55_100.Get("cutflow")
h_ZHa35_100 = ZHa35_100.Get("cutflow")
h_ZHa25_100 = ZHa25_100.Get("cutflow")
h_ZHa15_100 = ZHa15_100.Get("cutflow")

h_ZHa55_1000 = ZHa55_1000.Get("cutflow")
h_ZHa35_1000 = ZHa35_1000.Get("cutflow")
h_ZHa25_1000 = ZHa25_1000.Get("cutflow")
h_ZHa15_1000 = ZHa15_1000.Get("cutflow")

h_ZHa55_10_weighted   = ZHa55_10.Get  ("cutflow_weighted")
h_ZHa35_10_weighted   = ZHa35_10.Get  ("cutflow_weighted")
h_ZHa25_10_weighted   = ZHa25_10.Get  ("cutflow_weighted")
h_ZHa15_10_weighted   = ZHa15_10.Get  ("cutflow_weighted")
h_ZHa55_100_weighted  = ZHa55_100.Get ("cutflow_weighted")
h_ZHa35_100_weighted  = ZHa35_100.Get ("cutflow_weighted")
h_ZHa25_100_weighted  = ZHa25_100.Get ("cutflow_weighted")
h_ZHa15_100_weighted  = ZHa15_100.Get ("cutflow_weighted")
h_ZHa55_1000_weighted = ZHa55_1000.Get("cutflow_weighted")
h_ZHa35_1000_weighted = ZHa35_1000.Get("cutflow_weighted")
h_ZHa25_1000_weighted = ZHa25_1000.Get("cutflow_weighted")
h_ZHa15_1000_weighted = ZHa15_1000.Get("cutflow_weighted")


with open("cutflow.tex","w+") as f: 
  f.write("\\begin{table}[h]\n")
  f.write("\\centering\n")
  f.write("  \\begin{tabular}{ c c | c | c c c c c c c }\n")
  f.write("  \\hline\n")
  f.write("  \\hline\n")
  f.write("  \multicolumn{2}{c}{Sample} &  & \multicolumn{7}{c}{Selection} \\\\\n")
  f.write("  \\hline\n")
  f.write("  $m_a$ [GeV] & $c\\tau$ [mm] & Efficiency & All events & Trigger & Filter & Presel. & $p_{\\text{T},Z}$ & 1 DV & 2 DV\\\\\n")
  f.write("  \\hline\n")
  f.write("  \\multirow{6}{*}{55} & \\multirow{2}{*}{10} & Total    & %.4f & %.4f & %.4f & %.4f & %.4f & %.4f & %.4f \\\\\n" % 
          (h_ZHa55_10_weighted.GetBinContent(1)/h_ZHa55_10_weighted.GetBinContent(1),
           h_ZHa55_10_weighted.GetBinContent(2)/h_ZHa55_10_weighted.GetBinContent(1),
           h_ZHa55_10_weighted.GetBinContent(3)/h_ZHa55_10_weighted.GetBinContent(1),
           h_ZHa55_10_weighted.GetBinContent(4)/h_ZHa55_10_weighted.GetBinContent(1),
           h_ZHa55_10_weighted.GetBinContent(5)/h_ZHa55_10_weighted.GetBinContent(1),
           h_ZHa55_10_weighted.GetBinContent(6)/h_ZHa55_10_weighted.GetBinContent(1),
           h_ZHa55_10_weighted.GetBinContent(7)/h_ZHa55_10_weighted.GetBinContent(1)))
  f.write("                      &                     & Relative & %.4f & %.4f & %.4f & %.4f & %.4f & %.4f & %.4f \\\\\n" % 
           (h_ZHa55_10_weighted.GetBinContent(1)/h_ZHa55_10_weighted.GetBinContent(1),
            h_ZHa55_10_weighted.GetBinContent(2)/h_ZHa55_10_weighted.GetBinContent(1),
            h_ZHa55_10_weighted.GetBinContent(3)/h_ZHa55_10_weighted.GetBinContent(2),
            h_ZHa55_10_weighted.GetBinContent(4)/h_ZHa55_10_weighted.GetBinContent(3),
            h_ZHa55_10_weighted.GetBinContent(5)/h_ZHa55_10_weighted.GetBinContent(4),
            h_ZHa55_10_weighted.GetBinContent(6)/h_ZHa55_10_weighted.GetBinContent(5),
            h_ZHa55_10_weighted.GetBinContent(7)/h_ZHa55_10_weighted.GetBinContent(6)))
  f.write("                      \\cline{2-10}\n")
  f.write("                      & \\multirow{2}{*}{100} & Total    & %.4f & %.4f & %.4f & %.4f & %.4f & %.4f & %.4f \\\\\n" % 
           (h_ZHa55_100_weighted.GetBinContent(1)/h_ZHa55_100_weighted.GetBinContent(1),
            h_ZHa55_100_weighted.GetBinContent(2)/h_ZHa55_100_weighted.GetBinContent(1),
            h_ZHa55_100_weighted.GetBinContent(3)/h_ZHa55_100_weighted.GetBinContent(1),
            h_ZHa55_100_weighted.GetBinContent(4)/h_ZHa55_100_weighted.GetBinContent(1),
            h_ZHa55_100_weighted.GetBinContent(5)/h_ZHa55_100_weighted.GetBinContent(1),
            h_ZHa55_100_weighted.GetBinContent(6)/h_ZHa55_100_weighted.GetBinContent(1),
            h_ZHa55_100_weighted.GetBinContent(7)/h_ZHa55_100_weighted.GetBinContent(1)))
  f.write("                      &                     & Relative & %.4f & %.4f & %.4f & %.4f & %.4f & %.4f & %.4f \\\\\n" %  
           (h_ZHa55_100_weighted.GetBinContent(1)/h_ZHa55_100_weighted.GetBinContent(1),
            h_ZHa55_100_weighted.GetBinContent(2)/h_ZHa55_100_weighted.GetBinContent(1),
            h_ZHa55_100_weighted.GetBinContent(3)/h_ZHa55_100_weighted.GetBinContent(2),
            h_ZHa55_100_weighted.GetBinContent(4)/h_ZHa55_100_weighted.GetBinContent(3),
            h_ZHa55_100_weighted.GetBinContent(5)/h_ZHa55_100_weighted.GetBinContent(4),
            h_ZHa55_100_weighted.GetBinContent(6)/h_ZHa55_100_weighted.GetBinContent(5),
            h_ZHa55_100_weighted.GetBinContent(7)/h_ZHa55_100_weighted.GetBinContent(6)))
  f.write("                      \\cline{2-10}\n")
  f.write("                      & \\multirow{2}{*}{1000} & Total    & %.4f & %.4f & %.4f & %.4f & %.4f & %.4f & %.4f \\\\\n" % 
           (h_ZHa55_1000_weighted.GetBinContent(1)/h_ZHa55_1000_weighted.GetBinContent(1),
            h_ZHa55_1000_weighted.GetBinContent(2)/h_ZHa55_1000_weighted.GetBinContent(1),
            h_ZHa55_1000_weighted.GetBinContent(3)/h_ZHa55_1000_weighted.GetBinContent(1),
            h_ZHa55_1000_weighted.GetBinContent(4)/h_ZHa55_1000_weighted.GetBinContent(1),
            h_ZHa55_1000_weighted.GetBinContent(5)/h_ZHa55_1000_weighted.GetBinContent(1),
            h_ZHa55_1000_weighted.GetBinContent(6)/h_ZHa55_1000_weighted.GetBinContent(1),
            h_ZHa55_1000_weighted.GetBinContent(7)/h_ZHa55_1000_weighted.GetBinContent(1)))
  f.write("                      &                     & Relative & %.4f & %.4f & %.4f & %.4f & %.4f & %.4f & %.4f \\\\\n" %  
           (h_ZHa55_1000_weighted.GetBinContent(1)/h_ZHa55_1000_weighted.GetBinContent(1),
            h_ZHa55_1000_weighted.GetBinContent(2)/h_ZHa55_1000_weighted.GetBinContent(1),
            h_ZHa55_1000_weighted.GetBinContent(3)/h_ZHa55_1000_weighted.GetBinContent(2),
            h_ZHa55_1000_weighted.GetBinContent(4)/h_ZHa55_1000_weighted.GetBinContent(3),
            h_ZHa55_1000_weighted.GetBinContent(5)/h_ZHa55_1000_weighted.GetBinContent(4),
            h_ZHa55_1000_weighted.GetBinContent(6)/h_ZHa55_1000_weighted.GetBinContent(5),
            h_ZHa55_1000_weighted.GetBinContent(7)/h_ZHa55_1000_weighted.GetBinContent(6)))
  f.write("  \\hline\n")
  f.write("  \\multirow{6}{*}{35} & \\multirow{2}{*}{10} & Total    & %.4f & %.4f & %.4f & %.4f & %.4f & %.4f & %.4f \\\\\n" % 
          (h_ZHa35_10_weighted.GetBinContent(1)/h_ZHa35_10_weighted.GetBinContent(1),
           h_ZHa35_10_weighted.GetBinContent(2)/h_ZHa35_10_weighted.GetBinContent(1),
           h_ZHa35_10_weighted.GetBinContent(3)/h_ZHa35_10_weighted.GetBinContent(1),
           h_ZHa35_10_weighted.GetBinContent(4)/h_ZHa35_10_weighted.GetBinContent(1),
           h_ZHa35_10_weighted.GetBinContent(5)/h_ZHa35_10_weighted.GetBinContent(1),
           h_ZHa35_10_weighted.GetBinContent(6)/h_ZHa35_10_weighted.GetBinContent(1),
           h_ZHa35_10_weighted.GetBinContent(7)/h_ZHa35_10_weighted.GetBinContent(1)))
  f.write("                      &                     & Relative & %.4f & %.4f & %.4f & %.4f & %.4f & %.4f & %.4f \\\\\n" % 
           (h_ZHa35_10_weighted.GetBinContent(1)/h_ZHa35_10_weighted.GetBinContent(1),
            h_ZHa35_10_weighted.GetBinContent(2)/h_ZHa35_10_weighted.GetBinContent(1),
            h_ZHa35_10_weighted.GetBinContent(3)/h_ZHa35_10_weighted.GetBinContent(2),
            h_ZHa35_10_weighted.GetBinContent(4)/h_ZHa35_10_weighted.GetBinContent(3),
            h_ZHa35_10_weighted.GetBinContent(5)/h_ZHa35_10_weighted.GetBinContent(4),
            h_ZHa35_10_weighted.GetBinContent(6)/h_ZHa35_10_weighted.GetBinContent(5),
            h_ZHa35_10_weighted.GetBinContent(7)/h_ZHa35_10_weighted.GetBinContent(6)))
  f.write("                      \\cline{2-10}\n")
  f.write("                      & \\multirow{2}{*}{100} & Total    & %.4f & %.4f & %.4f & %.4f & %.4f & %.4f & %.4f \\\\\n" % 
           (h_ZHa35_100_weighted.GetBinContent(1)/h_ZHa35_100_weighted.GetBinContent(1),
            h_ZHa35_100_weighted.GetBinContent(2)/h_ZHa35_100_weighted.GetBinContent(1),
            h_ZHa35_100_weighted.GetBinContent(3)/h_ZHa35_100_weighted.GetBinContent(1),
            h_ZHa35_100_weighted.GetBinContent(4)/h_ZHa35_100_weighted.GetBinContent(1),
            h_ZHa35_100_weighted.GetBinContent(5)/h_ZHa35_100_weighted.GetBinContent(1),
            h_ZHa35_100_weighted.GetBinContent(6)/h_ZHa35_100_weighted.GetBinContent(1),
            h_ZHa35_100_weighted.GetBinContent(7)/h_ZHa35_100_weighted.GetBinContent(1)))
  f.write("                      &                     & Relative & %.4f & %.4f & %.4f & %.4f & %.4f & %.4f & %.4f \\\\\n" %  
           (h_ZHa35_100_weighted.GetBinContent(1)/h_ZHa35_100_weighted.GetBinContent(1),
            h_ZHa35_100_weighted.GetBinContent(2)/h_ZHa35_100_weighted.GetBinContent(1),
            h_ZHa35_100_weighted.GetBinContent(3)/h_ZHa35_100_weighted.GetBinContent(2),
            h_ZHa35_100_weighted.GetBinContent(4)/h_ZHa35_100_weighted.GetBinContent(3),
            h_ZHa35_100_weighted.GetBinContent(5)/h_ZHa35_100_weighted.GetBinContent(4),
            h_ZHa35_100_weighted.GetBinContent(6)/h_ZHa35_100_weighted.GetBinContent(5),
            h_ZHa35_100_weighted.GetBinContent(7)/h_ZHa35_100_weighted.GetBinContent(6)))
  f.write("                      \\cline{2-10}\n")
  f.write("                      & \\multirow{2}{*}{1000} & Total    & %.4f & %.4f & %.4f & %.4f & %.4f & %.4f & %.4f \\\\\n" % 
           (h_ZHa35_1000_weighted.GetBinContent(1)/h_ZHa35_1000_weighted.GetBinContent(1),
            h_ZHa35_1000_weighted.GetBinContent(2)/h_ZHa35_1000_weighted.GetBinContent(1),
            h_ZHa35_1000_weighted.GetBinContent(3)/h_ZHa35_1000_weighted.GetBinContent(1),
            h_ZHa35_1000_weighted.GetBinContent(4)/h_ZHa35_1000_weighted.GetBinContent(1),
            h_ZHa35_1000_weighted.GetBinContent(5)/h_ZHa35_1000_weighted.GetBinContent(1),
            h_ZHa35_1000_weighted.GetBinContent(6)/h_ZHa35_1000_weighted.GetBinContent(1),
            h_ZHa35_1000_weighted.GetBinContent(7)/h_ZHa35_1000_weighted.GetBinContent(1)))
  f.write("                      &                     & Relative & %.4f & %.4f & %.4f & %.4f & %.4f & %.4f & %.4f \\\\\n" %  
           (h_ZHa35_1000_weighted.GetBinContent(1)/h_ZHa35_1000_weighted.GetBinContent(1),
            h_ZHa35_1000_weighted.GetBinContent(2)/h_ZHa35_1000_weighted.GetBinContent(1),
            h_ZHa35_1000_weighted.GetBinContent(3)/h_ZHa35_1000_weighted.GetBinContent(2),
            h_ZHa35_1000_weighted.GetBinContent(4)/h_ZHa35_1000_weighted.GetBinContent(3),
            h_ZHa35_1000_weighted.GetBinContent(5)/h_ZHa35_1000_weighted.GetBinContent(4),
            h_ZHa35_1000_weighted.GetBinContent(6)/h_ZHa35_1000_weighted.GetBinContent(5),
            h_ZHa35_1000_weighted.GetBinContent(7)/h_ZHa35_1000_weighted.GetBinContent(6)))
  f.write("  \\hline\n")
  f.write("  \\multirow{6}{*}{25} & \\multirow{2}{*}{10} & Total    & %.4f & %.4f & %.4f & %.4f & %.4f & %.4f & %.4f \\\\\n" % 
          (h_ZHa25_10_weighted.GetBinContent(1)/h_ZHa25_10_weighted.GetBinContent(1),
           h_ZHa25_10_weighted.GetBinContent(2)/h_ZHa25_10_weighted.GetBinContent(1),
           h_ZHa25_10_weighted.GetBinContent(3)/h_ZHa25_10_weighted.GetBinContent(1),
           h_ZHa25_10_weighted.GetBinContent(4)/h_ZHa25_10_weighted.GetBinContent(1),
           h_ZHa25_10_weighted.GetBinContent(5)/h_ZHa25_10_weighted.GetBinContent(1),
           h_ZHa25_10_weighted.GetBinContent(6)/h_ZHa25_10_weighted.GetBinContent(1),
           h_ZHa25_10_weighted.GetBinContent(7)/h_ZHa25_10_weighted.GetBinContent(1)))
  f.write("                      &                     & Relative & %.4f & %.4f & %.4f & %.4f & %.4f & %.4f & %.4f \\\\\n" % 
           (h_ZHa25_10_weighted.GetBinContent(1)/h_ZHa25_10_weighted.GetBinContent(1),
            h_ZHa25_10_weighted.GetBinContent(2)/h_ZHa25_10_weighted.GetBinContent(1),
            h_ZHa25_10_weighted.GetBinContent(3)/h_ZHa25_10_weighted.GetBinContent(2),
            h_ZHa25_10_weighted.GetBinContent(4)/h_ZHa25_10_weighted.GetBinContent(3),
            h_ZHa25_10_weighted.GetBinContent(5)/h_ZHa25_10_weighted.GetBinContent(4),
            h_ZHa25_10_weighted.GetBinContent(6)/h_ZHa25_10_weighted.GetBinContent(5),
            h_ZHa25_10_weighted.GetBinContent(7)/h_ZHa25_10_weighted.GetBinContent(6)))
  f.write("                      \\cline{2-10}\n")
  f.write("                      & \\multirow{2}{*}{100} & Total    & %.4f & %.4f & %.4f & %.4f & %.4f & %.4f & %.4f \\\\\n" % 
           (h_ZHa25_100_weighted.GetBinContent(1)/h_ZHa25_100_weighted.GetBinContent(1),
            h_ZHa25_100_weighted.GetBinContent(2)/h_ZHa25_100_weighted.GetBinContent(1),
            h_ZHa25_100_weighted.GetBinContent(3)/h_ZHa25_100_weighted.GetBinContent(1),
            h_ZHa25_100_weighted.GetBinContent(4)/h_ZHa25_100_weighted.GetBinContent(1),
            h_ZHa25_100_weighted.GetBinContent(5)/h_ZHa25_100_weighted.GetBinContent(1),
            h_ZHa25_100_weighted.GetBinContent(6)/h_ZHa25_100_weighted.GetBinContent(1),
            h_ZHa25_100_weighted.GetBinContent(7)/h_ZHa25_100_weighted.GetBinContent(1)))
  f.write("                      &                     & Relative & %.4f & %.4f & %.4f & %.4f & %.4f & %.4f & %.4f \\\\\n" %  
           (h_ZHa25_100_weighted.GetBinContent(1)/h_ZHa25_100_weighted.GetBinContent(1),
            h_ZHa25_100_weighted.GetBinContent(2)/h_ZHa25_100_weighted.GetBinContent(1),
            h_ZHa25_100_weighted.GetBinContent(3)/h_ZHa25_100_weighted.GetBinContent(2),
            h_ZHa25_100_weighted.GetBinContent(4)/h_ZHa25_100_weighted.GetBinContent(3),
            h_ZHa25_100_weighted.GetBinContent(5)/h_ZHa25_100_weighted.GetBinContent(4),
            h_ZHa25_100_weighted.GetBinContent(6)/h_ZHa25_100_weighted.GetBinContent(5),
            h_ZHa25_100_weighted.GetBinContent(7)/h_ZHa25_100_weighted.GetBinContent(6)))
  f.write("                      \\cline{2-10}\n")
  f.write("                      & \\multirow{2}{*}{1000} & Total    & %.4f & %.4f & %.4f & %.4f & %.4f & %.4f & %.4f \\\\\n" % 
           (h_ZHa25_1000_weighted.GetBinContent(1)/h_ZHa25_1000_weighted.GetBinContent(1),
            h_ZHa25_1000_weighted.GetBinContent(2)/h_ZHa25_1000_weighted.GetBinContent(1),
            h_ZHa25_1000_weighted.GetBinContent(3)/h_ZHa25_1000_weighted.GetBinContent(1),
            h_ZHa25_1000_weighted.GetBinContent(4)/h_ZHa25_1000_weighted.GetBinContent(1),
            h_ZHa25_1000_weighted.GetBinContent(5)/h_ZHa25_1000_weighted.GetBinContent(1),
            h_ZHa25_1000_weighted.GetBinContent(6)/h_ZHa25_1000_weighted.GetBinContent(1),
            h_ZHa25_1000_weighted.GetBinContent(7)/h_ZHa25_1000_weighted.GetBinContent(1)))
  f.write("                      &                     & Relative & %.4f & %.4f & %.4f & %.4f & %.4f & %.4f & %.4f \\\\\n" %  
           (h_ZHa25_1000_weighted.GetBinContent(1)/h_ZHa25_1000_weighted.GetBinContent(1),
            h_ZHa25_1000_weighted.GetBinContent(2)/h_ZHa25_1000_weighted.GetBinContent(1),
            h_ZHa25_1000_weighted.GetBinContent(3)/h_ZHa25_1000_weighted.GetBinContent(2),
            h_ZHa25_1000_weighted.GetBinContent(4)/h_ZHa25_1000_weighted.GetBinContent(3),
            h_ZHa25_1000_weighted.GetBinContent(5)/h_ZHa25_1000_weighted.GetBinContent(4),
            h_ZHa25_1000_weighted.GetBinContent(6)/h_ZHa25_1000_weighted.GetBinContent(5),
            h_ZHa25_1000_weighted.GetBinContent(7)/h_ZHa25_1000_weighted.GetBinContent(6)))
  f.write("  \\hline\n")
  f.write("  \\multirow{6}{*}{15} & \\multirow{2}{*}{10} & Total    & %.4f & %.4f & %.4f & %.4f & %.4f & %.4f & %.4f \\\\\n" % 
          (h_ZHa15_10_weighted.GetBinContent(1)/h_ZHa15_10_weighted.GetBinContent(1),
           h_ZHa15_10_weighted.GetBinContent(2)/h_ZHa15_10_weighted.GetBinContent(1),
           h_ZHa15_10_weighted.GetBinContent(3)/h_ZHa15_10_weighted.GetBinContent(1),
           h_ZHa15_10_weighted.GetBinContent(4)/h_ZHa15_10_weighted.GetBinContent(1),
           h_ZHa15_10_weighted.GetBinContent(5)/h_ZHa15_10_weighted.GetBinContent(1),
           h_ZHa15_10_weighted.GetBinContent(6)/h_ZHa15_10_weighted.GetBinContent(1),
           h_ZHa15_10_weighted.GetBinContent(7)/h_ZHa15_10_weighted.GetBinContent(1)))
  f.write("                      &                     & Relative & %.4f & %.4f & %.4f & %.4f & %.4f & %.4f & %.4f \\\\\n" % 
           (h_ZHa15_10_weighted.GetBinContent(1)/h_ZHa15_10_weighted.GetBinContent(1),
            h_ZHa15_10_weighted.GetBinContent(2)/h_ZHa15_10_weighted.GetBinContent(1),
            h_ZHa15_10_weighted.GetBinContent(3)/h_ZHa15_10_weighted.GetBinContent(2),
            h_ZHa15_10_weighted.GetBinContent(4)/h_ZHa15_10_weighted.GetBinContent(3),
            h_ZHa15_10_weighted.GetBinContent(5)/h_ZHa15_10_weighted.GetBinContent(4),
            h_ZHa15_10_weighted.GetBinContent(6)/h_ZHa15_10_weighted.GetBinContent(5),
            h_ZHa15_10_weighted.GetBinContent(7)/h_ZHa15_10_weighted.GetBinContent(6)))
  f.write("                      \\cline{2-10}\n")
  f.write("                      & \\multirow{2}{*}{100} & Total    & %.4f & %.4f & %.4f & %.4f & %.4f & %.4f & %.4f \\\\\n" % 
           (h_ZHa15_100_weighted.GetBinContent(1)/h_ZHa15_100_weighted.GetBinContent(1),
            h_ZHa15_100_weighted.GetBinContent(2)/h_ZHa15_100_weighted.GetBinContent(1),
            h_ZHa15_100_weighted.GetBinContent(3)/h_ZHa15_100_weighted.GetBinContent(1),
            h_ZHa15_100_weighted.GetBinContent(4)/h_ZHa15_100_weighted.GetBinContent(1),
            h_ZHa15_100_weighted.GetBinContent(5)/h_ZHa15_100_weighted.GetBinContent(1),
            h_ZHa15_100_weighted.GetBinContent(6)/h_ZHa15_100_weighted.GetBinContent(1),
            h_ZHa15_100_weighted.GetBinContent(7)/h_ZHa15_100_weighted.GetBinContent(1)))
  f.write("                      &                     & Relative & %.4f & %.4f & %.4f & %.4f & %.4f & %.4f & %.4f \\\\\n" %  
           (h_ZHa15_100_weighted.GetBinContent(1)/h_ZHa15_100_weighted.GetBinContent(1),
            h_ZHa15_100_weighted.GetBinContent(2)/h_ZHa15_100_weighted.GetBinContent(1),
            h_ZHa15_100_weighted.GetBinContent(3)/h_ZHa15_100_weighted.GetBinContent(2),
            h_ZHa15_100_weighted.GetBinContent(4)/h_ZHa15_100_weighted.GetBinContent(3),
            h_ZHa15_100_weighted.GetBinContent(5)/h_ZHa15_100_weighted.GetBinContent(4),
            h_ZHa15_100_weighted.GetBinContent(6)/h_ZHa15_100_weighted.GetBinContent(5),
            h_ZHa15_100_weighted.GetBinContent(7)/h_ZHa15_100_weighted.GetBinContent(6)))
  f.write("                      \\cline{2-10}\n")
  f.write("                      & \\multirow{2}{*}{1000} & Total    & %.4f & %.4f & %.4f & %.4f & %.4f & %.4f & %.4f \\\\\n" % 
           (h_ZHa15_1000_weighted.GetBinContent(1)/h_ZHa15_1000_weighted.GetBinContent(1),
            h_ZHa15_1000_weighted.GetBinContent(2)/h_ZHa15_1000_weighted.GetBinContent(1),
            h_ZHa15_1000_weighted.GetBinContent(3)/h_ZHa15_1000_weighted.GetBinContent(1),
            h_ZHa15_1000_weighted.GetBinContent(4)/h_ZHa15_1000_weighted.GetBinContent(1),
            h_ZHa15_1000_weighted.GetBinContent(5)/h_ZHa15_1000_weighted.GetBinContent(1),
            h_ZHa15_1000_weighted.GetBinContent(6)/h_ZHa15_1000_weighted.GetBinContent(1),
            h_ZHa15_1000_weighted.GetBinContent(7)/h_ZHa15_1000_weighted.GetBinContent(1)))
  f.write("                      &                     & Relative & %.4f & %.4f & %.4f & %.4f & %.4f & %.4f & %.4f \\\\\n" %  
           (h_ZHa15_1000_weighted.GetBinContent(1)/h_ZHa15_1000_weighted.GetBinContent(1),
            h_ZHa15_1000_weighted.GetBinContent(2)/h_ZHa15_1000_weighted.GetBinContent(1),
            h_ZHa15_1000_weighted.GetBinContent(3)/h_ZHa15_1000_weighted.GetBinContent(2),
            h_ZHa15_1000_weighted.GetBinContent(4)/h_ZHa15_1000_weighted.GetBinContent(3),
            h_ZHa15_1000_weighted.GetBinContent(5)/h_ZHa15_1000_weighted.GetBinContent(4),
            h_ZHa15_1000_weighted.GetBinContent(6)/h_ZHa15_1000_weighted.GetBinContent(5),
            h_ZHa15_1000_weighted.GetBinContent(7)/h_ZHa15_1000_weighted.GetBinContent(6)))
  f.write("  \\hline\n")
  f.write("  \end{tabular}\n")
  f.write("\label{table:cutflowPercentage}\n")
  f.write("\end{table}\n")
