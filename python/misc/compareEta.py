from ROOT import *
from plot_classes import Hist1D


f_data = TFile("/data/jburzyns/NtupleReader/ZJets.root")
f_sig  = TFile("/data/jburzyns/NtupleReader/ZH_a15a15_4b_ctau100.root")

#h1 = f_data.Get("PreselCR/Z_eta")
h2 = f_data.Get("PreselSR/j2_pt")
h3 = f_sig.Get("PreselSR/j2_pt")

h2.Rebin(5)
h3.Rebin(5)


Hist1D( hists = [h2,h3],
        types = ["data","ZH_a15a15_4b_ctau100"],
        legends = ["data","sig."],
        name = "j2_pt",
        x_title = "subleading jet p_{T}",
        x_units = "",
        y_min = 0.0,
        log_scale_y = False,
        y_axis_type = "Events",
        hide_lumi = True,
        norm = True,
        extra_lines_loc = [0.17,0.775],
        extra_legend_lines = [""] )
