from ROOT import *
from plot_classes import Hist1D


f_old  = TFile("/data/jburzyns/Hto4bLLPAlgorithm/user.jburzyns.data18_13TeV.00358031.physics_Main.r11760_r11764_tid20135468_00.NTUP.v4_tree.root/old.root")
f_new  = TFile("/data/jburzyns/Hto4bLLPAlgorithm/user.jburzyns.data18_13TeV.00358031.physics_Main.r11969_r11784_tid21711686_00.NTUP.v4_tree.root/new.root")

t_old = TTree() 
t_new = TTree() 

f_new.GetObject("outTree",t_new)
f_old.GetObject("outTree",t_old)


passFilter1 = TH1F("filter1","",2,0,2)
passFilter2 = TH1F("filter2","",2,0,2)
passFilter_e1 = TH1F("filter_e1","",2,0,2)
passFilter_e2 = TH1F("filter_e2","",2,0,2)
passFilter_mu1 = TH1F("filter_mu1","",2,0,2)
passFilter_mu2 = TH1F("filter_mu2","",2,0,2)

jet_pt1 = TH1F("jet_pt1","",100,0,100)
jet_pt2 = TH1F("jet_pt2","",100,0,100)
jet_passFilter1 = TH1F("jet_passFilter1","",2,0,2)
jet_passFilter2 = TH1F("jet_passFilter2","",2,0,2)

el_pt1 = TH1F("el_pt1","",100,0,100)
el_pt2 = TH1F("el_pt2","",100,0,100)
el_eta1 = TH1F("el_eta1","",60,-3,3)
el_eta2 = TH1F("el_eta2","",60,-3,3)
el_d01 = TH1F("el_d01","",100,-0.5,0.5)
el_d02 = TH1F("el_d02","",100,-0.5,0.5)
el_d0sig1 = TH1F("el_d0sig1","",100,-5,5)
el_d0sig2 = TH1F("el_d0sig2","",100,-5,5)
el_z01 = TH1F("el_z01","",100,-1,1)
el_z02 = TH1F("el_z02","",100,-1,1)
el_z0sintheta1 = TH1F("el_z0sintheta1","",100,-0.5,0.5)
el_z0sintheta2 = TH1F("el_z0sintheta2","",100,-0.5,0.5)

mu_pt1  = TH1F("mu_pt1","",100,0,100)
mu_pt2  = TH1F("mu_pt2","",100,0,100)
mu_eta1 = TH1F("mu_eta1","",60,-3,3)
mu_eta2 = TH1F("mu_eta2","",60,-3,3)
mu_d01 = TH1F("mu_d01","",100,-0.1,0.1)
mu_d02 = TH1F("mu_d02","",100,-0.1,0.1)
mu_d0sig1 = TH1F("mu_d0sig1","",60,-3,3)
mu_d0sig2 = TH1F("mu_d0sig2","",60,-3,3)
mu_z01 = TH1F("mu_z01","",100,-1,1)
mu_z02 = TH1F("mu_z02","",100,-1,1)
mu_z0sintheta1 = TH1F("mu_z0sintheta1","",100,-0.5,0.5)
mu_z0sintheta2 = TH1F("mu_z0sintheta2","",100,-0.5,0.5)

t_old.Draw("passesFilter>>filter1")
t_new.Draw("passesFilter>>filter2")
t_old.Draw("passesElecFilter>>filter_e1")
t_new.Draw("passesElecFilter>>filter_e2")
t_old.Draw("passesMuonFilter>>filter_mu1")
t_new.Draw("passesMuonFilter>>filter_mu2")

t_old.Draw("jet_pt>>jet_pt1","passesFilter*jet_passPresel*jet_passFilter")
t_new.Draw("jet_pt>>jet_pt2","passesFilter*jet_passPresel*jet_passFilter")
t_old.Draw("jet_passFilter>>jet_passFilter1","passesFilter")
t_new.Draw("jet_passFilter>>jet_passFilter2","passesFilter")

t_old.Draw("el_pt>>el_pt1","passesFilter*el_Medium")
t_new.Draw("el_pt>>el_pt2","passesFilter*el_Medium")
t_old.Draw("el_eta>>el_eta1","passesFilter*el_Medium")
t_new.Draw("el_eta>>el_eta2","passesFilter*el_Medium")
t_old.Draw("el_trkd0>>el_d01","passesFilter*el_Medium")
t_new.Draw("el_trkd0>>el_d02","passesFilter*el_Medium")
t_old.Draw("el_trkd0sig>>el_d0sig1","passesFilter*el_Medium")
t_new.Draw("el_trkd0sig>>el_d0sig2","passesFilter*el_Medium")
t_old.Draw("el_trkz0>>el_z01","passesFilter*el_Medium")
t_new.Draw("el_trkz0>>el_z02","passesFilter*el_Medium")
t_old.Draw("el_trkz0>>el_z0sintheta1","passesFilter*el_Medium")
t_new.Draw("el_trkz0>>el_z0sintheta2","passesFilter*el_Medium")
t_old.Draw("muon_pt>>mu_pt1","passesFilter")
t_new.Draw("muon_pt>>mu_pt2","passesFilter")
t_old.Draw("muon_eta>>mu_eta1","passesFilter")
t_new.Draw("muon_eta>>mu_eta2","passesFilter")
t_old.Draw("muon_trkd0>>mu_d01","passesFilter")
t_new.Draw("muon_trkd0>>mu_d02","passesFilter")
t_old.Draw("muon_trkz0>>mu_z01","passesFilter")
t_new.Draw("muon_trkz0>>mu_z02","passesFilter")


# Filter

Hist1D( hists = [passFilter1,passFilter2],
        types = ["data","data_alt"],
        legends = ["r11761","r11969"],
        name = "filter",
        x_title = "pass VH(4b) filter",
        x_units = "",
        y_min = 0.0,
        log_scale_y = False,
        y_axis_type = "Events",
        hide_lumi = False,
        lumi_val = "524",
        lumi_units = "pb",
        norm = False,
        extra_lines_loc = [0.17,0.775],
        extra_legend_lines = ["data18"] )
Hist1D( hists = [passFilter_e1,passFilter_e2],
        types = ["data","data_alt"],
        legends = ["r11761","r11969"],
        name = "filter_e",
        x_title = "pass VH(4b) electron filter",
        x_units = "",
        y_min = 0.0,
        log_scale_y = False,
        y_axis_type = "Events",
        hide_lumi = False,
        lumi_val = "524",
        lumi_units = "pb",
        norm = False,
        extra_lines_loc = [0.17,0.775],
        extra_legend_lines = ["data18"] )
Hist1D( hists = [passFilter_mu1,passFilter_mu2],
        types = ["data","data_alt"],
        legends = ["r11761","r11969"],
        name = "filter_mu",
        x_title = "pass VH(4b) muon filter",
        x_units = "",
        y_min = 0.0,
        log_scale_y = False,
        y_axis_type = "Events",
        hide_lumi = False,
        lumi_val = "524",
        lumi_units = "pb",
        norm = False,
        extra_lines_loc = [0.17,0.775],
        extra_legend_lines = ["data18"] )

# Jets

Hist1D( hists = [jet_pt1,jet_pt2],
        types = ["data","data_alt"],
        legends = ["r11761","r11969"],
        name = "jet_pt",
        x_title = "p_{T}",
        x_units = "GeV",
        y_min = 0.1,
        rebin = 2,
        log_scale_y = False,
        y_axis_type = "Jets",
        hide_lumi = False,
        lumi_val = "524",
        lumi_units = "pb",
        norm = False,
        extra_lines_loc = [0.17,0.775],
        extra_legend_lines = ["data18, Pass VH(4b) filter","Jets pass presel"] )
Hist1D( hists = [jet_passFilter1,jet_passFilter2],
        types = ["data","data_alt"],
        legends = ["r11761","r11969"],
        name = "jet_passFilter",
        x_title = "Pass displaced filter",
        x_units = "",
        y_min = 0.0,
        log_scale_y = False,
        y_axis_type = "Jets",
        hide_lumi = False,
        lumi_val = "524",
        lumi_units = "pb",
        norm = True,
        extra_lines_loc = [0.17,0.775],
        extra_legend_lines = ["data18, Pass VH(4b) filter"] )

# Electrons

Hist1D( hists = [el_pt1,el_pt2],
        types = ["data","data_alt"],
        legends = ["r11761","r11969"],
        name = "el_pt",
        x_title = "p_{T}",
        x_units = "GeV",
        y_min = 0.1,
        rebin = 2,
        log_scale_y = True,
        y_axis_type = "Electrons",
        hide_lumi = False,
        lumi_val = "524",
        lumi_units = "pb",
        norm = False,
        extra_lines_loc = [0.17,0.775],
        extra_legend_lines = ["data18, Pass VH(4b) filter","Medium electrons"] )
Hist1D( hists = [el_eta1,el_eta2],
        types = ["data","data_alt"],
        legends = ["r11761","r11969"],
        name = "el_eta",
        x_title = "#eta",
        x_units = "",
        y_min = 0.0,
        log_scale_y = False,
        y_axis_type = "Electrons",
        hide_lumi = False,
        lumi_val = "524",
        lumi_units = "pb",
        norm = False,
        extra_lines_loc = [0.17,0.775],
        extra_legend_lines = ["data18, Pass VH(4b) filter","Medium electrons"] )
Hist1D( hists = [el_d01,el_d02],
        types = ["data","data_alt"],
        legends = ["r11761","r11969"],
        name = "el_d0",
        x_title = "d_{0}",
        x_units = "mm",
        y_min = 0.1,
        log_scale_y = True,
        y_axis_type = "Electrons",
        hide_lumi = False,
        lumi_val = "524",
        lumi_units = "pb",
        norm = False,
        extra_lines_loc = [0.17,0.775],
        extra_legend_lines = ["data18, Pass VH(4b) filter","Medium electrons"] )
Hist1D( hists = [el_d0sig1,el_d0sig2],
        types = ["data","data_alt"],
        legends = ["r11761","r11969"],
        name = "el_d0sig",
        x_title = "d_{0} sig.",
        x_units = "mm",
        y_min = 0.1,
        log_scale_y = True,
        y_axis_type = "Electrons",
        hide_lumi = False,
        lumi_val = "524",
        lumi_units = "pb",
        norm = False,
        extra_lines_loc = [0.17,0.775],
        extra_legend_lines = ["data18, Pass VH(4b) filter","Medium electrons"] )
Hist1D( hists = [el_z01,el_z02],
        types = ["data","data_alt"],
        legends = ["r11761","r11969"],
        name = "el_z0",
        x_title = "z_{0}",
        x_units = "mm",
        y_min = 0.1,
        log_scale_y = True,
        y_axis_type = "Electrons",
        hide_lumi = False,
        lumi_val = "524",
        lumi_units = "pb",
        norm = False,
        extra_lines_loc = [0.17,0.775],
        extra_legend_lines = ["data18, Pass VH(4b) filter","Medium electrons"] )
Hist1D( hists = [el_z0sintheta1,el_z0sintheta2],
        types = ["data","data_alt"],
        legends = ["r11761","r11969"],
        name = "el_z0sintheta",
        x_title = "z_{0} sin #theta",
        x_units = "mm",
        y_min = 0.1,
        log_scale_y = True,
        y_axis_type = "Electrons",
        hide_lumi = False,
        lumi_val = "524",
        lumi_units = "pb",
        norm = False,
        extra_lines_loc = [0.17,0.775],
        extra_legend_lines = ["data18, Pass VH(4b) filter","Medium electrons"] )

# Muons

Hist1D( hists = [mu_pt1,mu_pt2],
        types = ["data","data_alt"],
        legends = ["r11761","r11969"],
        name = "mu_pt",
        x_title = "p_{T}",
        x_units = "GeV",
        y_min = 0.1,
        rebin = 2,
        log_scale_y = True,
        y_axis_type = "Muons",
        hide_lumi = False,
        lumi_val = "524",
        lumi_units = "pb",
        norm = False,
        extra_lines_loc = [0.17,0.775],
        extra_legend_lines = ["data18, Pass VH(4b) filter","Medium muons"] )
Hist1D( hists = [mu_eta1,mu_eta2],
        types = ["data","data_alt"],
        legends = ["r11761","r11969"],
        name = "mu_eta",
        x_title = "#eta",
        x_units = "",
        y_min = 0.0,
        log_scale_y = False,
        y_axis_type = "Muons",
        hide_lumi = False,
        lumi_val = "524",
        lumi_units = "pb",
        norm = False,
        extra_lines_loc = [0.17,0.775],
        extra_legend_lines = ["data18, Pass VH(4b) filter","Medium muons"] )
Hist1D( hists = [mu_d01,mu_d02],
        types = ["data","data_alt"],
        legends = ["r11761","r11969"],
        name = "mu_d0",
        x_title = "d_{0}",
        x_units = "mm",
        y_min = 0.1,
        log_scale_y = True,
        y_axis_type = "Muons",
        hide_lumi = False,
        lumi_val = "524",
        lumi_units = "pb",
        norm = False,
        extra_lines_loc = [0.17,0.775],
        extra_legend_lines = ["data18, Pass VH(4b) filter","Medium muons"] )
Hist1D( hists = [mu_z01,mu_z02],
        types = ["data","data_alt"],
        legends = ["r11761","r11969"],
        name = "mu_z0",
        x_title = "z_{0}",
        x_units = "mm",
        y_min = 0.1,
        log_scale_y = True,
        y_axis_type = "Muons",
        hide_lumi = False,
        lumi_val = "524",
        lumi_units = "pb",
        norm = False,
        extra_lines_loc = [0.17,0.775],
        extra_legend_lines = ["data18, Pass VH(4b) filter","Medium muons"] )
