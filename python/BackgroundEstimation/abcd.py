#!/usr/bin/env python
from array import array
import os
from ROOT import *
import math
from plot_classes import Hist1DFit, Hist2D
from classes_test import Hist1DBackground
gROOT.SetBatch(1)


######################################################################
    
f_sig = TFile('/data/jburzyns/NtupleReader/ZH_a55a55_4b_ctau10.root')
f = TFile('/data/jburzyns/NtupleReader/ZJets.root')

#regions = ["ABCDHighPTMediummu","ABCDLowPTMediummu","ABCDHighPTLoosemu","ABCDLowPTLoosemu"]
regions = ["ABCDMediummu"]
cut = 4

for region in regions:

  h = f.Get("Presel/" + region)
  h_sig = f_sig.Get("Presel/" + region)

# Bkg
  A_err = Double(0.0)
  B_err = Double(0.0)
  C_err = Double(0.0)
  D_err = Double(0.0)
  D = h.IntegralAndError(1,cut,1,cut,D_err)
  B = h.IntegralAndError(cut+1,401,1,cut,B_err)
  C = h.IntegralAndError(1,cut,cut+1,401,C_err)
  A = h.IntegralAndError(cut+1,401,cut+1,401,A_err)
  Apred = B*C/D
  Apred_err = Apred * math.sqrt( pow(B_err/B,2) + pow(C_err/C,2) + pow(D_err/D,2))

# Sig

  Asig_err = Double(0.0)
  Bsig_err = Double(0.0)
  Csig_err = Double(0.0)
  Dsig_err = Double(0.0)
  Dsig = h_sig.IntegralAndError(1,cut,1,cut,Dsig_err)
  Bsig = h_sig.IntegralAndError(cut+1,401,1,cut,Bsig_err)
  Csig = h_sig.IntegralAndError(1,cut,cut+1,401,Csig_err)
  Asig = h_sig.IntegralAndError(cut+1,401,cut+1,401,Asig_err)

  h.RebinX(4)
  h.RebinY(4)
  h_sig.RebinX(4)
  h_sig.RebinY(4)
  
  Z_string = ""
  qual_string = ""

  if "Medium" in region:
    qual_string = "Medium DVs"
  else:
    qual_string = "Loose DVs"

  if "Low" in region:
    Z_string = "Z p_{T} < 30 GeV"
  else:
    Z_string = "Z p_{T} > 30 GeV"

  Hist2D( hist = h,
          profile=True,
          name = region,
          x_title = "v_{1} #mu*n_{trk}",
          y_title = "v_{2} #mu*n_{trk}",
          x_units = "GeV",
          y_units = "GeV",
          x_max = 100,
          y_max = 100,
          log_scale_z = True,
          lumi_val = "140",
          hide_lumi = False,
          extra_lines_loc = [0.57,0.875],
          extra_legend_lines = ["SHERPA Z+jets",qual_string,Z_string] )
  Hist2D( hist = h_sig,
          name = region+"_sig",
          x_title = "v_{1} #mu*n_{trk}",
          y_title = "v_{2} #mu*n_{trk}",
          x_units = "GeV",
          y_units = "GeV",
          x_max = 100,
          y_max = 100,
          log_scale_z = True,
          lumi_val = "140",
          hide_lumi = False,
          extra_lines_loc = [0.57,0.875],
          extra_legend_lines = ["m_{a},c#tau = [55,10]",qual_string,Z_string] )


  with open(region+"abcd.tex","w+") as tf: 
    tf.write("\\documentclass{article}\n")
    tf.write("\\begin{document}\n")
    tf.write("\\begin{table}[h]\n")
    tf.write("\\centering\n")
    tf.write("  \\begin{tabular}{ c | c c c }\n")
    tf.write("  \\hline\n")
    tf.write("  \\hline\n")
    tf.write("  Region & Z+jets & Signal & $S/\sqrt{B}$ \\\\\n")
    tf.write("  \\hline\n")
    tf.write("  D & $%.4f \\pm$ %.2f & %.2f & %.2f \\\\\n" % (D,D_err,Dsig,Dsig/math.sqrt(D)))
    tf.write("  C & $%.4f \\pm$ %.2f & %.2f & %.2f \\\\\n" % (C,C_err,Csig,Csig/math.sqrt(C)))
    tf.write("  B & $%.4f \\pm$ %.2f & %.2f & %.2f \\\\\n" % (B,B_err,Bsig,Bsig/math.sqrt(B)))
    tf.write("  A & $%.4f \\pm$ %.2f & %.2f & %.2f \\\\\n" % (A,A_err,Asig,Asig/math.sqrt(A)))
    tf.write("  A (pred) & $%.2f \\pm$ %.2f & &  \\\\\n" % (Apred,Apred_err))
    tf.write("  \\hline\n")
    tf.write("  \end{tabular}\n")
    tf.write("\label{table:abcd}\n")
    tf.write("\end{table}\n")
    tf.write("\\end{document}\n")

  os.system("pdflatex " + region + "abcd.tex")
