#!/usr/bin/env python
from array import array
from ROOT import *
from background_plots import Hist1DBackground, TFCompare
from plot_classes import Hist1DFit
gROOT.SetBatch(1)

mass_string = ''
quality_string = ''
ZpTBinsLow=range(10,51,5)
ZpTBinsLow=[10,15,20,25,30,40,50,75,100,150,250,500]
ZpTBinsHigh=range(50,501,25)

fBkg  = TFile('/data/jburzyns/NtupleReader/ZJets.root')
fSig  = TFile('/data/jburzyns/NtupleReader/ZH_a55a55_4b_ctau10.root')

regions = ["LooseLow","LooseHigh","MediumLow","MediumHigh","TightLow","TightHigh"]
regions = ["MediumHigh"]

for region in regions:

  if "Loose" in region:
    quality_string = 'Loose DVs'
  elif "Medium" in region:
    quality_string = 'Medium DVs'
  elif "Tight" in region:
    quality_string = 'Tight DVs'

  if "High" in region:
    mass_string = '#mu > 3'
  elif "Low" in region:
    mass_string = '1 < #mu < 3'


  # high dPhi SR
  SR0      = fBkg.Get(region+"0SR/Z_pt")
  SR1      = fBkg.Get(region+"1SR/Z_pt")
  SR2      = fBkg.Get(region+"2SR/Z_pt")
  SR0_sig  = fSig.Get(region+"0SR/Z_pt")
  SR1_sig  = fSig.Get(region+"1SR/Z_pt")
  SR2_sig  = fSig.Get(region+"2SR/Z_pt")

  SR0.Add(fBkg.Get(region+"0CR/Z_pt"))
  SR1.Add(fBkg.Get(region+"1CR/Z_pt"))
  SR2.Add(fBkg.Get(region+"2CR/Z_pt"))
  SR0_sig.Add(fSig.Get(region+"0CR/Z_pt"))
  SR1_sig.Add(fSig.Get(region+"1CR/Z_pt"))
  SR2_sig.Add(fSig.Get(region+"2CR/Z_pt"))



  SR0.Sumw2()
  SR1.Sumw2()
  SR2.Sumw2()
  SR0_sig.Sumw2()
  SR1_sig.Sumw2()
  SR2_sig.Sumw2()

  SR0_low = SR0.Rebin(len(array('d',ZpTBinsLow))-1,region+'_SR0_rebinLow',array('d',ZpTBinsLow))
  SR1_low = SR1.Rebin(len(array('d',ZpTBinsLow))-1,region+'_SR1_rebinLow',array('d',ZpTBinsLow))
  SR0_high = SR0.Rebin(len(array('d',ZpTBinsHigh))-1,region+'_SR0_rebinHigh',array('d',ZpTBinsHigh))

  TF = SR1_low.Clone("TF")
  TF.Divide(SR0_low)

  fit      = TF1("fit","pol1",10.0,50.0)
  fit_low  = TF1("fit_low","pol1",10.0,50.0)
  fit_high = TF1("fit_high","pol1",10.0,50.0)
  TF.Fit(fit,"REM")

  Hist1DFit( h = TF,
             f = fit,
             legends = ["Observed","Fit"],
             name = region+"_TF",
             x_title = "Z p_{T}",
             x_units = "GeV",
             y_title = "DV Transfer Factor",
             y_min = 0.0,
             lumi_val = "140",
             hide_lumi = False,
             extra_lines_loc = [0.17,0.785],
             extra_legend_lines = [quality_string,mass_string] )

  fit_low.SetParameter(0,fit.GetParameter(0) - fit.GetParError(0))
  fit_low.SetParameter(1,fit.GetParameter(1) - fit.GetParError(1))
  fit_high.SetParameter(0,fit.GetParameter(0) + fit.GetParError(0))
  fit_high.SetParameter(1,fit.GetParameter(1) + fit.GetParError(1))

  EXP_SR1_HIGH = TH1F(region+'EXP_SR1_HIGH','',len(ZpTBinsHigh)-1,array('d',ZpTBinsHigh))
  EXP_SR2_HIGH = TH1F(region+'EXP_SR2_HIGH','',len(ZpTBinsHigh)-1,array('d',ZpTBinsHigh))

  for i in range(1,len(ZpTBinsHigh)):
    EXP_SR1_HIGH.SetBinContent(i, fit.Eval(EXP_SR1_HIGH.GetBinCenter(i))*SR0_high.GetBinContent(i))
    EXP_SR2_HIGH.SetBinContent(i, pow(fit.Eval(EXP_SR2_HIGH.GetBinCenter(i)),2)*SR0_high.GetBinContent(i))

    EXP_SR1_HIGH.SetBinError(i-1, fit_high.Eval(EXP_SR1_HIGH.GetBinCenter(i))*(SR0_high.GetBinContent(i) + SR0_high.GetBinError(i))
                                     - fit.Eval(EXP_SR1_HIGH.GetBinCenter(i))*SR0_high.GetBinContent(i))
    EXP_SR2_HIGH.SetBinError(i-1, pow(fit_high.Eval(EXP_SR1_HIGH.GetBinCenter(i)),2)*(SR0_high.GetBinContent(i) + SR0_high.GetBinError(i))
                                     - pow(fit.Eval(EXP_SR1_HIGH.GetBinCenter(i)),2)*SR0_high.GetBinContent(i))


  EXP_SR1_HIGH.Sumw2()
  EXP_SR2_HIGH.Sumw2()


  ZpTBins=[50,75,100,150,250,500]

  EXP_SR1_HIGH = EXP_SR1_HIGH.Rebin(len(array('d',ZpTBins))-1,region+'_EXP_SR1_rebin',array('d',ZpTBins))
  EXP_SR2_HIGH = EXP_SR2_HIGH.Rebin(len(array('d',ZpTBins))-1,region+'_EXP_SR2_rebin',array('d',ZpTBins))

  SR0_sig = SR0_sig.Rebin(len(array('d',ZpTBins))-1,region+'_SR0_sig_rebin',array('d',ZpTBins))
  SR1_sig = SR1_sig.Rebin(len(array('d',ZpTBins))-1,region+'_SR1_sig_rebin',array('d',ZpTBins))
  SR2_sig = SR2_sig.Rebin(len(array('d',ZpTBins))-1,region+'_SR2_sig_rebin',array('d',ZpTBins))

  SR1 = SR1.Rebin(len(array('d',ZpTBins))-1,region+'_SR1_rebin',array('d',ZpTBins))
  SR2 = SR2.Rebin(len(array('d',ZpTBins))-1,region+'_SR2_rebin',array('d',ZpTBins))



  Hist1DBackground( expected = EXP_SR1_HIGH,
                    observed = SR1,
                    sig = SR1_sig,
                    legends = ["Bkg. (prediction)","SHERPA Z+jets","m_{a} = 55 GeV"],
                    doRatio = True,
                    name = region+"_SR1_fit",
                    x_title = "Z p_{T}",
                    x_units = "GeV",
                    y_min = 0.01,
                    y_axis_type = "Events",
                    #log_scale_y = True,
                    lumi_val = "140",
                    hide_lumi = False,
                    extra_lines_loc = [0.17,0.775],
                    extra_legend_lines = ["1 " + quality_string,mass_string] )
  Hist1DBackground( expected = EXP_SR2_HIGH,
                    observed = SR2,
                    sig = SR2_sig,
                    legends = ["Bkg. (prediction)","SHERPA Z+jets","m_{a} = 55 GeV"],
                    doRatio = True,
                    name = region+"_SR2_fit",
                    x_title = "Z p_{T}",
                    x_units = "GeV",
                    y_min = 0.01,
                    #log_scale_y = True,
                    y_axis_type = "Events",
                    lumi_val = "140",
                    hide_lumi = False,
                    extra_lines_loc = [0.17,0.775],
                    extra_legend_lines = ["2 " + quality_string,mass_string] )
