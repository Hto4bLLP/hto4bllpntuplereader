#!/usr/bin/env python
from array import array
from ROOT import *
from background_plots import Hist1DBackground, TFCompare
gROOT.SetBatch(1)

mass_string = ''
quality_string = ''
#ZpTBins=[10,20,30,40,50,75,100,150,250,500]
ZpTBins=[10,25,50,100,150,250,500]

fBkg  = TFile('/data/jburzyns/NtupleReader/ZJets.root')
fSig  = TFile('/data/jburzyns/NtupleReader/ZH_a55a55_4b_ctau10.root')

regions = ["LooseLow","LooseHigh","MediumLow","MediumHigh","TightLow","TightHigh"]

for region in regions:

  if "Loose" in region:
    quality_string = 'Loose DVs'
  elif "Medium" in region:
    quality_string = 'Medium DVs'
  elif "Tight" in region:
    quality_string = 'Tight DVs'

  if "High" in region:
    mass_string = '#mu > 3'
  elif "Low" in region:
    mass_string = '1 < #mu < 3'


  # low dPhi CR
  CR0      = fBkg.Get(region+"0CR/Z_pt")
  CR1      = fBkg.Get(region+"1CR/Z_pt")
  CR2      = fBkg.Get(region+"2CR/Z_pt")
  CR0_sig  = fSig.Get(region+"0CR/Z_pt")
  CR1_sig  = fSig.Get(region+"1CR/Z_pt")
  CR2_sig  = fSig.Get(region+"2CR/Z_pt")
  # high dPhi SR
  SR0      = fBkg.Get(region+"0SR/Z_pt")
  SR1      = fBkg.Get(region+"1SR/Z_pt")
  SR2      = fBkg.Get(region+"2SR/Z_pt")
  SR0_sig  = fSig.Get(region+"0SR/Z_pt")
  SR1_sig  = fSig.Get(region+"1SR/Z_pt")
  SR2_sig  = fSig.Get(region+"2SR/Z_pt")

  CR0.Sumw2()
  CR1.Sumw2()
  CR2.Sumw2()
  CR0_sig.Sumw2()
  CR1_sig.Sumw2()
  CR2_sig.Sumw2()
  SR0.Sumw2()
  SR1.Sumw2()
  SR2.Sumw2()
  SR0_sig.Sumw2()
  SR1_sig.Sumw2()
  SR2_sig.Sumw2()

  CR0 = CR0.Rebin(len(array('d',ZpTBins))-1,region+'_CR0_rebin',array('d',ZpTBins))
  CR1 = CR1.Rebin(len(array('d',ZpTBins))-1,region+'_CR1_rebin',array('d',ZpTBins))
  CR2 = CR2.Rebin(len(array('d',ZpTBins))-1,region+'_CR2_rebin',array('d',ZpTBins))
  SR0 = SR0.Rebin(len(array('d',ZpTBins))-1,region+'_SR0_rebin',array('d',ZpTBins))
  SR1 = SR1.Rebin(len(array('d',ZpTBins))-1,region+'_SR1_rebin',array('d',ZpTBins))
  SR2 = SR2.Rebin(len(array('d',ZpTBins))-1,region+'_SR2_rebin',array('d',ZpTBins))

  CR0_sig = CR0_sig.Rebin(len(array('d',ZpTBins))-1,region+'_CR0_sig_rebin',array('d',ZpTBins))
  CR1_sig = CR1_sig.Rebin(len(array('d',ZpTBins))-1,region+'_CR1_sig_rebin',array('d',ZpTBins))
  CR2_sig = CR2_sig.Rebin(len(array('d',ZpTBins))-1,region+'_CR2_sig_rebin',array('d',ZpTBins))
  SR0_sig = SR0_sig.Rebin(len(array('d',ZpTBins))-1,region+'_SR0_sig_rebin',array('d',ZpTBins))
  SR1_sig = SR1_sig.Rebin(len(array('d',ZpTBins))-1,region+'_SR1_sig_rebin',array('d',ZpTBins))
  SR2_sig = SR2_sig.Rebin(len(array('d',ZpTBins))-1,region+'_SR2_sig_rebin',array('d',ZpTBins))


  TFCR = CR1.Clone("TFCR")
  TFCR.Divide(CR0)

  TFSR = SR1.Clone("TFSR")
  TFSR.Divide(SR0)

  TFCompare( TF1 = TFCR,
             TF2 = TFSR,
             legends = ["CR","SR"],
             name = region+"_TF",
             x_title = "Z p_{T}",
             x_units = "GeV",
             y_title = "DV Transfer Factor",
             y_min = 0.0,
             lumi_val = "140",
             hide_lumi = False,
             extra_lines_loc = [0.17,0.785],
             extra_legend_lines = [quality_string,mass_string] )



  SR0_EXP = SR0.Clone()
  SR1_EXP = SR0.Clone()
  SR1_EXP.Multiply(TFCR)
  SR2_EXP = SR1_EXP.Clone()
  SR2_EXP.Multiply(TFCR)

  CR1_EXP = CR1.Clone()
  CR2_EXP = CR2.Clone()
  Hist1DBackground( expected = CR1_EXP,
                    observed = CR1,
                    sig = CR1_sig,
                    legends = ["Bkg. (prediction)","SHERPA Z+jets","m_{a} = 55 GeV"],
                    doRatio = True,
                    name = region+"_CR1",
                    x_title = "Z p_{T}",
                    x_units = "GeV",
                    y_min = 0.01,
                    y_axis_type = "Events",
                    log_scale_y = True,
                    blind = True,
                    lumi_val = "140",
                    hide_lumi = False,
                    extra_lines_loc = [0.17,0.775],
                    extra_legend_lines = ["1 " + quality_string,mass_string] )
  Hist1DBackground( expected = CR2_EXP,
                    observed = CR2,
                    sig = CR2_sig,
                    legends = ["Bkg. (prediction)","SHERPA Z+jets","m_{a} = 55 GeV"],
                    doRatio = True,
                    name = region+"_CR2",
                    x_title = "Z p_{T}",
                    x_units = "GeV",
                    y_min = 0.01,
                    log_scale_y = True,
                    y_axis_type = "Events",
                    blind = True,
                    lumi_val = "140",
                    hide_lumi = False,
                    extra_lines_loc = [0.17,0.775],
                    extra_legend_lines = ["2 " + quality_string,mass_string] )
  Hist1DBackground( expected = SR0,
                    observed = SR0,
                    sig = SR0_sig,
                    legends = ["SHERPA Z+jets","SHERPA Z+jets","m_{a} = 55 GeV"],
                    doRatio = True,
                    name = region+"_SR0",
                    x_title = "Z p_{T}",
                    x_units = "GeV",
                    y_min = 0.01,
                    y_axis_type = "Events",
                    blind = True,
                    #log_scale_y = True,
                    lumi_val = "140",
                    hide_lumi = False,
                    extra_lines_loc = [0.17,0.775],
                    extra_legend_lines = ["0 " + quality_string,mass_string] )
  Hist1DBackground( expected = SR1_EXP,
                    observed = SR1,
                    sig = SR1_sig,
                    legends = ["SHERPA Z+jets","SHERPA Z+jets","m_{a} = 55 GeV"],
                    doRatio = True,
                    name = region+"_SR1",
                    x_title = "Z p_{T}",
                    x_units = "GeV",
                    y_min = 0.01,
                    y_axis_type = "Events",
                    blind = False,
                    #log_scale_y = True,
                    lumi_val = "140",
                    hide_lumi = False,
                    extra_lines_loc = [0.17,0.775],
                    extra_legend_lines = ["1 " + quality_string,mass_string] )
  Hist1DBackground( expected = SR2_EXP,
                    observed = SR2,
                    sig = SR2_sig,
                    legends = ["SHERPA Z+jets","SHERPA Z+jets","m_{a} = 55 GeV"],
                    doRatio = True,
                    name = region+"_SR2",
                    x_title = "Z p_{T}",
                    x_units = "GeV",
                    y_min = 0.01,
                    blind = False,
                    #log_scale_y = True,
                    y_axis_type = "Events",
                    lumi_val = "140",
                    hide_lumi = False,
                    extra_lines_loc = [0.17,0.775],
                    extra_legend_lines = ["2 " + quality_string,mass_string] )
