from plot_classes import Hist1D
from collections import defaultdict
from ROOT import TFile, TTree, TH1F

ZHa55_10 = TFile("/data/jburzyns/NtupleReader/ZH_a55a55_4b_ctau10.root")
ZHa35_10 = TFile("/data/jburzyns/NtupleReader/ZH_a35a35_4b_ctau10.root")
ZHa25_10 = TFile("/data/jburzyns/NtupleReader/ZH_a25a25_4b_ctau10.root")
ZHa15_10 = TFile("/data/jburzyns/NtupleReader/ZH_a15a15_4b_ctau10.root")

ZHa55_100 = TFile("/data/jburzyns/NtupleReader/ZH_a55a55_4b_ctau100.root")
ZHa35_100 = TFile("/data/jburzyns/NtupleReader/ZH_a35a35_4b_ctau100.root")
ZHa25_100 = TFile("/data/jburzyns/NtupleReader/ZH_a25a25_4b_ctau100.root")
ZHa15_100 = TFile("/data/jburzyns/NtupleReader/ZH_a15a15_4b_ctau100.root")

ZHa55_1000 = TFile("/data/jburzyns/NtupleReader/ZH_a55a55_4b_ctau1000.root")
ZHa35_1000 = TFile("/data/jburzyns/NtupleReader/ZH_a35a35_4b_ctau1000.root")
ZHa25_1000 = TFile("/data/jburzyns/NtupleReader/ZH_a25a25_4b_ctau1000.root")
ZHa15_1000 = TFile("/data/jburzyns/NtupleReader/ZH_a15a15_4b_ctau1000.root")

trees = defaultdict(dict)

trees["15"]["10"]   = ZHa15_10.Get("tree")
trees["15"]["100"]  = ZHa15_100.Get("tree")
trees["15"]["1000"] = ZHa15_1000.Get("tree")

trees["25"]["10"]   = ZHa25_10.Get("tree")
trees["25"]["100"]  = ZHa25_100.Get("tree")
trees["25"]["1000"] = ZHa25_1000.Get("tree")

trees["35"]["10"]   = ZHa35_10.Get("tree")
trees["35"]["100"]  = ZHa35_100.Get("tree")
trees["35"]["1000"] = ZHa35_1000.Get("tree")

trees["55"]["10"]   = ZHa55_10.Get("tree")
trees["55"]["100"]  = ZHa55_100.Get("tree")
trees["55"]["1000"] = ZHa55_1000.Get("tree")

legs_mass  = ["m_{a} = 15 GeV", "m_{a} = 25 GeV", "m_{a} = 35 GeV", "m_{a} = 55 GeV"]
types_mass = ["ZH_a15a15_4b_ctau10", "ZH_a25a25_4b_ctau10", "ZH_a35a35_4b_ctau10", "ZH_a55a55_4b_ctau10"]
legs_ctau  = ["c#tau = 10 mm", "c#tau = 100 mm", "c#tau = 1000 mm"]
types_ctau = ["ZH_a55a55_4b_ctau10", "ZH_a55a55_4b_ctau100", "ZH_a55a55_4b_ctau1000"]

masses = ["15","25","35","55"]
ctaus  = ["10","100","1000"]


class plot:

  def __init__(self, var, bins, label, units, log=False):
    self.var  = var
    self.bins = bins
    self.label = label
    self.units = units
    self.log = log

plot_list = []

plot_list.append(plot("H_pt",[125,0,250],"p_{T}(H)","GeV"))
plot_list.append(plot("V_pt",[125,0,250],"p_{T}(V)","GeV"))
plot_list.append(plot("LLP_deltaR",[100,0,6],"#DeltaR(a_{1},a_{2})",""))
plot_list.append(plot("LLP_pt[0]",[75,0,150], "p_{T}(a_{1})","GeV"))
plot_list.append(plot("LLP_pt[1]",[75,0,150], "p_{T}(a_{2})","GeV"))
plot_list.append(plot("V_child_pt[0]",[75,0,150], "p_{T}(l_{1})","GeV"))
plot_list.append(plot("V_child_pt[1]",[75,0,150], "p_{T}(l_{2})","GeV"))
plot_list.append(plot("LLP_Lxy[0]",[100,0,500], "decay L_{xy}(a_{1})","mm"))
plot_list.append(plot("LLP_Lxy[1]",[100,0,500], "decay L_{xy}(a_{2})","mm"))
plot_list.append(plot("LLP_lifetime_prop",[100,0,1000], "c#tau(a)","mm",log=True))

hists = defaultdict(lambda: defaultdict(dict))

for plot in plot_list:
  for mass in masses:
    for ctau in ctaus:
      hists[plot.var][mass][ctau] = TH1F("h_"+mass+ctau+plot.var,"",plot.bins[0], plot.bins[1], plot.bins[2])

for plot in plot_list:
  for mass in masses:
    for ctau in ctaus:
      trees[mass][ctau].Draw(plot.var+">>h_"+mass+ctau+plot.var,"totalEventWeight")

legs_mass  = ["m_{a} = 15 GeV", "m_{a} = 25 GeV", "m_{a} = 35 GeV", "m_{a} = 55 GeV"]
types_mass = ["ZH_a15a15_4b_ctau10", "ZH_a25a25_4b_ctau10", "ZH_a35a35_4b_ctau10", "ZH_a55a55_4b_ctau10"]
legs_ctau  = ["c#tau = 10 mm", "c#tau = 100 mm", "c#tau = 1000 mm"]
types_ctau = ["ZH_a15a15_4b_ctau10", "ZH_a15a15_4b_ctau100", "ZH_a15a15_4b_ctau1000"]

for plot in plot_list:
  for ctau in ctaus:
    tmp_hists = [hists[plot.var][mass][ctau] for mass in masses] 

    Hist1D( hists = tmp_hists,
            types = types_mass,
            legends = legs_mass,
            y_axis_type = "Events",
            atlas_mod = "Simulation Internal",
            name = plot.var+ctau,
            x_title = plot.label,
            x_units = plot.units,
            log_scale_y = plot.log,
            hide_lumi = True,
            norm = True,
            draw_markers = False,
            extra_lines_loc = [0.17,0.775],
            extra_legend_lines = ["c#tau = " + ctau + " mm"] )

  for mass in masses:
    tmp_hists = [hists[plot.var][mass][ctau] for ctau in ctaus]

    Hist1D( hists = tmp_hists,
            types = types_ctau,
            legends = legs_ctau,
            y_axis_type = "Events",
            atlas_mod = "Simulation Internal",
            name = plot.var+mass,
            x_title = plot.label,
            x_units = plot.units,
            log_scale_y = plot.log,
            hide_lumi = True,
            norm = True,
            draw_markers = False,
            extra_lines_loc = [0.17,0.775],
            extra_legend_lines = ["m_{a} = " + mass + " GeV"] )
