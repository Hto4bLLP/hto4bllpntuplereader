#!/usr/bin/env python
import math
import argparse
import re
import os
from plot_classes import Hist1D, Hist2D, HistStack
from ROOT import *

bins = {
  "nSecVtx_signal": [5,0,5],
  "nSecVtx": [7,0,7],
  "nSecVtx_presel": [5,0,5],
  "nSecVtx_signal_loose": [5,0,5],
  "nSecVtx_signal_tight": [5,0,5],
  "secVtx_mult": [7,0,7],
  "secVtxMedium_r": [75,0,150],
  "secVtxMedium_distFromPV": [75,0,300],
  "secVtxMedium_minJetDR": [20,0,1.0],
  "secVtxMedium_ntrk": [13,2,15],
  "secVtx_nSiHits": [30,0,150],
  "secVtx_m": [50,0,25],
  "secVtxMedium_pt": [25,0,25],
  "secVtx_minOneTrackRemovedMass": [50,0,50],
  "secVtx_m/secVtx_maxDR": [50,0,50],
  "secVtxMedium_LxySig": [50,0,1000],
  "secVtxMedium_minDist": [20,0,100],
  "secVtx_kerasScore": [20,0,1],
  "secVtx_trk_chi2": [50,0,10],
  "secVtx_trk_chi2_toSV": [50,0,100],
  "secVtx_trk_d0_wrtSV": [50,-2.5,2.5],
  "secVtx_trk_z0_wrtSV": [50,-2.5,2.5],
  "secVtx_trk_errd0_wrtSV": [50,0,.2],
  "secVtx_trk_errz0_wrtSV": [50,0,.2],
  "secVtx_trk_z0signif_wrtSV": [100,-20,20],
  "secVtx_trk_d0signif_wrtSV": [100,-20,20],
  "secVtxMedium_errLxy": [100,0,1.0],
  "mjj" : [100,0,1000],
  "mjjj" : [100,0,1000],
  "mjjjj" : [100,0,1000],
  "dijet_dR" : [50,0,5],
  "dijet_dR3" : [50,0,5],
  "dijet_dR4" : [50,0,5],
  "nJets" : [10,0,10],
  "jet_chf" : [100,0,1],
  "jet_lep_dR" : [50,0,5],
  "j1_pt" : [48,20,500],
  "j2_pt" : [38,20,400],
  "l1_pt" : [48,20,500],
  "Z_pt" : [50,0,500],
  "l2_pt" : [38,20,400],
  "Z_m" : [30,50,200],
  "BDTweight" : [50,-1,1],
  "Lep_JetMinCHF_DeltaPhi" : [32,0,3.2],
  "Lep_SumLeadingJets_DeltaPhi" : [32,0,3.2],
  "minCHFJet_phi" : [64,-3.2,3.2],
  "Ht" : [100,0,1000],
  "Lep_Jet_MinDeltaPhi" : [32,0,3.2],
  "lep_pt_imbalance" : [25,0,1],
  "z_jets_dPhi" : [32,0,3.2],
}

labels = {
  "secVtx_mult": ["Vertex multiplicity",""],
  "nSecVtx": ["Vertex multiplicity",""],
  "nSecVtx_presel": ["Vertex multiplicity",""],
  "nSecVtx_signal": ["Vertex multiplicity",""],
  "nSecVtx_signal_loose": ["Vertex multiplicity",""],
  "nSecVtx_signal_tight": ["Vertex multiplicity",""],
  "secVtxMedium_r": ["L_{xy}","mm"],
  "secVtxMedium_distFromPV": ["Distance from PV","mm"],
  "secVtxMedium_ntrk": ["n_{trk}",""],
  "secVtx_nSiHits": ["Number of Si Hits",""],
  "secVtx_m": ["mass","GeV"],
  "secVtxMedium_pt": ["p_{T}","GeV"],
  "secVtxMedium_LxySig": ["L_{xy} Significance",""],
  "secVtxMedium_minDist": ["Distance between DVs","mm"],
  "secVtxMedium_minJetDR": ["#DeltaR(j)",""],
  "secVtx_minOneTrackRemovedMass": ["minOneTrackRemovedMass","GeV"],
  "secVtx_kerasScore": ["Keras score",""],
  "secVtx_trk_chi2": ["Vertex Track #chi^{2}",""],
  "secVtx_trk_chi2_toSV": ["Vetex Track #chi^{2} to vertex",""],
  "secVtx_trk_d0_wrtSV": ["Vertex Track d_{0} to vertex ","mm"],
  "secVtx_trk_z0_wrtSV": ["Vertex Track z_{0} to vertex ","mm"],
  "secVtx_trk_errd0_wrtSV": ["Vertex Track d_{0} to vertex error","mm"],
  "secVtx_trk_errz0_wrtSV": ["Vertex Track z_{0} to vertex error","mm"],
  "secVtx_trk_d0signif_wrtSV": ["Vertex Track d_{0} significance",""],
  "secVtx_trk_z0signif_wrtSV": ["Vertex Track z_{0} significance",""],
  "secVtxMedium_errLxy": ["Vertex #sigma(L_{xy})","mm"],
  "mjj" : ["m_{jj}","GeV"],
  "mjjj" : ["m_{jjj}","GeV"],
  "mjjjj" : ["m_{jjjj}","GeV"],
  "dijet_dR" : ["#DeltaR_{jj}",""],
  "dijet_dR3" : ["#DeltaR_{avg}",""],
  "dijet_dR4" : ["#DeltaR_{avg}",""],
  "nJets" : ["n_{jet}",""],
  "jet_chf" : ["CHF",""],
  "jet_lep_dR" : ["#DeltaR_{jet,lep}",""],
  "j1_pt" : ["Leading jet p_{T}","GeV"],
  "j2_pt" : ["Subleading jet p_{T}","GeV"],
  "l1_pt" : ["Leading lepton p_{T}","GeV"],
  "l2_pt" : ["Subleading lepton p_{T}","GeV"],
  "Z_pt" : ["Z p_{T}","GeV"],
  "Z_m" : ["Z mass","GeV"],
  "BDTweight" : ["BDT score",""],
  "Lep_JetMinCHF_DeltaPhi" : ["Lep_JetMinCHF_DeltaPhi",""],
  "Lep_SumLeadingJets_DeltaPhi" : ["#Delta#phi(l,jj)",""],
  "minCHFJet_phi" : ["minCHFJet_phi",""],
  "Ht": ["Ht","GeV"],
  "Lep_Jet_MinDeltaPhi" : ["Lep_Jet_MinDeltaPhi",""],
  "lep_pt_imbalance" : ["p_{T} imbalance",""],
  "z_jets_dPhi" : ["|#Delta#phi(Z,jj)|",""],
}

legends = {
  "data" : "data",
  "ZJets" : "SHERPA Z+jets",
  "WH_a55a55_4b_ctau1" : "m_{a}, c#tau = [55,1]",
  "WH_a55a55_4b_ctau10" : "m_{a}, c#tau = [55,10]",
  "WH_a55a55_4b_ctau100" : "m_{a}, c#tau = [55,100]",
  "WH_a15a15_4b_ctau1" : "m_{a}, c#tau = [15,1]",
  "WH_a15a15_4b_ctau10" : "m_{a}, c#tau = [15,10]",
  "WH_a15a15_4b_ctau100" : "m_{a}, c#tau = [15,100]",
  "ZH_a55a55_4b_ctau10" : "m_{a}, c#tau = [55,10]",
  "ZH_a55a55_4b_ctau100" : "m_{a}, c#tau = [55,100]",
  "ZH_a35a35_4b_ctau10" : "m_{a}, c#tau = [35,10]",
  "ZH_a35a35_4b_ctau100" : "m_{a}, c#tau = [35,100]",
  "ZH_a25a25_4b_ctau10" : "m_{a}, c#tau = [25,10]",
  "ZH_a25a25_4b_ctau100" : "m_{a}, c#tau = [25,100]",
  "ZH_a15a15_4b_ctau10" : "m_{a}, c#tau = [15,10]",
  "ZH_a15a15_4b_ctau100" : "m_{a}, c#tau = [15,100]",
}

def parseCutString(cutString):
    legend_lines = []

    cutList = cutString.split('*')
    vertexCuts = ""
    eventCuts = "Vertex Preselection"
    for cut in cutList:
        cut = cut.replace(')',"")
        if "_ntrk" in cut:
          vertexCuts += ", n_{trk} #geq " + cut.split('=')[-1] if vertexCuts else "n_{trk} #geq " + cut.split('=')[-1]
        elif "_maxDR" in cut:
          vertexCuts += ", m\' > " + cut.split('>')[-1] if vertexCuts else "m\' > " + cut.split('>')[-1]
        elif "_m" in cut:
          vertexCuts += ", m > " + cut.split('>')[-1] if vertexCuts else "m > " + cut.split('>')[-1]
        elif "_keras" in cut:
          vertexCuts += ", Keras score > " + cut.split('>')[-1] if vertexCuts else "Keras score > " + cut.split('>')[-1]
#        elif "_passPresel" in cut:
#          vertexCuts += ", Vertex Preselection" if vertexCuts else "Vertex Preselection" 

    if eventCuts:
        legend_lines.append(eventCuts)
    if vertexCuts:
        legend_lines.append(vertexCuts)
    return legend_lines

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--files", nargs='+', required=True, help="list of files to plot" )
    parser.add_argument("--var", required=True, type=str,help="variable to plot" )
    parser.add_argument("--cutString", type=str, default="", help="optional cut string to apply during TTree::Draw()")
    parser.add_argument("--log", action='store_true', help="log scale plot")
    parser.add_argument("--norm", action='store_true', help="normalize")
    parser.add_argument("--y_min", type=float, help="y minimum")

    args = parser.parse_args()

    files = {}
    trees = {}

    for f in args.files:
        key = os.path.basename(f).split('.')[0]
        files[key] = TFile(f)
        trees[key] = TTree()
        files[key].GetObject("tree",trees[key])

    histos = []
    hist_dict = {}
    keys = []
    legs = []
    for key in trees:
        keys.append(key)
        h = TH1F("h_"+args.var+"_"+key,"", bins[args.var][0], bins[args.var][1], bins[args.var][2])
        histos.append(h)
        hist_dict[key] = h
        legs.append(legends[key])

    var = args.var
    if("d0sig" in var):
        var = "secVtx_trk_d0_wrtSV/secVtx_trk_errd0_wrtSV"
    if("z0sig" in var):
        var = "secVtx_trk_z0_wrtSV/secVtx_trk_errz0_wrtSV"
    if("LxySig" in var):
        var = "secVtxMedium_r/secVtxMedium_errLxy"
    if("_m" in var):
#        var = "secVtxMedium_m/secVtxMedium_maxDR"
        var = "secVtxMedium_m"

    for key in trees:
        t = trees[key]
        if "mult" in args.var:
          t.Draw("Sum$(" + args.cutString + ")>>+h_"+args.var+"_"+key,"totalEventWeight*passPresel")
        else:
          t.Draw("fabs("+var + ")>>+h_"+args.var+"_"+key,args.cutString+"*totalEventWeight" if args.cutString else "totalEventWeight")
    
    # compute S/B
    cutString = parseCutString(args.cutString)
    leg_lines = cutString

    Hist1D( hists = histos,
            types = keys,
            legends = legs,
            name = args.var,
            x_title = labels[args.var][0],
            x_units = labels[args.var][1],
            y_min = args.y_min,
            log_scale_y = args.log,
            y_axis_type = "Events" if not "secVtx" in args.var else "Vertices",
            lumi_val = "140",
            hide_lumi = False,
            norm = args.norm,
            x_max = 99999999,
            extra_lines_loc = [0.17,0.755],
            extra_legend_lines = leg_lines)

