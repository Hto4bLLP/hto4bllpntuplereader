#!/usr/bin/env python
import math
import argparse
import re
import os
from plot_classes import Graph1D, GraphBrazil
from ROOT import *


if __name__ == "__main__":

    base_dir = "/home/net3/jburzyns/VH4b/StatisticalAnalysis/run/"
    f_55 = TFile(base_dir+"ZH_a55a55_4b.root")
    f_35 = TFile(base_dir+"ZH_a35a35_4b.root")
    f_25 = TFile(base_dir+"ZH_a25a25_4b.root")

    g_55 = f_55.Get("efficiency")
    g_35 = f_35.Get("efficiency")
    g_25 = f_25.Get("efficiency")

    eff_graphs = [g_55, g_35, g_25]

    legs_eff = ["m_{a} = 55 GeV", "m_{a} = 35 GeV", "m_{a} = 25 GeV"]
    keys_eff = ["ZH_a55a55_4b_ctau10","ZH_a35a35_4b_ctau10", "ZH_a25a25_4b_ctau10"]

    Graph1D(graphs = eff_graphs,
           types = keys_eff,
           legends = legs_eff,
           y_title = "Global efficiency",
           name = "globalEff",
           x_title = "#it{a} proper decay length",
           x_units = "m",
           y_max = 0.02,
           y_min = 0.0,
           x_min = 0.0005,
           x_max = 5,
           log_scale_y = False,
           log_scale_x = True,
           lumi_val = "140",
           hide_lumi = False,
           extra_lines_loc = [0.17,0.775],
           extra_legend_lines = [""]
           )
    Graph1D(graphs = eff_graphs,
           types = keys_eff,
           legends = legs_eff,
           y_title = "Global efficiency",
           name = "globalEff",
           x_title = "#it{a} proper decay length",
           x_units = "m",
           y_max = 1.0,
           y_min = 0.00001,
           x_min = 0.0005,
           x_max = 5,
           log_scale_y = True,
           log_scale_x = True,
           lumi_val = "140",
           hide_lumi = False,
           extra_lines_loc = [0.17,0.775],
           extra_legend_lines = [""]
           )
