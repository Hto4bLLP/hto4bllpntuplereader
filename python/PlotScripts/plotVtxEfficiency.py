from plot_classes import Hist1D, Eff1D
from ROOT import TFile, TTree, TH1F

ZHa55 = TFile("/data/jburzyns/NtupleReader/ZH_a55a55.root")
ZHa35 = TFile("/data/jburzyns/NtupleReader/ZH_a35a35.root")
ZHa25 = TFile("/data/jburzyns/NtupleReader/ZH_a25a25.root")
ZHa15 = TFile("/data/jburzyns/NtupleReader/ZH_a15a15.root")

t_ZHa55 = ZHa55.Get("tree")
t_ZHa35 = ZHa35.Get("tree")
t_ZHa25 = ZHa25.Get("tree")
t_ZHa15 = ZHa15.Get("tree")

nbins = 30
x_min = 0
x_max = 300

h_ZHa55_r_num                 = TH1F("h_ZHa55_r_num","",nbins,x_min,x_max)
h_ZHa55_r_num_presel          = TH1F("h_ZHa55_r_num_presel","",nbins,x_min,x_max)
h_ZHa55_r_num_good            = TH1F("h_ZHa55_r_num_good","",nbins,x_min,x_max)
h_ZHa55_r_num_good_no_mat     = TH1F("h_ZHa55_r_num_good_no_mat","",nbins,x_min,x_max)
h_ZHa35_r_num                 = TH1F("h_ZHa35_r_num","",nbins,x_min,x_max)
h_ZHa35_r_num_presel          = TH1F("h_ZHa35_r_num_presel","",nbins,x_min,x_max)
h_ZHa35_r_num_good            = TH1F("h_ZHa35_r_num_good","",nbins,x_min,x_max)
h_ZHa35_r_num_good_no_mat     = TH1F("h_ZHa35_r_num_good_no_mat","",nbins,x_min,x_max)
h_ZHa25_r_num                 = TH1F("h_ZHa25_r_num","",nbins,x_min,x_max)
h_ZHa25_r_num_presel          = TH1F("h_ZHa25_r_num_presel","",nbins,x_min,x_max)
h_ZHa25_r_num_good            = TH1F("h_ZHa25_r_num_good","",nbins,x_min,x_max)
h_ZHa25_r_num_good_no_mat     = TH1F("h_ZHa25_r_num_good_no_mat","",nbins,x_min,x_max)
h_ZHa15_r_num                 = TH1F("h_ZHa15_r_num","",nbins,x_min,x_max)
h_ZHa15_r_num_presel          = TH1F("h_ZHa15_r_num_presel","",nbins,x_min,x_max)
h_ZHa15_r_num_good            = TH1F("h_ZHa15_r_num_good","",nbins,x_min,x_max)
h_ZHa15_r_num_good_no_mat     = TH1F("h_ZHa15_r_num_good_no_mat","",nbins,x_min,x_max)
h_ZHa55_r_denom = TH1F("h_ZHa55_r_denom","",nbins,x_min,x_max)
h_ZHa35_r_denom = TH1F("h_ZHa35_r_denom","",nbins,x_min,x_max)
h_ZHa25_r_denom = TH1F("h_ZHa25_r_denom","",nbins,x_min,x_max)
h_ZHa15_r_denom = TH1F("h_ZHa15_r_denom","",nbins,x_min,x_max)

x_min = -1*x_max
h_ZHa55_z_num                 = TH1F("h_ZHa55_z_num","",nbins,x_min,x_max)
h_ZHa55_z_num_presel          = TH1F("h_ZHa55_z_num_presel","",nbins,x_min,x_max)
h_ZHa55_z_num_good            = TH1F("h_ZHa55_z_num_good","",nbins,x_min,x_max)
h_ZHa55_z_num_good_no_mat     = TH1F("h_ZHa55_z_num_good_no_mat","",nbins,x_min,x_max)
h_ZHa35_z_num                 = TH1F("h_ZHa35_z_num","",nbins,x_min,x_max)
h_ZHa35_z_num_presel          = TH1F("h_ZHa35_z_num_presel","",nbins,x_min,x_max)
h_ZHa35_z_num_good            = TH1F("h_ZHa35_z_num_good","",nbins,x_min,x_max)
h_ZHa35_z_num_good_no_mat     = TH1F("h_ZHa35_z_num_good_no_mat","",nbins,x_min,x_max)
h_ZHa25_z_num                 = TH1F("h_ZHa25_z_num","",nbins,x_min,x_max)
h_ZHa25_z_num_presel          = TH1F("h_ZHa25_z_num_presel","",nbins,x_min,x_max)
h_ZHa25_z_num_good            = TH1F("h_ZHa25_z_num_good","",nbins,x_min,x_max)
h_ZHa25_z_num_good_no_mat     = TH1F("h_ZHa25_z_num_good_no_mat","",nbins,x_min,x_max)
h_ZHa15_z_num                 = TH1F("h_ZHa15_z_num","",nbins,x_min,x_max)
h_ZHa15_z_num_presel          = TH1F("h_ZHa15_z_num_presel","",nbins,x_min,x_max)
h_ZHa15_z_num_good            = TH1F("h_ZHa15_z_num_good","",nbins,x_min,x_max)
h_ZHa15_z_num_good_no_mat     = TH1F("h_ZHa15_z_num_good_no_mat","",nbins,x_min,x_max)
h_ZHa55_z_denom = TH1F("h_ZHa55_z_denom","",nbins,x_min,x_max)
h_ZHa35_z_denom = TH1F("h_ZHa35_z_denom","",nbins,x_min,x_max)
h_ZHa25_z_denom = TH1F("h_ZHa25_z_denom","",nbins,x_min,x_max)
h_ZHa15_z_denom = TH1F("h_ZHa15_z_denom","",nbins,x_min,x_max)

t_ZHa55.Draw("truthVtx_r>>+h_ZHa55_r_num","truthVtx_isMatched*truthVtx_isGood*totalEventWeight")
t_ZHa55.Draw("truthVtx_r>>+h_ZHa55_r_num_presel","truthVtx_isMatched_passPresel*truthVtx_isGood*totalEventWeight")
t_ZHa55.Draw("truthVtx_r>>+h_ZHa55_r_num_good","truthVtx_isMatched_passPresel*truthVtx_isMatched_passTracks*truthVtx_isMatched_passMass*truthVtx_isGood*totalEventWeight")
t_ZHa55.Draw("truthVtx_r>>+h_ZHa55_r_num_good_no_mat","truthVtx_isMatched_passFiducial*truthVtx_isMatched_passQuality*truthVtx_isMatched_passTracks*truthVtx_isMatched_passMass*truthVtx_isGood*totalEventWeight")
t_ZHa55.Draw("truthVtx_r>>+h_ZHa55_r_denom","truthVtx_isGood*totalEventWeight")
t_ZHa35.Draw("truthVtx_r>>+h_ZHa35_r_num","truthVtx_isMatched*truthVtx_isGood*totalEventWeight")
t_ZHa35.Draw("truthVtx_r>>+h_ZHa35_r_num_presel","truthVtx_isMatched_passPresel*truthVtx_isGood*totalEventWeight")
t_ZHa35.Draw("truthVtx_r>>+h_ZHa35_r_num_good","truthVtx_isMatched_passPresel*truthVtx_isMatched_passTracks*truthVtx_isMatched_passMass*truthVtx_isGood*totalEventWeight")
t_ZHa35.Draw("truthVtx_r>>+h_ZHa35_r_num_good_no_mat","truthVtx_isMatched_passFiducial*truthVtx_isMatched_passQuality*truthVtx_isMatched_passTracks*truthVtx_isMatched_passMass*truthVtx_isGood*totalEventWeight")
t_ZHa35.Draw("truthVtx_r>>+h_ZHa35_r_denom","truthVtx_isGood*totalEventWeight")
t_ZHa25.Draw("truthVtx_r>>+h_ZHa25_r_num","truthVtx_isMatched*truthVtx_isGood*totalEventWeight")
t_ZHa25.Draw("truthVtx_r>>+h_ZHa25_r_num_presel","truthVtx_isMatched_passPresel*truthVtx_isGood*totalEventWeight")
t_ZHa25.Draw("truthVtx_r>>+h_ZHa25_r_num_good","truthVtx_isMatched_passPresel*truthVtx_isMatched_passTracks*truthVtx_isMatched_passMass*truthVtx_isGood*totalEventWeight")
t_ZHa25.Draw("truthVtx_r>>+h_ZHa25_r_num_good_no_mat","truthVtx_isMatched_passFiducial*truthVtx_isMatched_passQuality*truthVtx_isMatched_passTracks*truthVtx_isMatched_passMass*truthVtx_isGood*totalEventWeight")
t_ZHa25.Draw("truthVtx_r>>+h_ZHa25_r_denom","truthVtx_isGood*totalEventWeight")
t_ZHa15.Draw("truthVtx_r>>+h_ZHa15_r_num","truthVtx_isMatched*truthVtx_isGood*totalEventWeight")
t_ZHa15.Draw("truthVtx_r>>+h_ZHa15_r_num_presel","truthVtx_isMatched_passPresel*truthVtx_isGood*totalEventWeight")
t_ZHa15.Draw("truthVtx_r>>+h_ZHa15_r_num_good","truthVtx_isMatched_passPresel*truthVtx_isMatched_passTracks*truthVtx_isMatched_passMass*truthVtx_isGood*totalEventWeight")
t_ZHa15.Draw("truthVtx_r>>+h_ZHa15_r_num_good_no_mat","truthVtx_isMatched_passFiducial*truthVtx_isMatched_passQuality*truthVtx_isMatched_passTracks*truthVtx_isMatched_passMass*truthVtx_isGood*totalEventWeight")
t_ZHa15.Draw("truthVtx_r>>+h_ZHa15_r_denom","truthVtx_isGood*totalEventWeight")


##################################
###########  z ###################
##################################
t_ZHa55.Draw("truthVtx_z>>+h_ZHa55_z_num","truthVtx_isMatched*truthVtx_isGood*totalEventWeight")
t_ZHa55.Draw("truthVtx_z>>+h_ZHa55_z_num_presel","truthVtx_isMatched_passPresel*truthVtx_isGood*totalEventWeight")
t_ZHa55.Draw("truthVtx_z>>+h_ZHa55_z_num_good","truthVtx_isMatched_passPresel*truthVtx_isMatched_passTracks*truthVtx_isMatched_passMass*truthVtx_isGood*totalEventWeight")
t_ZHa55.Draw("truthVtx_z>>+h_ZHa55_z_num_good_no_mat","truthVtx_isMatched_passFiducial*truthVtx_isMatched_passQuality*truthVtx_isMatched_passTracks*truthVtx_isMatched_passMass*truthVtx_isGood*totalEventWeight")
t_ZHa55.Draw("truthVtx_z>>+h_ZHa55_z_denom","truthVtx_isGood*totalEventWeight")
t_ZHa35.Draw("truthVtx_z>>+h_ZHa35_z_num","truthVtx_isMatched*truthVtx_isGood*totalEventWeight")
t_ZHa35.Draw("truthVtx_z>>+h_ZHa35_z_num_presel","truthVtx_isMatched_passPresel*truthVtx_isGood*totalEventWeight")
t_ZHa35.Draw("truthVtx_z>>+h_ZHa35_z_num_good","truthVtx_isMatched_passPresel*truthVtx_isMatched_passTracks*truthVtx_isMatched_passMass*truthVtx_isGood*totalEventWeight")
t_ZHa35.Draw("truthVtx_z>>+h_ZHa35_z_num_good_no_mat","truthVtx_isMatched_passFiducial*truthVtx_isMatched_passQuality*truthVtx_isMatched_passTracks*truthVtx_isMatched_passMass*truthVtx_isGood*totalEventWeight")
t_ZHa35.Draw("truthVtx_z>>+h_ZHa35_z_denom","truthVtx_isGood*totalEventWeight")
t_ZHa25.Draw("truthVtx_z>>+h_ZHa25_z_num","truthVtx_isMatched*truthVtx_isGood*totalEventWeight")
t_ZHa25.Draw("truthVtx_z>>+h_ZHa25_z_num_presel","truthVtx_isMatched_passPresel*truthVtx_isGood*totalEventWeight")
t_ZHa25.Draw("truthVtx_z>>+h_ZHa25_z_num_good","truthVtx_isMatched_passPresel*truthVtx_isMatched_passTracks*truthVtx_isMatched_passMass*truthVtx_isGood*totalEventWeight")
t_ZHa25.Draw("truthVtx_z>>+h_ZHa25_z_num_good_no_mat","truthVtx_isMatched_passFiducial*truthVtx_isMatched_passQuality*truthVtx_isMatched_passTracks*truthVtx_isMatched_passMass*truthVtx_isGood*totalEventWeight")
t_ZHa25.Draw("truthVtx_z>>+h_ZHa25_z_denom","truthVtx_isGood*totalEventWeight")
t_ZHa15.Draw("truthVtx_z>>+h_ZHa15_z_num","truthVtx_isMatched*truthVtx_isGood*totalEventWeight")
t_ZHa15.Draw("truthVtx_z>>+h_ZHa15_z_num_presel","truthVtx_isMatched_passPresel*truthVtx_isGood*totalEventWeight")
t_ZHa15.Draw("truthVtx_z>>+h_ZHa15_z_num_good","truthVtx_isMatched_passPresel*truthVtx_isMatched_passTracks*truthVtx_isMatched_passMass*truthVtx_isGood*totalEventWeight")
t_ZHa15.Draw("truthVtx_z>>+h_ZHa15_z_num_good_no_mat","truthVtx_isMatched_passFiducial*truthVtx_isMatched_passQuality*truthVtx_isMatched_passTracks*truthVtx_isMatched_passMass*truthVtx_isGood*totalEventWeight")
t_ZHa15.Draw("truthVtx_z>>+h_ZHa15_z_denom","truthVtx_isGood*totalEventWeight")

h_ZHa55_r             = h_ZHa55_r_num.Clone            ( "h_ZHa55_r_ratio")
h_ZHa55_r_presel      = h_ZHa55_r_num_presel.Clone     ( "h_ZHa55_r_ratio_presel")
h_ZHa55_r_good        = h_ZHa55_r_num_good.Clone       ( "h_ZHa55_r_ratio_good")
h_ZHa55_r_good_no_mat = h_ZHa55_r_num_good_no_mat.Clone( "h_ZHa55_r_ratio_good_no_mat")

h_ZHa35_r             = h_ZHa35_r_num.Clone            ( "h_ZHa35_r_ratio")
h_ZHa35_r_presel      = h_ZHa35_r_num_presel.Clone     ( "h_ZHa35_r_ratio_presel")
h_ZHa35_r_good        = h_ZHa35_r_num_good.Clone       ( "h_ZHa35_r_ratio_good")
h_ZHa35_r_good_no_mat = h_ZHa35_r_num_good_no_mat.Clone( "h_ZHa35_r_ratio_good_no_mat")

h_ZHa25_r             = h_ZHa25_r_num.Clone            ( "h_ZHa25_r_ratio")
h_ZHa25_r_presel      = h_ZHa25_r_num_presel.Clone     ( "h_ZHa25_r_ratio_presel")
h_ZHa25_r_good        = h_ZHa25_r_num_good.Clone       ( "h_ZHa25_r_ratio_good")
h_ZHa25_r_good_no_mat = h_ZHa25_r_num_good_no_mat.Clone( "h_ZHa25_r_ratio_good_no_mat")

h_ZHa15_r             = h_ZHa15_r_num.Clone            ( "h_ZHa15_r_ratio")
h_ZHa15_r_presel      = h_ZHa15_r_num_presel.Clone     ( "h_ZHa15_r_ratio_presel")
h_ZHa15_r_good        = h_ZHa15_r_num_good.Clone       ( "h_ZHa15_r_ratio_good")
h_ZHa15_r_good_no_mat = h_ZHa15_r_num_good_no_mat.Clone( "h_ZHa15_r_ratio_good_no_mat")

##############################
############ z ###############
##############################

h_ZHa55_z             = h_ZHa55_z_num.Clone            ( "h_ZHa55_z_ratio")
h_ZHa55_z_presel      = h_ZHa55_z_num_presel.Clone     ( "h_ZHa55_z_ratio_presel")
h_ZHa55_z_good        = h_ZHa55_z_num_good.Clone       ( "h_ZHa55_z_ratio_good")
h_ZHa55_z_good_no_mat = h_ZHa55_z_num_good_no_mat.Clone( "h_ZHa55_z_ratio_good_no_mat")

h_ZHa35_z             = h_ZHa35_z_num.Clone            ( "h_ZHa35_z_ratio")
h_ZHa35_z_presel      = h_ZHa35_z_num_presel.Clone     ( "h_ZHa35_z_ratio_presel")
h_ZHa35_z_good        = h_ZHa35_z_num_good.Clone       ( "h_ZHa35_z_ratio_good")
h_ZHa35_z_good_no_mat = h_ZHa35_z_num_good_no_mat.Clone( "h_ZHa35_z_ratio_good_no_mat")

h_ZHa25_z             = h_ZHa25_z_num.Clone            ( "h_ZHa25_z_ratio")
h_ZHa25_z_presel      = h_ZHa25_z_num_presel.Clone     ( "h_ZHa25_z_ratio_presel")
h_ZHa25_z_good        = h_ZHa25_z_num_good.Clone       ( "h_ZHa25_z_ratio_good")
h_ZHa25_z_good_no_mat = h_ZHa25_z_num_good_no_mat.Clone( "h_ZHa25_z_ratio_good_no_mat")

h_ZHa15_z             = h_ZHa15_z_num.Clone            ( "h_ZHa15_z_ratio")
h_ZHa15_z_presel      = h_ZHa15_z_num_presel.Clone     ( "h_ZHa15_z_ratio_presel")
h_ZHa15_z_good        = h_ZHa15_z_num_good.Clone       ( "h_ZHa15_z_ratio_good")
h_ZHa15_z_good_no_mat = h_ZHa15_z_num_good_no_mat.Clone( "h_ZHa15_z_ratio_good_no_mat")

h_ZHa55_r.Divide            (h_ZHa55_r_denom)
h_ZHa55_r_presel.Divide     (h_ZHa55_r_denom)
h_ZHa55_r_good.Divide       (h_ZHa55_r_denom)
h_ZHa55_r_good_no_mat.Divide(h_ZHa55_r_denom)

h_ZHa35_r.Divide            (h_ZHa35_r_denom)
h_ZHa35_r_presel.Divide     (h_ZHa35_r_denom)
h_ZHa35_r_good.Divide       (h_ZHa35_r_denom)
h_ZHa35_r_good_no_mat.Divide(h_ZHa35_r_denom)

h_ZHa25_r.Divide            (h_ZHa25_r_denom)
h_ZHa25_r_presel.Divide     (h_ZHa25_r_denom)
h_ZHa25_r_good.Divide       (h_ZHa25_r_denom)
h_ZHa25_r_good_no_mat.Divide(h_ZHa25_r_denom)

h_ZHa15_r.Divide            (h_ZHa15_r_denom)
h_ZHa15_r_presel.Divide     (h_ZHa15_r_denom)
h_ZHa15_r_good.Divide       (h_ZHa15_r_denom)
h_ZHa15_r_good_no_mat.Divide(h_ZHa15_r_denom)

h_ZHa55_z.Divide            (h_ZHa55_z_denom)
h_ZHa55_z_presel.Divide     (h_ZHa55_z_denom)
h_ZHa55_z_good.Divide       (h_ZHa55_z_denom)
h_ZHa55_z_good_no_mat.Divide(h_ZHa55_z_denom)

h_ZHa35_z.Divide            (h_ZHa35_z_denom)
h_ZHa35_z_presel.Divide     (h_ZHa35_z_denom)
h_ZHa35_z_good.Divide       (h_ZHa35_z_denom)
h_ZHa35_z_good_no_mat.Divide(h_ZHa35_z_denom)

h_ZHa25_z.Divide            (h_ZHa25_z_denom)
h_ZHa25_z_presel.Divide     (h_ZHa25_z_denom)
h_ZHa25_z_good.Divide       (h_ZHa25_z_denom)
h_ZHa25_z_good_no_mat.Divide(h_ZHa25_z_denom)

h_ZHa15_z.Divide            (h_ZHa15_z_denom)
h_ZHa15_z_presel.Divide     (h_ZHa15_z_denom)
h_ZHa15_z_good.Divide       (h_ZHa15_z_denom)
h_ZHa15_z_good_no_mat.Divide(h_ZHa15_z_denom)

#########################################
############ Sample Compare #############
#########################################

Eff1D( hists = [h_ZHa15_r, h_ZHa25_r, h_ZHa35_r, h_ZHa55_r],
       types = ["ZH_a15a15_4b_ctau10", "ZH_a25a25_4b_ctau10", "ZH_a35a35_4b_ctau10", "ZH_a55a55_4b_ctau10"],
       legends = ["m_{a} = 15 GeV","m_{a} = 25 GeV","m_{a} = 35 GeV","m_{a} = 55 GeV"],
       name = "AllEff_r_all",
       x_title = "LLP L_{xy}",
       x_units = "mm",
       y_min = 0.0,
       log_scale_y = False,
       y_title = "Vertex Reconstruction Efficiency",
       lumi_val = "140",
       hide_lumi = False,
       norm = False,
       extra_lines_loc = [0.17,0.775],
       extra_legend_lines = ["All vertices"] )
Eff1D( hists = [h_ZHa15_z, h_ZHa25_z, h_ZHa35_z, h_ZHa55_z],
       types = ["ZH_a15a15_4b_ctau10", "ZH_a25a25_4b_ctau10", "ZH_a35a35_4b_ctau10", "ZH_a55a55_4b_ctau10"],
       legends = ["m_{a} = 15 GeV","m_{a} = 25 GeV","m_{a} = 35 GeV","m_{a} = 55 GeV"],
       name = "AllEff_z_all",
       x_title = "LLP z",
       x_units = "mm",
       y_min = 0.0,
       log_scale_y = False,
       y_title = "Vertex Reconstruction Efficiency",
       lumi_val = "140",
       hide_lumi = False,
       norm = False,
       extra_lines_loc = [0.17,0.775],
       extra_legend_lines = ["All vertices"] )
Eff1D( hists = [h_ZHa15_r_presel, h_ZHa25_r_presel, h_ZHa35_r_presel, h_ZHa55_r_presel],
       types = ["ZH_a15a15_4b_ctau10", "ZH_a25a25_4b_ctau10", "ZH_a35a35_4b_ctau10", "ZH_a55a55_4b_ctau10"],
       legends = ["m_{a} = 15 GeV","m_{a} = 25 GeV","m_{a} = 35 GeV","m_{a} = 55 GeV"],
       name = "AllEff_r_presel",
       x_title = "LLP L_{xy}",
       x_units = "mm",
       y_min = 0.0,
       log_scale_y = False,
       y_title = "Vertex Reconstruction Efficiency",
       lumi_val = "140",
       hide_lumi = False,
       norm = False,
       extra_lines_loc = [0.17,0.775],
       extra_legend_lines = ["Preselection vertices"] )
Eff1D( hists = [h_ZHa15_z_presel, h_ZHa25_z_presel, h_ZHa35_z_presel, h_ZHa55_z_presel],
       types = ["ZH_a15a15_4b_ctau10", "ZH_a25a25_4b_ctau10", "ZH_a35a35_4b_ctau10", "ZH_a55a55_4b_ctau10"],
       legends = ["m_{a} = 15 GeV","m_{a} = 25 GeV","m_{a} = 35 GeV","m_{a} = 55 GeV"],
       name = "AllEff_z_presel",
       x_title = "LLP z",
       x_units = "mm",
       y_min = 0.0,
       log_scale_y = False,
       y_title = "Vertex Reconstruction Efficiency",
       lumi_val = "140",
       hide_lumi = False,
       norm = False,
       extra_lines_loc = [0.17,0.775],
       extra_legend_lines = ["Preselection vertices"] )
Eff1D( hists = [h_ZHa15_r_good, h_ZHa25_r_good, h_ZHa35_r_good, h_ZHa55_r_good],
       types = ["ZH_a15a15_4b_ctau10", "ZH_a25a25_4b_ctau10", "ZH_a35a35_4b_ctau10", "ZH_a55a55_4b_ctau10"],
       legends = ["m_{a} = 15 GeV","m_{a} = 25 GeV","m_{a} = 35 GeV","m_{a} = 55 GeV"],
       name = "AllEff_r_good",
       x_title = "LLP L_{xy}",
       x_units = "mm",
       y_min = 0.0,
       log_scale_y = False,
       y_title = "Vertex Reconstruction Efficiency",
       lumi_val = "140",
       hide_lumi = False,
       norm = False,
       extra_lines_loc = [0.17,0.775],
       extra_legend_lines = ["Good vertices"] )
Eff1D( hists = [h_ZHa15_z_good, h_ZHa25_z_good, h_ZHa35_z_good, h_ZHa55_z_good],
       types = ["ZH_a15a15_4b_ctau10", "ZH_a25a25_4b_ctau10", "ZH_a35a35_4b_ctau10", "ZH_a55a55_4b_ctau10"],
       legends = ["m_{a} = 15 GeV","m_{a} = 25 GeV","m_{a} = 35 GeV","m_{a} = 55 GeV"],
       name = "AllEff_z_good",
       x_title = "LLP z",
       x_units = "mm",
       y_min = 0.0,
       log_scale_y = False,
       y_title = "Vertex Reconstruction Efficiency",
       lumi_val = "140",
       hide_lumi = False,
       norm = False,
       extra_lines_loc = [0.17,0.775],
       extra_legend_lines = ["Good vertices"] )

########################################
############ Selection Compare #########
########################################
h_ZHa55_r             = h_ZHa55_r_num.Clone            ( "h_ZHa55_r_ratio")
h_ZHa55_r_presel      = h_ZHa55_r_num_presel.Clone     ( "h_ZHa55_r_ratio_presel")
h_ZHa55_r_good        = h_ZHa55_r_num_good.Clone       ( "h_ZHa55_r_ratio_good")
h_ZHa55_r_good_no_mat = h_ZHa55_r_num_good_no_mat.Clone( "h_ZHa55_r_ratio_good_no_mat")

h_ZHa35_r             = h_ZHa35_r_num.Clone            ( "h_ZHa35_r_ratio")
h_ZHa35_r_presel      = h_ZHa35_r_num_presel.Clone     ( "h_ZHa35_r_ratio_presel")
h_ZHa35_r_good        = h_ZHa35_r_num_good.Clone       ( "h_ZHa35_r_ratio_good")
h_ZHa35_r_good_no_mat = h_ZHa35_r_num_good_no_mat.Clone( "h_ZHa35_r_ratio_good_no_mat")

h_ZHa25_r             = h_ZHa25_r_num.Clone            ( "h_ZHa25_r_ratio")
h_ZHa25_r_presel      = h_ZHa25_r_num_presel.Clone     ( "h_ZHa25_r_ratio_presel")
h_ZHa25_r_good        = h_ZHa25_r_num_good.Clone       ( "h_ZHa25_r_ratio_good")
h_ZHa25_r_good_no_mat = h_ZHa25_r_num_good_no_mat.Clone( "h_ZHa25_r_ratio_good_no_mat")

h_ZHa15_r             = h_ZHa15_r_num.Clone            ( "h_ZHa15_r_ratio")
h_ZHa15_r_presel      = h_ZHa15_r_num_presel.Clone     ( "h_ZHa15_r_ratio_presel")
h_ZHa15_r_good        = h_ZHa15_r_num_good.Clone       ( "h_ZHa15_r_ratio_good")
h_ZHa15_r_good_no_mat = h_ZHa15_r_num_good_no_mat.Clone( "h_ZHa15_r_ratio_good_no_mat")

##############################
############ z ###############
##############################

h_ZHa55_z             = h_ZHa55_z_num.Clone            ( "h_ZHa55_z_ratio")
h_ZHa55_z_presel      = h_ZHa55_z_num_presel.Clone     ( "h_ZHa55_z_ratio_presel")
h_ZHa55_z_good        = h_ZHa55_z_num_good.Clone       ( "h_ZHa55_z_ratio_good")
h_ZHa55_z_good_no_mat = h_ZHa55_z_num_good_no_mat.Clone( "h_ZHa55_z_ratio_good_no_mat")

h_ZHa35_z             = h_ZHa35_z_num.Clone            ( "h_ZHa35_z_ratio")
h_ZHa35_z_presel      = h_ZHa35_z_num_presel.Clone     ( "h_ZHa35_z_ratio_presel")
h_ZHa35_z_good        = h_ZHa35_z_num_good.Clone       ( "h_ZHa35_z_ratio_good")
h_ZHa35_z_good_no_mat = h_ZHa35_z_num_good_no_mat.Clone( "h_ZHa35_z_ratio_good_no_mat")

h_ZHa25_z             = h_ZHa25_z_num.Clone            ( "h_ZHa25_z_ratio")
h_ZHa25_z_presel      = h_ZHa25_z_num_presel.Clone     ( "h_ZHa25_z_ratio_presel")
h_ZHa25_z_good        = h_ZHa25_z_num_good.Clone       ( "h_ZHa25_z_ratio_good")
h_ZHa25_z_good_no_mat = h_ZHa25_z_num_good_no_mat.Clone( "h_ZHa25_z_ratio_good_no_mat")

h_ZHa15_z             = h_ZHa15_z_num.Clone            ( "h_ZHa15_z_ratio")
h_ZHa15_z_presel      = h_ZHa15_z_num_presel.Clone     ( "h_ZHa15_z_ratio_presel")
h_ZHa15_z_good        = h_ZHa15_z_num_good.Clone       ( "h_ZHa15_z_ratio_good")
h_ZHa15_z_good_no_mat = h_ZHa15_z_num_good_no_mat.Clone( "h_ZHa15_z_ratio_good_no_mat")

h_ZHa55_r.Divide            (h_ZHa55_r_denom)
h_ZHa55_r_presel.Divide     (h_ZHa55_r_denom)
h_ZHa55_r_good.Divide       (h_ZHa55_r_denom)
h_ZHa55_r_good_no_mat.Divide(h_ZHa55_r_denom)

h_ZHa35_r.Divide            (h_ZHa35_r_denom)
h_ZHa35_r_presel.Divide     (h_ZHa35_r_denom)
h_ZHa35_r_good.Divide       (h_ZHa35_r_denom)
h_ZHa35_r_good_no_mat.Divide(h_ZHa35_r_denom)

h_ZHa25_r.Divide            (h_ZHa25_r_denom)
h_ZHa25_r_presel.Divide     (h_ZHa25_r_denom)
h_ZHa25_r_good.Divide       (h_ZHa25_r_denom)
h_ZHa25_r_good_no_mat.Divide(h_ZHa25_r_denom)

h_ZHa15_r.Divide            (h_ZHa15_r_denom)
h_ZHa15_r_presel.Divide     (h_ZHa15_r_denom)
h_ZHa15_r_good.Divide       (h_ZHa15_r_denom)
h_ZHa15_r_good_no_mat.Divide(h_ZHa15_r_denom)

h_ZHa55_z.Divide            (h_ZHa55_z_denom)
h_ZHa55_z_presel.Divide     (h_ZHa55_z_denom)
h_ZHa55_z_good.Divide       (h_ZHa55_z_denom)
h_ZHa55_z_good_no_mat.Divide(h_ZHa55_z_denom)

h_ZHa35_z.Divide            (h_ZHa35_z_denom)
h_ZHa35_z_presel.Divide     (h_ZHa35_z_denom)
h_ZHa35_z_good.Divide       (h_ZHa35_z_denom)
h_ZHa35_z_good_no_mat.Divide(h_ZHa35_z_denom)

h_ZHa25_z.Divide            (h_ZHa25_z_denom)
h_ZHa25_z_presel.Divide     (h_ZHa25_z_denom)
h_ZHa25_z_good.Divide       (h_ZHa25_z_denom)
h_ZHa25_z_good_no_mat.Divide(h_ZHa25_z_denom)

h_ZHa15_z.Divide            (h_ZHa15_z_denom)
h_ZHa15_z_presel.Divide     (h_ZHa15_z_denom)
h_ZHa15_z_good.Divide       (h_ZHa15_z_denom)
h_ZHa15_z_good_no_mat.Divide(h_ZHa15_z_denom)
Eff1D( hists = [h_ZHa55_r, h_ZHa55_r_presel, h_ZHa55_r_good],
       types = ["ZH_a55a55_4b_ctau10", "ZH_a55a55_4b_ctau100", "ZH_a55a55_4b_ctau1000"],
       legends = ["All vertices", "Preselection", "Good"],
       name = "ZHa55Eff_r",
       x_title = "LLP L_{xy}",
       x_units = "mm",
       y_min = 0.0,
       log_scale_y = False,
       y_title = "Vertex Reconstruction Efficiency",
       lumi_val = "140",
       hide_lumi = False,
       norm = False,
       extra_lines_loc = [0.17,0.775],
       extra_legend_lines = ["m_{a} = 55 GeV"] )
Eff1D( hists = [h_ZHa55_z, h_ZHa55_z_presel, h_ZHa55_z_good],
       types = ["ZH_a55a55_4b_ctau10", "ZH_a55a55_4b_ctau100", "ZH_a55a55_4b_ctau1000"],
       legends = ["All vertices", "Preselection", "Good"],
       name = "ZHa55Eff_z",
       x_title = "LLP z",
       x_units = "mm",
       y_min = 0.0,
       log_scale_y = False,
       y_title = "Vertex Reconstruction Efficiency",
       lumi_val = "140",
       hide_lumi = False,
       norm = False,
       extra_lines_loc = [0.17,0.775],
       extra_legend_lines = ["m_{a} = 55 GeV"] )
Eff1D( hists = [h_ZHa35_r, h_ZHa35_r_presel, h_ZHa35_r_good],
       types = ["ZH_a35a35_4b_ctau10", "ZH_a35a35_4b_ctau100", "ZH_a35a35_4b_ctau1000"],
       legends = ["All vertices", "Preselection", "Good"],
       name = "ZHa35Eff_r",
       x_title = "LLP L_{xy}",
       x_units = "mm",
       y_min = 0.0,
       log_scale_y = False,
       y_title = "Vertex Reconstruction Efficiency",
       lumi_val = "140",
       hide_lumi = False,
       norm = False,
       extra_lines_loc = [0.17,0.775],
       extra_legend_lines = ["m_{a} = 35 GeV"] )
Eff1D( hists = [h_ZHa35_z, h_ZHa35_z_presel, h_ZHa35_z_good],
       types = ["ZH_a35a35_4b_ctau10", "ZH_a35a35_4b_ctau100", "ZH_a35a35_4b_ctau1000"],
       legends = ["All vertices", "Preselection", "Good"],
       name = "ZHa35Eff_z",
       x_title = "LLP z",
       x_units = "mm",
       y_min = 0.0,
       log_scale_y = False,
       y_title = "Vertex Reconstruction Efficiency",
       lumi_val = "140",
       hide_lumi = False,
       norm = False,
       extra_lines_loc = [0.17,0.775],
       extra_legend_lines = ["m_{a} = 35 GeV"] )
Eff1D( hists = [h_ZHa25_r, h_ZHa25_r_presel, h_ZHa25_r_good],
       types = ["ZH_a25a25_4b_ctau10", "ZH_a25a25_4b_ctau100", "ZH_a25a25_4b_ctau1000"],
       legends = ["All vertices", "Preselection", "Good"],
       name = "ZHa25Eff_r",
       x_title = "LLP L_{xy}",
       x_units = "mm",
       y_min = 0.0,
       log_scale_y = False,
       y_title = "Vertex Reconstruction Efficiency",
       lumi_val = "140",
       hide_lumi = False,
       norm = False,
       extra_lines_loc = [0.17,0.775],
       extra_legend_lines = ["m_{a} = 25 GeV"] )
Eff1D( hists = [h_ZHa25_z, h_ZHa25_z_presel, h_ZHa25_z_good],
       types = ["ZH_a25a25_4b_ctau10", "ZH_a25a25_4b_ctau100", "ZH_a25a25_4b_ctau1000"],
       legends = ["All vertices", "Preselection", "Good"],
       name = "ZHa25Eff_z",
       x_title = "LLP z",
       x_units = "mm",
       y_min = 0.0,
       log_scale_y = False,
       y_title = "Vertex Reconstruction Efficiency",
       lumi_val = "140",
       hide_lumi = False,
       norm = False,
       extra_lines_loc = [0.17,0.775],
       extra_legend_lines = ["m_{a} = 25 GeV"] )
Eff1D( hists = [h_ZHa15_r, h_ZHa15_r_presel, h_ZHa15_r_good],
       types = ["ZH_a15a15_4b_ctau10", "ZH_a15a15_4b_ctau100", "ZH_a15a15_4b_ctau1000"],
       legends = ["All vertices", "Preselection", "Good"],
       name = "ZHa15Eff_r",
       x_title = "LLP L_{xy}",
       x_units = "mm",
       y_min = 0.0,
       log_scale_y = False,
       y_title = "Vertex Reconstruction Efficiency",
       lumi_val = "140",
       hide_lumi = False,
       norm = False,
       extra_lines_loc = [0.17,0.775],
       extra_legend_lines = ["m_{a} = 15 GeV"] )
Eff1D( hists = [h_ZHa15_z, h_ZHa15_z_presel, h_ZHa15_z_good],
       types = ["ZH_a15a15_4b_ctau10", "ZH_a15a15_4b_ctau100", "ZH_a15a15_4b_ctau1000"],
       legends = ["All vertices", "Preselection", "Good"],
       name = "ZHa15Eff_z",
       x_title = "LLP z",
       x_units = "mm",
       y_min = 0.0,
       log_scale_y = False,
       y_title = "Vertex Reconstruction Efficiency",
       lumi_val = "140",
       hide_lumi = False,
       norm = False,
       extra_lines_loc = [0.17,0.775],
       extra_legend_lines = ["m_{a} = 15 GeV"] )
