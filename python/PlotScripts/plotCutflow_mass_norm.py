from plot_classes import Hist1D
from ROOT import TFile


ZHa55_10 = TFile("/data/jburzyns/NtupleReader/ZH_a55a55_4b_ctau10.root")
ZHa35_10 = TFile("/data/jburzyns/NtupleReader/ZH_a35a35_4b_ctau10.root")
ZHa25_10 = TFile("/data/jburzyns/NtupleReader/ZH_a25a25_4b_ctau10.root")
ZHa15_10 = TFile("/data/jburzyns/NtupleReader/ZH_a15a15_4b_ctau10.root")
ZHa55_100 = TFile("/data/jburzyns/NtupleReader/ZH_a55a55_4b_ctau100.root")
ZHa35_100 = TFile("/data/jburzyns/NtupleReader/ZH_a35a35_4b_ctau100.root")
ZHa25_100 = TFile("/data/jburzyns/NtupleReader/ZH_a25a25_4b_ctau100.root")
ZHa15_100 = TFile("/data/jburzyns/NtupleReader/ZH_a15a15_4b_ctau100.root")
ZHa55_1000 = TFile("/data/jburzyns/NtupleReader/ZH_a55a55_4b_ctau1000.root")
ZHa35_1000 = TFile("/data/jburzyns/NtupleReader/ZH_a35a35_4b_ctau1000.root")
ZHa25_1000 = TFile("/data/jburzyns/NtupleReader/ZH_a25a25_4b_ctau1000.root")
ZHa15_1000 = TFile("/data/jburzyns/NtupleReader/ZH_a15a15_4b_ctau1000.root")

h_ZHa55_10 = ZHa55_10.Get("cutflow_weighted")
h_ZHa35_10 = ZHa35_10.Get("cutflow_weighted")
h_ZHa25_10 = ZHa25_10.Get("cutflow_weighted")
h_ZHa15_10 = ZHa15_10.Get("cutflow_weighted")

h_ZHa55_100 = ZHa55_100.Get("cutflow_weighted")
h_ZHa35_100 = ZHa35_100.Get("cutflow_weighted")
h_ZHa25_100 = ZHa25_100.Get("cutflow_weighted")
h_ZHa15_100 = ZHa15_100.Get("cutflow_weighted")

h_ZHa55_1000 = ZHa55_1000.Get("cutflow_weighted")
h_ZHa35_1000 = ZHa35_1000.Get("cutflow_weighted")
h_ZHa25_1000 = ZHa25_1000.Get("cutflow_weighted")
h_ZHa15_1000 = ZHa15_1000.Get("cutflow_weighted")

histos_10   = [h_ZHa55_10, h_ZHa35_10, h_ZHa25_10, h_ZHa15_10]
histos_100  = [h_ZHa55_100, h_ZHa35_100, h_ZHa25_100, h_ZHa15_100]
histos_1000 = [h_ZHa55_1000, h_ZHa35_1000, h_ZHa25_1000, h_ZHa15_1000]

histos_55   = [h_ZHa55_10, h_ZHa55_100, h_ZHa55_1000]
histos_35   = [h_ZHa35_10, h_ZHa35_100, h_ZHa35_1000]
histos_25   = [h_ZHa25_10, h_ZHa25_100, h_ZHa25_1000]
histos_15   = [h_ZHa15_10, h_ZHa15_100, h_ZHa15_1000]

keys_ctau10 = ["ZH_a55a55_4b_ctau10","ZH_a35a35_4b_ctau10", "ZH_a25a25_4b_ctau10", "ZH_a15a15_4b_ctau10"]
keys_ctau100 = ["ZH_a55a55_4b_ctau100","ZH_a35a35_4b_ctau100", "ZH_a25a25_4b_ctau100", "ZH_a15a15_4b_ctau100"]
keys_ctau1000 = ["ZH_a55a55_4b_ctau1000","ZH_a35a35_4b_ctau1000", "ZH_a25a25_4b_ctau1000", "ZH_a15a15_4b_ctau10"]

keys_55 = ["ZH_a55a55_4b_ctau10","ZH_a55a55_4b_ctau100", "ZH_a55a55_4b_ctau1000"]
keys_35 = ["ZH_a35a35_4b_ctau10","ZH_a35a35_4b_ctau100", "ZH_a35a35_4b_ctau1000"]
keys_25 = ["ZH_a25a25_4b_ctau10","ZH_a25a25_4b_ctau100", "ZH_a25a25_4b_ctau1000"]
keys_15 = ["ZH_a15a15_4b_ctau10","ZH_a15a15_4b_ctau100", "ZH_a15a15_4b_ctau1000"]

legends = {
  "ZJets" : "SHERPA Z+jets",
  "WH_a55a55_4b_ctau1" : "m_{a}, c#tau = [55,1]",
  "WH_a55a55_4b_ctau10" : "m_{a}, c#tau = [55,10]",
  "WH_a55a55_4b_ctau100" : "m_{a}, c#tau = [55,100]",
  "WH_a15a15_4b_ctau1" : "m_{a}, c#tau = [15,1]",
  "WH_a15a15_4b_ctau10" : "m_{a}, c#tau = [15,10]",
  "WH_a15a15_4b_ctau100" : "m_{a}, c#tau = [15,100]",
  "ZH_a55a55_4b_ctau10" : "m_{a}, c#tau = [55,10]",
  "ZH_a55a55_4b_ctau100" : "m_{a}, c#tau = [55,100]",
  "ZH_a55a55_4b_ctau1000" : "m_{a}, c#tau = [55,1000]",
  "ZH_a35a35_4b_ctau10" : "m_{a}, c#tau = [35,10]",
  "ZH_a35a35_4b_ctau100" : "m_{a}, c#tau = [35,100]",
  "ZH_a35a35_4b_ctau1000" : "m_{a}, c#tau = [35,1000]",
  "ZH_a25a25_4b_ctau10" : "m_{a}, c#tau = [25,10]",
  "ZH_a25a25_4b_ctau100" : "m_{a}, c#tau = [25,100]",
  "ZH_a25a25_4b_ctau1000" : "m_{a}, c#tau = [25,1000]",
  "ZH_a15a15_4b_ctau10" : "m_{a}, c#tau = [15,10]",
  "ZH_a15a15_4b_ctau100" : "m_{a}, c#tau = [15,100]",
  "ZH_a15a15_4b_ctau1000" : "m_{a}, c#tau = [15,1000]",
}


legs_ctau10 = []
legs_ctau100 = []
legs_ctau1000 = []
for key in keys_ctau10:
  legs_ctau10.append(legends[key])
for key in keys_ctau100:
  legs_ctau100.append(legends[key])
for key in keys_ctau1000:
  legs_ctau1000.append(legends[key])

legs_55 = []
legs_35 = []
legs_25 = []
legs_15 = []
for key in keys_55:
  legs_55.append(legends[key])
for key in keys_35:
  legs_35.append(legends[key])
for key in keys_25:
  legs_25.append(legends[key])
for key in keys_15:
  legs_15.append(legends[key])

###########################################
################# 55 GeV ##################
###########################################

for hist in histos_55:
  hist.Scale(1.0/hist.GetBinContent(1))

Hist1D( hists = histos_55,
        types = keys_55,
        legends = legs_55,
        name = "cutflow_norm_ma55",
        x_title = "",
        x_units = "",
        y_min = 0.00001,
        log_scale_y = True,
        y_axis_type = "Events",
        lumi_val = "140",
        hide_lumi = False,
        norm = False,
        extra_lines_loc = [0.17,0.775],
        extra_legend_lines = ["m_{a} = 55 GeV"] )

###########################################
################# 35 GeV ##################
###########################################

for hist in histos_35:
  hist.Scale(1.0/hist.GetBinContent(1))

Hist1D( hists = histos_35,
        types = keys_35,
        legends = legs_35,
        name = "cutflow_norm_ma35",
        x_title = "",
        x_units = "",
        y_min = 0.00001,
        log_scale_y = True,
        y_axis_type = "Events",
        lumi_val = "140",
        hide_lumi = False,
        norm = False,
        extra_lines_loc = [0.17,0.775],
        extra_legend_lines = ["m_{a} = 35 GeV"] )

###########################################
################# 25 GeV ##################
###########################################

for hist in histos_25:
  hist.Scale(1.0/hist.GetBinContent(1))

Hist1D( hists = histos_25,
        types = keys_25,
        legends = legs_25,
        name = "cutflow_norm_ma25",
        x_title = "",
        x_units = "",
        y_min = 0.00001,
        log_scale_y = True,
        y_axis_type = "Events",
        lumi_val = "140",
        hide_lumi = False,
        norm = False,
        extra_lines_loc = [0.17,0.775],
        extra_legend_lines = ["m_{a} = 25 GeV"] )

###########################################
################# 15 GeV ##################
###########################################

for hist in histos_15:
  hist.Scale(1.0/hist.GetBinContent(1))

Hist1D( hists = histos_15,
        types = keys_15,
        legends = legs_15,
        name = "cutflow_norm_ma15",
        x_title = "",
        x_units = "",
        y_min = 0.00001,
        log_scale_y = True,
        y_axis_type = "Events",
        lumi_val = "140",
        hide_lumi = False,
        norm = False,
        extra_lines_loc = [0.17,0.775],
        extra_legend_lines = ["m_{a} = 15 GeV"] )
