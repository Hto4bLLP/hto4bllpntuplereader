from ROOT import TH1F, TTree, TFile
from plot_classes import Hist1D


f_data = TFile("/data/jburzyns/NtupleReader/data18PeriodB.root")
t_data = f_data.Get("tree")

h_secVtx_r      = TH1F("h_all","",60,0,300)
h_secVtx_r_veto = TH1F("h_mat","",60,0,300)

t_data.Draw("secVtx_r>>+h_all")
t_data.Draw("secVtx_r>>+h_mat","secVtx_passMaterial")


Hist1D( hists = [h_secVtx_r,h_secVtx_r_veto],
        types = ["data","ZJets"],
        legends = ["All vertices","Vertices pass material veto"],
        name = "vtx_r",
        x_title = "L_{xy}",
        x_units = "mm",
        y_min = 0.0,
        log_scale_y = False,
        y_axis_type = "Vertices",
        lumi_val = "3.9",
        hide_lumi = False,
        norm = False,
        extra_lines_loc = [0.17,0.775],
        extra_legend_lines = [""] )
