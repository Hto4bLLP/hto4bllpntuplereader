from ROOT import *
from math import *
import array as arr
import os
import random
import string


def make_dir(dir_path):
    try:
        os.makedirs(dir_path)
    except OSError:
        if not os.path.isdir(dir_path):
            raise

def get_closest_match(full_name, match_dict):
    tmp_key_match = ""
    tmp_val_match = ""
    for k, v in match_dict.iteritems():
        if k in full_name and len(k) > len(tmp_key_match):
            tmp_key_match = k
            tmp_val_match = v
    return tmp_val_match

def set_style_ratio(hist, y_title = "Data/Pred.", y_min = 0.5, y_max = 1.5):
     hist.GetYaxis().SetRangeUser(y_min, y_max)
     hist.GetYaxis().SetNdivisions(504, 0)
     hist.GetYaxis().SetTitle(y_title)
     hist.GetYaxis().CenterTitle()

def format_bin_width(bin_spacing):
    if bin_spacing < 0.5:
        return str(round(bin_spacing * 20) / 20)
    elif bin_spacing < 1.0:
        return str(round(bin_spacing * 10) / 10)
    elif bin_spacing < 10:
        if (int(bin_spacing) == bin_spacing):
            return str(int(round(bin_spacing * 4) / 4))
        else:
            return str(round(bin_spacing * 4) / 4)
    elif bin_spacing < 100:
        return str(round(bin_spacing * 2) / 2)
    else:
        return str(int(round(bin_spacing)))


def format_for_drawing_stack(histo, hist_type = "data"):

    color_dict  = {
      "WH_a15a15_4b_ctau10":kAzure+5,
      "WH_a55a55_4b_ctau10":kViolet+5, 
      "WH_a15a15_4b_ctau100":kPink+5,
      "WH_a55a55_4b_ctau100":kOrange+5, 
      "WH_a15a15_4b_ctau1":kBlue,
      "WH_a55a55_4b_ctau1":kRed, 
      "ZH_a15a15_4b_ctau10":kAzure+5,
      "ZH_a55a55_4b_ctau10":kViolet+5, 
      "ZH_a15a15_4b_ctau100":kPink+5,
      "ZH_a55a55_4b_ctau100":kOrange+5, 
      "ZH_a15a15_4b_ctau1":kBlue,
      "ZH_a55a55_4b_ctau1":kRed, 
      "data":kWhite
    }
    marker_dict  = {
      "WH_a15a15_4b_ctau10":21,
      "WH_a55a55_4b_ctau10":22, 
      "WH_a15a15_4b_ctau100":23,
      "WH_a55a55_4b_ctau100":24, 
      "WH_a15a15_4b_ctau1":25,
      "WH_a55a55_4b_ctau1":26, 
      "ZH_a15a15_4b_ctau10":21,
      "ZH_a55a55_4b_ctau10":22, 
      "ZH_a15a15_4b_ctau100":23,
      "ZH_a55a55_4b_ctau100":24, 
      "ZH_a15a15_4b_ctau1":25,
      "ZH_a55a55_4b_ctau1":26, 
      "data":20
    }

    histo.SetLineColor( kBlack )
    histo.SetFillColor( color_dict[hist_type] )
    histo.SetMarkerColor( color_dict[hist_type] )
    histo.SetMarkerStyle( marker_dict[hist_type] )
    histo.GetXaxis().SetTitleOffset(1.2);
    histo.GetYaxis().SetTitleOffset(1.2);
    histo.GetXaxis().SetTitleSize(.05);
    histo.GetYaxis().SetTitleSize(.05);
    histo.GetXaxis().SetLabelSize(.05);
    histo.GetYaxis().SetLabelSize(.05);
    histo.GetXaxis().SetLabelOffset(0.01);

def format_for_drawing(histo, hist_type = "data"):

    color_dict  = {
      "ZH_a15a15_4b_ctau10":kAzure+5,
      "ZH_a15a15_4b_ctau100":kAzure+3,
      "ZH_a15a15_4b_ctau1000":kAzure+10,
      "ZH_a25a25_4b_ctau10":kViolet+5, 
      "ZH_a25a25_4b_ctau100":kViolet+3, 
      "ZH_a25a25_4b_ctau1000":kViolet+10, 
      "ZH_a35a35_4b_ctau10":kPink+5,
      "ZH_a35a35_4b_ctau100":kPink+3,
      "ZH_a35a35_4b_ctau1000":kPink+10,
      "ZH_a55a55_4b_ctau10":kOrange+5, 
      "ZH_a55a55_4b_ctau100":kOrange+3,
      "ZH_a55a55_4b_ctau1000":kOrange+10, 
      "data":kBlack,
      "data_alt":kBlack,
      "ZJets":kBlack,
      "expected":kBlack,
      "1sigma":kGreen,
      "2sigma":kYellow,
    }
    marker_dict  = {
      "ZH_a15a15_4b_ctau10":20,
      "ZH_a15a15_4b_ctau100":21,
      "ZH_a15a15_4b_ctau1000":22,
      "ZH_a25a25_4b_ctau10":23, 
      "ZH_a25a25_4b_ctau100":20, 
      "ZH_a25a25_4b_ctau1000":21, 
      "ZH_a35a35_4b_ctau10":21,
      "ZH_a35a35_4b_ctau100":22,
      "ZH_a35a35_4b_ctau1000":23,
      "ZH_a55a55_4b_ctau10":22, 
      "ZH_a55a55_4b_ctau100":23,
      "ZH_a55a55_4b_ctau1000":20, 
      "data":20,
      "data_alt":24,
      "ZJets":24,
      "expected":20,
      "1sigma":20,
      "2sigma":20,
    }
    line_dict  = {
      "ZH_a55a55_4b_ctau10":7,
      "ZH_a55a55_4b_ctau100":7,
      "ZH_a55a55_4b_ctau1000":7,
      "ZH_a35a35_4b_ctau10":6,
      "ZH_a35a35_4b_ctau100":6,
      "ZH_a35a35_4b_ctau1000":6,
      "ZH_a25a25_4b_ctau10":5,
      "ZH_a25a25_4b_ctau100":5,
      "ZH_a25a25_4b_ctau1000":5,
      "ZH_a15a15_4b_ctau10":4,
      "ZH_a15a15_4b_ctau100":4,
      "ZH_a15a15_4b_ctau1000":4,
    }

    if("data" in hist_type):
      histo.SetMarkerColor( color_dict[hist_type] )
      histo.SetMarkerStyle( marker_dict[hist_type] )
    else:
      histo.SetMarkerSize(0)

    #histo.GetFunction("expo").SetLineColor( color_dict[hist_type] );
    histo.SetLineColor( color_dict[hist_type] )
    histo.GetXaxis().SetTitleOffset(1.2);
    histo.GetYaxis().SetTitleOffset(1.2);
    histo.GetXaxis().SetTitleSize(.05);
    histo.GetYaxis().SetTitleSize(.05);
    histo.GetXaxis().SetLabelSize(.05);
    histo.GetYaxis().SetLabelSize(.05);
    histo.GetXaxis().SetLabelOffset(0.01);

def format_simple_pad_graph(pad):
    pad.SetPad(0.0, 0.0, 1., 1.)
    pad.SetTopMargin(0.065)
    pad.SetRightMargin(0.03)
    pad.SetLeftMargin(0.15)
    pad.SetBottomMargin(0.13)
    pad.SetBorderSize(0)
    pad.SetGridy(0)
    pad.SetBorderSize(0)

def format_simple_pad(pad):
    pad.SetPad(0.0, 0.0, 1., 1.)
    pad.SetTopMargin(0.065)
    pad.SetRightMargin(0.04)
    pad.SetLeftMargin(0.13)
    pad.SetBottomMargin(0.13)
    pad.SetBorderSize(0)
    pad.SetGridy(0)
    pad.SetBorderSize(0)

def format_simple_pad_2D(pad):
    pad.SetPad(0.0, 0.0, 1., 1.)
    pad.SetTopMargin(0.0675)
    pad.SetRightMargin(0.14)
    pad.SetLeftMargin(0.13)
    pad.SetBottomMargin(0.13)
    pad.SetBorderSize(0)
    pad.SetGridy(0)
    pad.SetBorderSize(0)

def format_2pads_for_ratio():
    pad1 = TPad("pad1", "pad1", 0, 0.25, 1., 1.0)
    pad1.SetTopMargin(0.065)
    pad1.SetRightMargin(0.04)
    pad1.SetLeftMargin(0.13)
    pad1.SetBottomMargin(0.02)
    pad1.SetBorderSize(0)
    pad1.SetGridy(0)
    pad1.SetBorderSize(0)

    pad2 = TPad("pad2", "pad2", 0, 0.0, 1, 0.25)
    pad2.SetTopMargin(0.01)
    pad2.SetRightMargin(0.04)
    pad2.SetLeftMargin(0.13)
    pad2.SetBottomMargin(0.4)
    pad2.SetFillColorAlpha(0, 0.)
    pad2.SetBorderSize(0)
    pad2.SetGridy(0)
    pad2.SetBorderSize(0)

    return pad1, pad2

def draw_hists_stack(hlist, options, types):
    assert(len(hlist) > 0)

    hs = THStack("hs","")

    for i in range(len(hlist)):
        hs.Add(hlist[i])
    hs.Draw("hist")


def draw_hists(hlist, options, types):
    assert(len(hlist) > 0)

    hlist[0].Draw(options)
    if "hist" in options.lower():
        hlist[0].Draw("hist same")

    for i in range(len(hlist)):
        #f = hlist[i].GetFunction("expo");
        #f.SetLineStyle(7)
        #f.Draw("SAME")
        hlist[i].Draw(options + ",same")
        if "hist" in options.lower() and types[i] != "data":
            hlist[i].Draw("hist same")

def draw_graphs(glist, options, types):
    assert(len(glist) > 0)

    glist[0].Draw(options)

    for i in range(len(glist)):
        glist[i].Draw(options + ",same")
