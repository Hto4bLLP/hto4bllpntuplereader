import inspect
import logging
import os
import sys
import glob
import fnmatch
import math
from subprocess import call

def intersect(*d):
    sets = iter(map(set, d))
    result = sets.next()
    for s in sets:
        result = result.intersection(s)
    return result

def drange(start, stop, step):
     r = start
     while r <= stop:
         yield r
         r += step

def mean(data):
  return float(float(sum(data)) / float(len(data)))

def stddev(data):
  data_mean = mean(data)
  sum_sq = 0.0
  for p in data:
    sum_sq += (float(p) - data_mean)**2.0
  return math.sqrt(sum_sq / float(len(data) - 1))

def PrintFrame():
  callerframerecord = inspect.stack()[1]    # 0 represents this line
                                            # 1 represents line at caller
  frame = callerframerecord[0]
  info = inspect.getframeinfo(frame)
  print("FILE:", info.filename, "FUNCTION:", info.function, "LINE:", info.lineno)

def ErrorMsg(msg):
  callerframerecord = inspect.stack()[1]    # 0 represents this line
                                            # 1 represents line at caller
  frame = callerframerecord[0]
  info = inspect.getframeinfo(frame)
  print("ERROR:", msg, "@", "FILE:", info.filename, "FUNCTION:", info.function, "LINE:", info.lineno)
  sys.exit(1)

def make_dir(dir_path):
    try:
        os.makedirs(dir_path)
    except OSError:
        if not os.path.isdir(dir_path):
            raise


def find_files(directory, pattern, files_only = False, match_full_path = False):
    matches = []
    for root, dirs, files in os.walk(directory):
        for basename in files:
            full_path = os.path.join(root, basename)
            condition = fnmatch.fnmatch(full_path, pattern) if match_full_path else fnmatch.fnmatch(basename, pattern)
            if condition:
                if (files_only and not os.path.isfile(full_path)):
                    continue
                else:
                    matches.append(full_path)
    return matches

def find_directories(directory, pattern):
    matches = []
    for root, dirs, files in os.walk(directory):
        for basename in dirs:
            if fnmatch.fnmatch(basename, pattern):
                filename = os.path.join(root, basename)
                if (os.path.isdir(filename)):
                    matches.append(filename)
    return matches

def my_hadd(new_file, directory, pattern, **kwargs):
    rootfiles = list(find_files(directory, pattern, files_only = True, **kwargs))
    call(["hadd", "-f", new_file] + rootfiles)
    call(["hadd", "-f", new_file] + rootfiles)

# supply your own list of .root files to be merged
def my_hadd_list(new_file, old_files):
    call(["hadd", "-f", new_file] + old_files)

def remove_prefix(text, prefix):
    if text.startswith(prefix):
        return text[len(prefix):]
    return text

def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in xrange(0, len(l), n):
        yield l[i:i + n]

def reverse_readline(filename, buf_size=8192):
    """a generator that returns the lines of a file in reverse order"""
    with open(filename) as fh:
        segment = None
        offset = 0
        fh.seek(0, os.SEEK_END)
        file_size = remaining_size = fh.tell()
        while remaining_size > 0:
            offset = min(file_size, offset + buf_size)
            fh.seek(file_size - offset)
            buffer = fh.read(min(remaining_size, buf_size))
            remaining_size -= buf_size
            lines = buffer.split('\n')
            # the first line of the buffer is probably not a complete line so
            # we'll save it and append it to the last line of the next buffer
            # we read
            if segment is not None:
                # if the previous chunk starts right from the beginning of line
                # do not concact the segment to the last line of new chunk
                # instead, yield the segment first
                if buffer[-1] is not '\n':
                    lines[-1] += segment
                else:
                    yield segment
            segment = lines[0]
            for index in range(len(lines) - 1, 0, -1):
                if len(lines[index]):
                    yield lines[index]
        # Don't yield None if the file was empty
        if segment is not None:
            yield segment

def find_signal_mass_point(filepath):
  try:
    idx = filepath.index("qqqq_m")
    mass = int(filepath[idx+6:idx+10])
    return mass
  except:
    print("WARNING: Couldn't determine signal mass point for sample with file name: ", filepath)
    print("WARNING: Defaulting to resonance mass = -1!")
    sys.exit(1)

def print_argparse_args(args):
    logging.info("########################################")
    logging.info("##### START COMMAND LINE ARGUMENTS #####")
    logging.info("########################################")
    for arg in vars(args):
        logging.info("%s = %s" % (arg, getattr(args, arg)))
    logging.info("########################################")
    logging.info("###### END COMMAND LINE ARGUMENTS ######")
    logging.info("########################################")
    logging.info("\n")

def normalize_histogram(histo):
    histo.Scale(1.0 / histo.Integral())

def AND(*args):
    return "(" + ' && '.join(args) + ")"

def OR(*args):
    return "(" + ' || '.join(args) + ")"

def NOT(args):
    return "!(" + args + ")"

