from ROOT import *
import atlas_style

import os
import math
from sys import argv, exit

from plot_base import *
from plot_util import *

gROOT.SetBatch(kTRUE)
gStyle.SetOptStat(0)
gStyle.SetLineWidth(2)

class TFCompare(PlotBase):
    def __init__(self, TF1, TF2, legends, *args, **kwargs):

        super(TFCompare, self).__init__(
                legend_loc = [0.6,0.9,0.9, 0.9 - 0.045*3 ],
                atlas_loc = [0.17,0.875],
                *args,
                **kwargs)

        x_max = -1
        y_min = 10000000
        y_max = -1

        self.hists = [TF1,TF2]
  
        for h in self.hists:

          self.set_x_axis_bounds(h)
          self.set_y_min(h)
          self.set_y_max(h)
          self.set_titles(h)

          h.GetXaxis().SetTitleOffset(1.2);
          h.GetYaxis().SetTitleOffset(1.2);
          h.GetXaxis().SetTitleSize(.05);
          h.GetYaxis().SetTitleSize(.05);
          h.GetXaxis().SetLabelSize(.05);
          h.GetYaxis().SetLabelSize(.05);
          h.GetXaxis().SetLabelOffset(0.01);

          h.GetYaxis().SetMaxDigits(3);

        self.pad_empty_space(self.hists)

        pad1 = self.canvas.cd(1)
        format_simple_pad(pad1)
        pad1.cd()

        if (self.log_scale_y):
            pad1.SetLogy()
        if (self.log_scale_x):
            pad1.SetLogx()

        TF1.SetMarkerStyle(20)
        TF1.SetMarkerColor(kBlack)
        TF1.SetLineColor(kBlack)
        TF1.Draw("PE")
        TF2.SetMarkerStyle(24)
        TF2.SetMarkerColor(kBlack)
        TF2.SetLineColor(kBlack)
        TF2.Draw("PE same")

        self.leg.AddEntry(TF1, legends[ 0 ], 'lp')
        self.leg.AddEntry(TF2, legends[ 1 ], 'lp')

        gPad.RedrawAxis();
        self.print_to_file(self.name + ".pdf")
        pad1.Close()

class Hist1DBackground(PlotBase):
    def __init__(self, expected, observed, sig, legends, blind = False, doRatio = False, *args, **kwargs):

        super(Hist1DBackground, self).__init__(
                legend_loc = [0.6,0.9,0.9, 0.9 - 0.045*3 ],
                atlas_loc = [0.17,0.885],
                *args,
                **kwargs)

        self.hists = [expected,observed,sig]

        x_max = -1
        y_min = 10000000
        y_max = -1

        for h in self.hists:
            if (self.rebin != None):
                h.Rebin(self.rebin)

            self.set_x_axis_bounds(h)
            self.set_y_min(h)
            self.set_y_max(h)
            self.set_titles(h)

            h.GetXaxis().SetTitleOffset(1.2);
            h.GetYaxis().SetTitleOffset(1.2);
            h.GetXaxis().SetTitleSize(.05);
            h.GetYaxis().SetTitleSize(.05);
            h.GetXaxis().SetLabelSize(.05);
            h.GetYaxis().SetLabelSize(.05);
            h.GetXaxis().SetLabelOffset(0.01);

            h.GetYaxis().SetMaxDigits(3);
            if h.GetXaxis().GetXmax() > x_max:
              x_max = h.GetXaxis().GetXmax()

        self.x_max = x_max
        self.pad_empty_space(self.hists)

        if not doRatio:
          pad1 = self.canvas.cd(1)
          format_simple_pad(pad1)
        else:
          
          # if blinded plot SvB
          if blind:
            ratio = sig.Clone("ratio")
            ratio.GetYaxis().SetTitle("S/#sqrt{B}")
            for i in range(1,sig.GetNbinsX()+1):
              if expected.GetBinContent(i) > 0:
                ratio.SetBinContent(i,sig.GetBinContent(i)/sqrt(expected.GetBinContent(i)))
              ratio.SetBinError(i,0)
            ratio.SetMinimum(0.5)
            ratio.SetMaximum(25)
          else:
            ratio = observed.Clone("ratio")
            ratio.Divide(expected)
            ratio.GetYaxis().SetTitle("obs./exp.")
            ratio.SetMinimum(0.4)
            ratio.SetMaximum(1.6)

          ratio.SetMarkerColor(kBlack)
          ratio.SetLineColor(kBlack)
          ratio.GetXaxis().SetTitleOffset(1.3);
          ratio.GetYaxis().SetTitleOffset(.37)
          ratio.GetXaxis().SetTitleSize(.14)
          ratio.GetYaxis().SetTitleSize(.14)
          ratio.GetXaxis().SetLabelSize(0.135)
          ratio.GetXaxis().SetLabelOffset(0.03)
          ratio.GetYaxis().SetLabelSize(0.125)
          ratio.GetYaxis().SetNdivisions(505)

          expected.GetXaxis().SetLabelOffset(0.05)
          observed.GetXaxis().SetLabelOffset(0.05)


          pad1, pad2 = format_2pads_for_ratio()
          pad1.SetFrameLineWidth(2)
          pad2.SetFrameLineWidth(2)

          pad1.SetTicks(1,1)
          pad2.SetTicks(1,1)

          pad2.Draw()
          pad1.Draw()

          pad2.cd()
          if blind:
            pad2.SetLogy()
          ratio.Draw("PE")
          line = TLine(self.x_min,1.0,self.x_max,1.0)
          line.SetLineColor(kBlack)
          line.SetLineWidth(1)
          line.SetLineStyle(2)
          line.Draw("same")

        pad1.cd()

        if (self.log_scale_y):
            pad1.SetLogy()
        if (self.log_scale_x):
            pad1.SetLogx()

        expected.SetMarkerSize(0)
        expected.SetLineColor(kMagenta+3)
        expected.SetFillColor(kMagenta+2)

        expectedErr = expected.Clone("expErr")
        expectedErr.SetFillStyle(3353)
        expectedErr.SetFillColor(kMagenta+3)

        observed.SetMarkerStyle(20)
        observed.SetMarkerColor(kBlack)
        observed.SetLineColor(kBlack)

        sig.SetLineStyle(6)
        sig.SetLineColor(kMagenta-9)
        sig.SetMarkerSize(0)

        expected.Draw("hist")

        gStyle.SetErrorX(0.5)
        expectedErr.Draw("E2 same")
        self.leg.AddEntry(expected, legends[ 0 ], 'f')
        sig.Draw("hist SAME")
        self.leg.AddEntry(sig, legends[ 2 ], 'l')

        if not blind:
          observed.Draw("PE SAME")
          self.leg.AddEntry(observed, legends[ 1 ], 'lp')

        gPad.RedrawAxis();

        self.print_to_file(self.name + ".pdf")
        pad1.Close()
