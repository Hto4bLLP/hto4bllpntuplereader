from ROOT import *
import atlas_style

import os
import math
from sys import argv, exit

from plot_base import *
from plot_util import *

gROOT.SetBatch(kTRUE)
gStyle.SetOptStat(0)
gStyle.SetLineWidth(2)

class HistStack(PlotBase):
    def __init__(self, hists, types, legends, y_axis_type = "Vertices", **kwargs):

        super(HistStack, self).__init__(
                legend_loc = [0.6,0.9,0.9, 0.9 - 0.045*(len(hists)+1) ],
                atlas_loc = [0.17,0.875],
                extra_lines_loc = [0.17,0.775],
                **kwargs)

        self.hists = hists
        self.hs = THStack("hs","")


        for h, t in zip(self.hists, types):
            if (self.rebin != None):
                h.Rebin(self.rebin)

            self.leg.AddEntry(h, legends[ self.hists.index(h)  ], 'f')

            format_for_drawing_stack(h, t)
#            h.GetYaxis().SetMaxDigits(3);
            self.hs.Add(h)


        self.pad_empty_space(self.hists)
        pad1 = self.canvas.cd(1)
        format_simple_pad(pad1)
        pad1.cd()
        if (self.log_scale_y):
            pad1.SetLogy()
        if (self.log_scale_x):
            pad1.SetLogx()

        self.hs.Draw("hist")
        self.set_x_axis_bounds(self.hs)
        self.set_y_min(self.hs)
        self.set_y_max(self.hs)
        self.set_titles(self.hs, y_axis_type)
        hs.GetXaxis().SetTitleOffset(1.2);
        hs.GetYaxis().SetTitleOffset(1.2);
        hs.GetXaxis().SetTitleSize(.05);
        hs.GetYaxis().SetTitleSize(.05);
        hs.GetXaxis().SetLabelSize(.05);
        hs.GetYaxis().SetLabelSize(.05);
        hs.GetXaxis().SetLabelOffset(0.01);

        self.print_to_file(self.name + ".pdf")
        pad1.Close()


class Graph1D(PlotBase):
    def __init__(self, graphs, types, legends, **kwargs):

        super(Graph1D, self).__init__(
                legend_loc = [0.5,0.9,0.9, 0.9 - 0.045*(len(graphs)+1) ],
                atlas_loc = [0.17,0.875],
                #extra_lines_loc = [0.17,0.775],
                **kwargs)

        self.graphs = graphs
        h = TH2F("h1","Cross-Section Limit",1000,self.x_min,self.x_max,1000,self.y_min,self.y_max)
        format_for_drawing(h, "ZH_a55a55_4b_ctau10")
        h.GetYaxis().SetTitle(self.y_title)
        h.GetXaxis().SetTitle(self.x_title + " [" + self.x_units + "]")

        h.Draw("");
        h.SetMaximum(1.0);
        h.SetMinimum(0.0);

        for g, t in zip(self.graphs, types):
            self.leg.AddEntry(g, legends[ self.graphs.index(g)  ], 'l')

            self.set_x_axis_bounds(g)
            self.set_y_min(g)
            self.set_y_max(g)
            self.set_titles_graph(g)
            format_for_drawing(g, t)

        pad1 = self.canvas.cd(1)
        format_simple_pad_graph(pad1)
        pad1.cd()
        if (self.log_scale_y):
            pad1.SetLogy()
        if (self.log_scale_x):
            pad1.SetLogx()

        draw_graphs(self.graphs, "C", types)
        pad1.Update()
        pad1.RedrawAxis()

        self.print_to_file(self.name + ".pdf")
        pad1.Close()


class GraphBrazil(PlotBase):
    def __init__(self, graphs, types, legends, **kwargs):

        super(GraphBrazil, self).__init__(
                legend_loc = [0.5,0.9,0.9, 0.9 - 0.045*(len(graphs)+1) ],
                atlas_loc = [0.17,0.875],
                #extra_lines_loc = [0.17,0.775],
                **kwargs)

        self.graphs = graphs
        h = TH2F("h1","Cross-Section Limit",1000,self.x_min,self.x_max,1000,self.y_min,self.y_max)
        format_for_drawing(h, "data")
        h.GetYaxis().SetTitle(self.y_title)
        h.GetXaxis().SetTitle(self.x_title + " [" + self.x_units + "]")

        h.Draw("");
        h.SetMaximum(1.0);
        h.SetMinimum(0.0);

        for g, t in zip(self.graphs, types):
            self.leg.AddEntry(g, legends[ self.graphs.index(g)  ], 'l') if "sigma" not in t else self.leg.AddEntry(g, legends[ self.graphs.index(g)  ], 'lf')

            self.set_x_axis_bounds(g)
            self.set_y_min(g)
            self.set_y_max(g)
            self.set_titles_graph(g)
            format_for_drawing(g, t)

        pad1 = self.canvas.cd(1)
        format_simple_pad(pad1)
        pad1.cd()
        if (self.log_scale_y):
            pad1.SetLogy()
        if (self.log_scale_x):
            pad1.SetLogx()

        graphs[types.index("2sigma")].SetFillColor(kYellow)
        graphs[types.index("2sigma")].SetLineWidth(0)
        graphs[types.index("2sigma")].Draw("E3")
        graphs[types.index("1sigma")].SetFillColor(kGreen)
        graphs[types.index("1sigma")].SetLineWidth(0)
        graphs[types.index("1sigma")].Draw("E3")
        graphs[types.index("expected")].SetLineWidth(2)
        graphs[types.index("expected")].SetLineStyle(7)
        graphs[types.index("expected")].Draw("C")


        pad1.Update()
        pad1.RedrawAxis()

        self.print_to_file(self.name + ".pdf")
        pad1.Close()

class Hist1DRatio(PlotBase):
    def __init__(self, num, denom, types, legends, y_axis_type = "Vertices", **kwargs):

        super(Hist1DRatio, self).__init__(
                legend_loc = [0.6,0.9,0.9, 0.9 - 0.045*(len(hists)+1) ],
                atlas_loc = [0.17,0.875],
                extra_lines_loc = [0.17,0.775],
                **kwargs)

        self.num = num
        self.denom = denom

        self.hists = [num,denom]

        y_min = 10000000
        y_max = -1
        for h, t in zip(self.hists, types):
            if (self.rebin != None):
                h.Rebin(self.rebin)
            if(self.norm == True):
                h.Scale(1.0/h.Integral())

            self.set_x_axis_bounds(h)
            self.set_y_min(h)
            self.set_y_max(h)
            self.set_titles(h, y_axis_type)
            format_for_drawing(h, t)
            h.GetYaxis().SetMaxDigits(3);

        self.pad_empty_space(self.hists)


        ratio = num.Clone("ratio")
        ratio.Divide(denom)
        ratio.GetYaxis().SetTitle("Data/MC")
        ratio.SetMarkerColor(kBlack)
        ratio.SetLineColor(kBlack)

        pad1, pad2 = format_2pads_for_ratio()

        pad1.Draw()
        pad2.Draw()

        pad1.cd()
        if (self.log_scale_y):
            pad1.SetLogy()

        num.Draw("hist")
        denom.Draw("hist same")
        denom.Draw("PE same")

        pad2.cd()
        ratio.Draw("PE")
        line = TLine(h_ratio.GetXaxis().GetXmin(),1.0,h_ratio.GetXaxis().GetXmax(),1.0)
        line.SetLineColor(kBlack)
        line.SetLineWidth(1)
        line.SetLineStyle(2)
        line.Draw("same")

        self.canvas.Update()
        self.canvas.Modified()

        self.print_to_file(self.name + "ratio.pdf")

class Hist1DFit(PlotBase):
    def __init__(self, h, f, legends, *args, **kwargs):

        super(Hist1DFit, self).__init__(
                legend_loc = [0.6,0.9,0.9, 0.9 - 0.045*3 ],
                atlas_loc = [0.17,0.875],
                *args,
                **kwargs)

        x_max = -1
        y_min = 10000000
        y_max = -1

        self.set_x_axis_bounds(h)
        self.set_y_min(h)
        self.set_y_max(h)
        self.set_titles(h)

        h.GetXaxis().SetTitleOffset(1.2);
        h.GetYaxis().SetTitleOffset(1.2);
        h.GetXaxis().SetTitleSize(.05);
        h.GetYaxis().SetTitleSize(.05);
        h.GetXaxis().SetLabelSize(.05);
        h.GetYaxis().SetLabelSize(.05);
        h.GetXaxis().SetLabelOffset(0.01);

        h.GetYaxis().SetMaxDigits(3);
        self.hists = [h]
        self.pad_empty_space(self.hists)

        pad1 = self.canvas.cd(1)
        format_simple_pad(pad1)
        pad1.cd()

        if (self.log_scale_y):
            pad1.SetLogy()
        if (self.log_scale_x):
            pad1.SetLogx()

        h.SetMarkerStyle(20)
        h.SetMarkerColor(kBlack)
        h.SetLineColor(kBlack)
        h.Draw("PE")

        f.SetLineStyle(7)
        f.SetLineColor(kAzure+4)
        #f.Draw("SAME")

        self.leg.AddEntry(h, legends[ 0 ], 'lp')
        self.leg.AddEntry(f, legends[ 1 ], 'l')

        gPad.RedrawAxis();
        self.print_to_file(self.name + ".pdf")
        pad1.Close()

class Hist1D(PlotBase):
    def __init__(self, hists, types, legends, draw_markers = True, y_axis_type = "Vertices", *args, **kwargs):

        super(Hist1D, self).__init__(
                legend_loc = [0.6,0.9,0.9, 0.9 - 0.045*(len(hists)+1) ],
                atlas_loc = [0.17,0.875],
                *args,
                **kwargs)

        self.hists = hists

        x_max = -1
        y_min = 10000000
        y_max = -1

        fits = []
        for h, t in zip(self.hists, types):
            if (self.rebin != None):
                h.Rebin(self.rebin)
            if draw_markers:
              self.leg.AddEntry(h, legends[ self.hists.index(h)  ], 'lp')
            else:
              self.leg.AddEntry(h, legends[ self.hists.index(h)  ], 'l')

            if(self.norm == True):
                h.Scale(1.0/h.Integral())

            #res = h.Fit("expo","S")

            self.set_x_axis_bounds(h)
            self.set_y_min(h)
            self.set_y_max(h)
            self.set_titles(h, y_axis_type)
            format_for_drawing(h, t)
            if "LRT" in h.GetName():
              h.SetMarkerStyle(h.GetMarkerStyle()+4)
            h.GetYaxis().SetMaxDigits(3);
            if h.GetXaxis().GetXmax() > x_max:
              x_max = h.GetXaxis().GetXmax()

        self.x_max = x_max
        self.pad_empty_space(self.hists)
        pad1 = self.canvas.cd(1)
        format_simple_pad(pad1)
        pad1.cd()
        if (self.log_scale_y):
            pad1.SetLogy()
        if (self.log_scale_x):
            pad1.SetLogx()

        if draw_markers:
          draw_hists(self.hists, "PE hist", types)
        else:
          draw_hists(self.hists, "ehist", types)

        

        if "_r" in self.name:
            self.draw_material_layers()

        if self.draw_cut:
            line = TLine()
            line.SetLineColor(kRed)
            line.SetLineWidth(2)
            line.SetLineStyle(8)
            cut = self.cut
            line.DrawLine(cut,0,cut,self.y_max * 1.0/self.empty_scale) if not self.norm else line.DrawLine(cut,0,cut,1.0)
            arrow = TArrow(cut,self.y_max*1.0/self.empty_scale, cut*1.5, self.y_max*1.0/self.empty_scale) if not self.norm else TArrow(cut,1.0,cut + 0.1*(self.x_max), 1.0,0.02,"|>")
            arrow.SetLineWidth(2)
            arrow.SetLineStyle(8)
            arrow.SetLineColor(kRed)
            arrow.SetFillColor(kRed)
            arrow.Draw()
           # self.draw_cut_line()

        self.print_to_file(self.name + ".pdf")
        pad1.Close()

    def draw_cut_line(self):
        line = TLine()
        line.SetLineColor(kRed)
        line.SetLineWidth(2)
        line.SetLineStyle(8)
        cut = self.cut
        line.DrawLine(cut,0,cut,self.y_max * 1.0/self.empty_scale) if not self.norm else line.DrawLine(cut,0,cut,1.0)
        arrow = TArrow(cut,self.y_max*1.0/self.empty_scale, cut*1.5, self.y_max*1.0/self.empty_scale) if not self.norm else TArrow(cut,1.0,cut + 0.1*(self.x_max), 1.0,0.02,"|>")
        arrow.SetLineWidth(2)
        arrow.SetLineStyle(8)
        arrow.SetLineColor(kRed)
        arrow.SetFillColor(kRed)
        arrow.Draw()

    def draw_material_layers(self):
        coords = [33.5,50.5,88.5,122.5,299.0]
        for coord in coords:
            line = TLine()
            line.SetLineColor(kRed)
            line.SetLineWidth(2)
            line.SetLineStyle(8)
            line.DrawLine(coord,0,coord,self.y_max * 1.0/self.empty_scale)
            if(coords.index(coord) == 0):
                self.leg.AddEntry(line, "Material Layers", "l")
        
class Hist2D(PlotBase):
    def __init__(self, hist, profile=False,**kwargs):

        super(Hist2D, self).__init__(
                legend_loc = [0.6,0.9,0.85, 0.9 ],
                atlas_loc = [0.175,0.875],
                **kwargs)

        self.hist = hist

        self.set_x_axis_bounds(self.hist)
        self.set_y_axis_bounds(self.hist)
        self.set_z_min(self.hist)
        self.set_z_max(self.hist)

        hist.GetZaxis().SetMaxDigits(3);
        self.set_titles(self.hist,"")

        pad1 = self.canvas.cd(1)
        format_for_drawing(hist)
        format_simple_pad_2D(pad1)
        pad1.cd()
        if (self.log_scale_y):
            pad1.SetLogy()
        if (self.log_scale_x):
            pad1.SetLogx()
        if (self.log_scale_z):
            pad1.SetLogz()

        self.hist.Draw("colz")
        #self.hist.Draw("HIST SAME")
        py = self.hist.ProfileX("py",1,25,"o")
        if(profile):
          py.Draw("same")

        self.print_to_file(self.name + ".pdf")
        pad1.Close()

class Eff1D(PlotBase):
    def __init__(self, hists, types, legends, y_axis_type = "Vertices", **kwargs):

        super(Eff1D, self).__init__(
                legend_loc = [0.5,0.9,0.9, 0.9 - 0.045*(len(hists)+1) ],
                atlas_loc = [0.17,0.875],
                #extra_lines_loc = [0.17,0.775],
                **kwargs)

        self.hists = hists

        for h, t in zip(self.hists, types):
            if (self.rebin != None):
                h.Rebin(self.rebin)

            self.leg.AddEntry(h, legends[ self.hists.index(h)  ], 'lp')
            self.set_x_axis_bounds(h)
            self.set_y_min(h)
            self.set_y_max(h)
            self.set_titles(h, y_axis_type)
            format_for_drawing(h, t)
            h.GetYaxis().SetMaxDigits(3);


        self.pad_empty_space(self.hists)
        pad1 = self.canvas.cd(1)
        format_simple_pad(pad1)
        pad1.cd()
        if (self.log_scale_y):
            pad1.SetLogy()
        if (self.log_scale_x):
            pad1.SetLogx()

        draw_hists(self.hists, "PE", types)

        self.print_to_file(self.name + ".pdf")
        pad1.Close()

