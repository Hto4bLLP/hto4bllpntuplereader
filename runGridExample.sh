prun --containerImage docker://gitlab-registry.cern.ch/hto4bllp/hto4bllpntuplereader \
--exec "echo %IN | sed 's/\,/\n/g' > input.txt; pwd; ls; ls ../VH4b; source /release_setup.sh; source ../VH4b/build/x86*/setup.sh; export SrcDir=../VH4b/src; run_NtupleReader --input_files input.txt --output miniNtuple.root" \
--outputs miniNtuple.root \
--inDS user.jburzyns.data18_13TeV.00349637.physics_Main.NTUP.v1_tree.root \
--outDS user.jburzyns.data18_13TeV.00349637.physics_Main.miniNTUP.v1.test4.root 
