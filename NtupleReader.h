// IMPORTANT! Do not remove this line
#define IS_MC
#undef DO_TRUTH
#undef DO_PHOTON
//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Thu Jan  9 09:37:48 2020 by ROOT version 6.14/08
// from TTree outTree/outTree
// found on file: /gpfs3/umass/jburzyns/VH4b/run/WpH_H125_a55a55_4b_ctau10_new/merged_output.root
//////////////////////////////////////////////////////////

#ifndef NtupleReader_h
#define NtupleReader_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TSelector.h>
#include <TTreeReader.h>
#include <TTreeReaderValue.h>
#include <TTreeReaderArray.h>

// Headers needed by this particular selector
#include <vector>
#include <string>
#include <TProofOutputFile.h>
//#include "Hto4bParameters.h"
#include "Tools/Vertex.h"
#include "Tools/MCConfig.h"
#include "Tools/Params.h"
#include "TMVA/DataLoader.h"
#include "TMVA/Reader.h"
#include "TH1F.h"
#include "TH2F.h"


class NtupleReader : public TSelector {
private :
   std::map<TString, std::map<TString, TH1F*>> hists1D; //!
   std::map<TString, std::map<TString, TH2F*>> hists2D; //!
   TProofOutputFile *m_prooffile; //!
   TFile *m_outfile; //!
   Params *m_params; //->
   Bool_t m_isMC; //!
   Bool_t m_isSignal = true; //!
   Bool_t m_UsePROOF; //!
   TH1F *m_cutflow_weighted; //!
   TH1F *m_cutflow; //!
   TTree* miniTree; //!
   //std::vector <Selection> m_selection; //!
   UInt_t m_ntrk_cut = 2;
   Float_t m_mass_cut = 3.0;
   Float_t m_minDR = 0.6;

   TMVA::DataLoader *dataloader;
   TMVA::Reader *reader;
   TMVA::Reader *dataReader;
   std::vector<TString> branchNames; 
   std::map<TString,float*> branches;

   void SetVertex(Vertex& v, 
                  std::string quality, 
                  size_t index, 
                  std::vector<size_t> good_jets = std::vector<size_t>(), 
                  std::vector<size_t> truth_verts = std::vector<size_t>());

   bool passLeptonTrigger();
   bool passPhotonTrigger();
   void passesRegionSelection();
   void fillCutFlow(int &cutCounter, std::vector<bool> &cuts, bool dec, double weight);
   std::pair<int, int> nMuons();
   std::pair<int, int> nElectrons();

public :
   TTreeReader     fReader;  //!the tree reader
   TTree          *fChain = 0;   //!pointer to the analyzed TTree or TChain

   // Analysis Methods
   virtual void BookMiniTree();
   virtual void FillMiniTree();
   virtual void ClearBranches();
   virtual void BookHistograms();
   virtual void FillHistogram1D(TString region, TString selection, Float_t val, Float_t weight = 1.0);
   virtual void FillHistogram2D(TString region, TString selection, Float_t val1, Float_t val2, Float_t weight = 1.0);
   virtual void FillVertexHistograms(TString region, Vertex v, Float_t weight = 1.0);

   // Output miniTree variables
   std::map<TString, Int_t> miniTree_IsRegion;

   ////////////////////////////////////////////////////
   //////////// Truth Level Variables /////////////////
   ////////////////////////////////////////////////////

   std::vector<Float_t> miniTree_LLP_beta ;
   std::vector<Float_t> miniTree_LLP_gamma ;
   std::vector<Float_t> miniTree_LLP_R ;
   std::vector<Float_t> miniTree_LLP_x ;
   std::vector<Float_t> miniTree_LLP_y ;
   std::vector<Float_t> miniTree_LLP_z ;
   std::vector<Float_t> miniTree_LLP_lifetime_lab ;
   std::vector<Float_t> miniTree_LLP_lifetime_prop ;
   std::vector<Float_t> miniTree_LLP_pt ;
   std::vector<Float_t> miniTree_LLP_eta ;
   std::vector<Float_t> miniTree_LLP_phi ;
   std::vector<Float_t> miniTree_LLP_m ;

   Float_t miniTree_LLP_deltaR ;

   std::vector<Float_t> miniTree_V_child_Id ;
   std::vector<Float_t> miniTree_V_child_pt ;
   std::vector<Float_t> miniTree_V_child_eta ;
   std::vector<Float_t> miniTree_V_child_phi ;
   std::vector<Float_t> miniTree_V_child_m ;

   Int_t   miniTree_V_Id ;
   Float_t miniTree_V_pt ;
   Float_t miniTree_V_eta ;
   Float_t miniTree_V_phi ;
   Float_t miniTree_V_m ;
   Float_t miniTree_H_pt ;
   Float_t miniTree_H_eta ;
   Float_t miniTree_H_phi ;
   Float_t miniTree_H_m ;

   ////////////////////////////////////////////////////
   //////////// Event Level Variables /////////////////
   ////////////////////////////////////////////////////

   Int_t   miniTree_dsid;

   Float_t miniTree_mcEventWeight;
   Float_t miniTree_pileupEventWeight;
   Float_t miniTree_XsecEventWeight;
   Float_t miniTree_lumiEventWeight;
   Float_t miniTree_totalEventWeight;


   Float_t  miniTree_v1_medium_mu = -1;
   Float_t  miniTree_v2_medium_mu = -1;
   Float_t  miniTree_v1_medium_muntrk = -1; 
   Float_t  miniTree_v2_medium_muntrk = -1;
   Float_t  miniTree_v1_loose_mu = -1;
   Float_t  miniTree_v2_loose_mu = -1;
   Float_t  miniTree_v1_loose_muntrk = -1;
   Float_t  miniTree_v2_loose_muntrk = -1;

   Float_t miniTree_Z_pt;
   Float_t miniTree_Z_eta;
   Float_t miniTree_Z_phi;
   Float_t miniTree_Z_m;
   Int_t miniTree_passZ;

   Int_t   miniTree_nJets;
   Float_t miniTree_dijet_dR;
   Float_t miniTree_dijet_dR3;
   Float_t miniTree_dijet_dR4;
   Float_t miniTree_jj_pt;
   Float_t miniTree_mjj;
   Float_t miniTree_mjjj;
   Float_t miniTree_mjjjj;
   Float_t miniTree_jet_lep_dR;
   Float_t miniTree_j1_pt;
   Float_t miniTree_j1_minVtxDR;
   Float_t miniTree_j1_DL1;
   Float_t miniTree_j1_chf;
   Float_t miniTree_j1_alpha_max;
   Float_t miniTree_j2_pt;
   Float_t miniTree_j2_minVtxDR;
   Float_t miniTree_j2_DL1;
   Float_t miniTree_j2_chf;
   Float_t miniTree_j2_alpha_max;
   Float_t miniTree_BDTweight;
   Float_t miniTree_Lep_JetMinCHF_DeltaPhi;
   Float_t miniTree_Lep_SumLeadingJets_DeltaPhi;
   Float_t miniTree_minCHFJet_pt;
   Float_t miniTree_minCHFJet_eta;
   Float_t miniTree_minCHFJet_phi;
   Float_t miniTree_minCHFJet_E;
   Float_t miniTree_Ht;
   Float_t miniTree_Lep_Jet_MinDeltaPhi;
   Float_t miniTree_lep_pt_imbalance;
   Float_t miniTree_z_jets_dPhi;
   Float_t miniTree_cosThetaStar;

   Int_t   miniTree_nSecVtx_signal_tight;
   Int_t   miniTree_nSecVtx_signal_medium;
   Int_t   miniTree_nSecVtx_signal_loose;

   Float_t miniTree_l1_pt;
   Float_t miniTree_l1_eta;
   Float_t miniTree_l1_phi;
   Float_t miniTree_l1_m;
   Float_t miniTree_l2_pt;
   Float_t miniTree_l2_eta;
   Float_t miniTree_l2_phi;
   Float_t miniTree_l2_m;

   Int_t miniTree_isSR;
   Int_t miniTree_isCR;
   Int_t miniTree_passPresel;
   Int_t miniTree_passPhoton;
   Int_t miniTree_passFullSelection_1Vtx;
   Int_t miniTree_passFullSelection_2Vtx;

   std::vector<std::string> miniTree_passedTriggers;
   Int_t miniTree_passFilter;
   Int_t miniTree_passTrigger;

   Int_t miniTree_isElEvent;
   Int_t miniTree_isMuEvent;
   Int_t miniTree_pass2Lep;
   Int_t miniTree_pass2El;
   Int_t miniTree_pass2Mu;
   Int_t miniTree_hasVtx;

   ////////////////////////////////////////////////////
   //////////// Vertex Level Variables ////////////////
   ////////////////////////////////////////////////////

   // Tight
   std::vector<Float_t>              miniTree_secVtxTight_pt;
   std::vector<Float_t>              miniTree_secVtxTight_eta;
   std::vector<Float_t>              miniTree_secVtxTight_phi;
   std::vector<Float_t>              miniTree_secVtxTight_m;
   std::vector<Float_t>              miniTree_secVtxTight_m_nonAssoc;
   std::vector<Float_t>              miniTree_secVtxTight_chi2;
   std::vector<Float_t>              miniTree_secVtxTight_minOpAng;
   std::vector<Float_t>              miniTree_secVtxTight_maxOpAng;
   std::vector<Float_t>              miniTree_secVtxTight_mind0;
   std::vector<Float_t>              miniTree_secVtxTight_maxd0;
   std::vector<Float_t>              miniTree_secVtxTight_minOneTrackRemovedMass;
   std::vector<Float_t>              miniTree_secVtxTight_r;
   std::vector<Float_t>              miniTree_secVtxTight_z;
   std::vector<Float_t>              miniTree_secVtxTight_distFromPV;
   std::vector<Int_t>                miniTree_secVtxTight_ntrk;
   std::vector<Int_t>                miniTree_secVtxTight_ntrk_lrt;
   std::vector<Int_t>                miniTree_secVtxTight_ntrk_sel;
   std::vector<Int_t>                miniTree_secVtxTight_ntrk_assoc;
   std::vector<Float_t>              miniTree_secVtxTight_matchScore;
   std::vector<Int_t>                miniTree_secVtxTight_isMatched;
   std::vector<Int_t>                miniTree_secVtxTight_isLLPMatched;
   std::vector<Int_t>                miniTree_secVtxTight_passQuality;
   std::vector<Int_t>                miniTree_secVtxTight_passFiducial;
   std::vector<Int_t>                miniTree_secVtxTight_passMaterial;
   std::vector<Int_t>                miniTree_secVtxTight_passPresel;
   std::vector<Float_t>              miniTree_secVtxTight_maxDR;
   std::vector<Float_t>              miniTree_secVtxTight_maxDR_wrtSV;
   std::vector<Float_t>              miniTree_secVtxTight_minVtxDR;
   std::vector<Float_t>              miniTree_secVtxTight_minJetDR;
   std::vector<Int_t>                miniTree_secVtxTight_jetIndex;
   std::vector<Float_t>              miniTree_secVtxTight_PVClosestApproach;
   std::vector<Float_t>              miniTree_secVtxTight_ZDR;
   std::vector<Float_t>              miniTree_secVtxTight_errLxy;
   std::vector<std::vector<Float_t>> miniTree_secVtxTight_twoTracksMass;
   std::vector<std::vector<Float_t>> miniTree_secVtxTight_trk_chi2;
   std::vector<std::vector<Float_t>> miniTree_secVtxTight_trk_pt_wrtSV;
   std::vector<std::vector<Float_t>> miniTree_secVtxTight_trk_eta_wrtSV;
   std::vector<std::vector<Float_t>> miniTree_secVtxTight_trk_phi_wrtSV;
   std::vector<std::vector<Float_t>> miniTree_secVtxTight_trk_d0_wrtSV;
   std::vector<std::vector<Float_t>> miniTree_secVtxTight_trk_z0_wrtSV;
   std::vector<std::vector<Float_t>> miniTree_secVtxTight_trk_errd0_wrtSV;
   std::vector<std::vector<Float_t>> miniTree_secVtxTight_trk_errz0_wrtSV;
   std::vector<std::vector<Float_t>> miniTree_secVtxTight_trk_chi2_toSV;

   // Medium
   std::vector<Float_t>              miniTree_secVtxMedium_pt;
   std::vector<Float_t>              miniTree_secVtxMedium_eta;
   std::vector<Float_t>              miniTree_secVtxMedium_phi;
   std::vector<Float_t>              miniTree_secVtxMedium_m;
   std::vector<Float_t>              miniTree_secVtxMedium_m_nonAssoc;
   std::vector<Float_t>              miniTree_secVtxMedium_chi2;
   std::vector<Float_t>              miniTree_secVtxMedium_minOpAng;
   std::vector<Float_t>              miniTree_secVtxMedium_maxOpAng;
   std::vector<Float_t>              miniTree_secVtxMedium_mind0;
   std::vector<Float_t>              miniTree_secVtxMedium_maxd0;
   std::vector<Float_t>              miniTree_secVtxMedium_minOneTrackRemovedMass;
   std::vector<Float_t>              miniTree_secVtxMedium_r;
   std::vector<Float_t>              miniTree_secVtxMedium_z;
   std::vector<Float_t>              miniTree_secVtxMedium_distFromPV;
   std::vector<Int_t>                miniTree_secVtxMedium_ntrk;
   std::vector<Int_t>                miniTree_secVtxMedium_ntrk_lrt;
   std::vector<Int_t>                miniTree_secVtxMedium_ntrk_sel;
   std::vector<Int_t>                miniTree_secVtxMedium_ntrk_assoc;
   std::vector<Int_t>                miniTree_secVtxMedium_isMatched;
   std::vector<Int_t>                miniTree_secVtxMedium_isLLPMatched;
   std::vector<Int_t>                miniTree_secVtxMedium_passQuality;
   std::vector<Int_t>                miniTree_secVtxMedium_passFiducial;
   std::vector<Int_t>                miniTree_secVtxMedium_passMaterial;
   std::vector<Int_t>                miniTree_secVtxMedium_passPresel;
   std::vector<Float_t>              miniTree_secVtxMedium_maxDR;
   std::vector<Float_t>              miniTree_secVtxMedium_maxDR_wrtSV;
   std::vector<Float_t>              miniTree_secVtxMedium_minVtxDR;
   std::vector<Float_t>              miniTree_secVtxMedium_minDist;
   std::vector<Float_t>              miniTree_secVtxMedium_minJetDR;
   std::vector<Int_t>                miniTree_secVtxMedium_jetIndex;
   std::vector<Float_t>              miniTree_secVtxMedium_ZDR;
   std::vector<Float_t>              miniTree_secVtxMedium_PVClosestApproach;
   std::vector<Float_t>              miniTree_secVtxMedium_errLxy;
   std::vector<std::vector<Float_t>> miniTree_secVtxMedium_twoTracksMass;
   std::vector<std::vector<Float_t>> miniTree_secVtxMedium_trk_chi2;
   std::vector<std::vector<Float_t>> miniTree_secVtxMedium_trk_pt_wrtSV;
   std::vector<std::vector<Float_t>> miniTree_secVtxMedium_trk_eta_wrtSV;
   std::vector<std::vector<Float_t>> miniTree_secVtxMedium_trk_phi_wrtSV;
   std::vector<std::vector<Float_t>> miniTree_secVtxMedium_trk_d0_wrtSV;
   std::vector<std::vector<Float_t>> miniTree_secVtxMedium_trk_z0_wrtSV;
   std::vector<std::vector<Float_t>> miniTree_secVtxMedium_trk_errd0_wrtSV;
   std::vector<std::vector<Float_t>> miniTree_secVtxMedium_trk_errz0_wrtSV;
   std::vector<std::vector<Float_t>> miniTree_secVtxMedium_trk_chi2_toSV;

   // Loose
   std::vector<Float_t>              miniTree_secVtxLoose_pt;
   std::vector<Float_t>              miniTree_secVtxLoose_eta;
   std::vector<Float_t>              miniTree_secVtxLoose_phi;
   std::vector<Float_t>              miniTree_secVtxLoose_m;
   std::vector<Float_t>              miniTree_secVtxLoose_m_nonAssoc;
   std::vector<Float_t>              miniTree_secVtxLoose_chi2;
   std::vector<Float_t>              miniTree_secVtxLoose_minOpAng;
   std::vector<Float_t>              miniTree_secVtxLoose_maxOpAng;
   std::vector<Float_t>              miniTree_secVtxLoose_mind0;
   std::vector<Float_t>              miniTree_secVtxLoose_maxd0;
   std::vector<Float_t>              miniTree_secVtxLoose_minOneTrackRemovedMass;
   std::vector<Float_t>              miniTree_secVtxLoose_r;
   std::vector<Float_t>              miniTree_secVtxLoose_z;
   std::vector<Float_t>              miniTree_secVtxLoose_distFromPV;
   std::vector<Int_t>                miniTree_secVtxLoose_ntrk;
   std::vector<Int_t>                miniTree_secVtxLoose_ntrk_lrt;
   std::vector<Int_t>                miniTree_secVtxLoose_ntrk_sel;
   std::vector<Int_t>                miniTree_secVtxLoose_ntrk_assoc;
   std::vector<Int_t>                miniTree_secVtxLoose_isMatched;
   std::vector<Int_t>                miniTree_secVtxLoose_isLLPMatched;
   std::vector<Int_t>                miniTree_secVtxLoose_passQuality;
   std::vector<Int_t>                miniTree_secVtxLoose_passFiducial;
   std::vector<Int_t>                miniTree_secVtxLoose_passMaterial;
   std::vector<Int_t>                miniTree_secVtxLoose_passPresel;
   std::vector<Float_t>              miniTree_secVtxLoose_maxDR;
   std::vector<Float_t>              miniTree_secVtxLoose_maxDR_wrtSV;
   std::vector<Float_t>              miniTree_secVtxLoose_minVtxDR;
   std::vector<Float_t>              miniTree_secVtxLoose_minJetDR;
   std::vector<Int_t>                miniTree_secVtxLoose_jetIndex;
   std::vector<Float_t>              miniTree_secVtxLoose_PVClosestApproach;
   std::vector<Float_t>              miniTree_secVtxLoose_ZDR;
   std::vector<Float_t>              miniTree_secVtxLoose_errLxy;
   std::vector<std::vector<Float_t>> miniTree_secVtxLoose_twoTracksMass;
   std::vector<std::vector<Float_t>> miniTree_secVtxLoose_trk_chi2;
   std::vector<std::vector<Float_t>> miniTree_secVtxLoose_trk_pt_wrtSV;
   std::vector<std::vector<Float_t>> miniTree_secVtxLoose_trk_eta_wrtSV;
   std::vector<std::vector<Float_t>> miniTree_secVtxLoose_trk_phi_wrtSV;
   std::vector<std::vector<Float_t>> miniTree_secVtxLoose_trk_d0_wrtSV;
   std::vector<std::vector<Float_t>> miniTree_secVtxLoose_trk_z0_wrtSV;
   std::vector<std::vector<Float_t>> miniTree_secVtxLoose_trk_errd0_wrtSV;
   std::vector<std::vector<Float_t>> miniTree_secVtxLoose_trk_errz0_wrtSV;
   std::vector<std::vector<Float_t>> miniTree_secVtxLoose_trk_chi2_toSV;


   ////////////////////////////////////////////////////
   ///////////// Truth Level Variables ////////////////
   ////////////////////////////////////////////////////
   std::vector<Float_t> miniTree_truthVtx_r;
   std::vector<Float_t> miniTree_truthVtx_z;

   std::vector<Int_t> miniTree_truthVtx_isGood;
   std::vector<Int_t> miniTree_truthVtx_isMatched;
   std::vector<Int_t> miniTree_truthVtx_isMatched_passQuality;
   std::vector<Int_t> miniTree_truthVtx_isMatched_passFiducial;
   std::vector<Int_t> miniTree_truthVtx_isMatched_passMaterial;
   std::vector<Int_t> miniTree_truthVtx_isMatched_passPresel;
   std::vector<Int_t> miniTree_truthVtx_isMatched_passTracks;
   std::vector<Int_t> miniTree_truthVtx_isMatched_passMass;
   
   ////////////////////////////////////////////////////
   /////////////    Jet Level Variables ///////////////
   ////////////////////////////////////////////////////

   std::vector<Float_t> miniTree_jet_pt;
   std::vector<Float_t> miniTree_jet_eta;
   std::vector<Float_t> miniTree_jet_phi;
   std::vector<Float_t> miniTree_jet_E;
   std::vector<Float_t> miniTree_jet_chf;
   std::vector<Float_t> miniTree_jet_alpha_max;
   std::vector<Int_t>   miniTree_jet_passBaseline;  
   std::vector<Int_t>   miniTree_jet_passPresel;  
   std::vector<Int_t>   miniTree_jet_passFilter; 
   //Variables added by Bing
   std::vector<Float_t> miniTree_jet_EMFrac;
   std::vector<Float_t> miniTree_jet_Width;
   std::vector<Float_t> miniTree_jet_Jvt;
   std::vector<Float_t> miniTree_jet_DL1;
   //Truth Jet Info   
   std::vector<Int_t> miniTree_jet_HadronConeExclTruthLabelID;
   std::vector<Int_t> miniTree_jet_HadronConeExclExtendedTruthLabelID;
   std::vector<Int_t> miniTree_jet_PartonTruthLabelID;
   // Additional variables added by jackson
   std::vector<Int_t> miniTree_jet_passSignal;
   std::vector<Int_t> miniTree_jet_hasLooseVtx;
   std::vector<Int_t> miniTree_jet_hasMedVtx;
   std::vector<Int_t> miniTree_jet_hasTightVtx;
   std::vector<Float_t> miniTree_jet_minLooseVtxDR;
   std::vector<Float_t> miniTree_jet_minMedVtxDR;
   std::vector<Float_t> miniTree_jet_minTightVtxDR;

   //Variables added by Bing
   ////////////////////////////////////////////////////
   ///////////// Photon Level Variables ///////////////
   ////////////////////////////////////////////////////
  
   std::vector<Float_t> miniTree_ph_m;
   std::vector<Float_t> miniTree_ph_pt;
   std::vector<Float_t> miniTree_ph_eta;
   std::vector<Float_t> miniTree_ph_phi;
   std::vector<Int_t>   miniTree_ph_isIsolated_Cone40CaloOnly;
   std::vector<Int_t>   miniTree_ph_isIsolated_Cone40;
   std::vector<Int_t>   miniTree_ph_isIsolated_Cone20;
   std::vector<Int_t>   miniTree_ph_IsLoose;
   std::vector<Int_t>   miniTree_ph_IsMedium;
   std::vector<Int_t>   miniTree_ph_IsTight;
   std::vector<Int_t>   miniTree_ph_convType;
   //

   ////////////////////////////////////////////////////
   ///////////// Lepton Level Variables ///////////////
   ////////////////////////////////////////////////////

   std::vector<Float_t> miniTree_el_pt;
   std::vector<Float_t> miniTree_el_eta;
   std::vector<Float_t> miniTree_el_phi;
   std::vector<Float_t> miniTree_el_m;    
   std::vector<Int_t>   miniTree_el_charge;
   std::vector<Int_t>   miniTree_el_isIsolated_Gradient; 
   std::vector<Int_t>   miniTree_el_Loose; 
   std::vector<Int_t>   miniTree_el_Medium;  
   std::vector<Int_t>   miniTree_el_Tight; 
   std::vector<Int_t>   miniTree_el_passBaseline;  
   std::vector<Int_t>   miniTree_el_passFilter; 

   std::vector<Float_t> miniTree_mu_pt;
   std::vector<Float_t> miniTree_mu_eta;
   std::vector<Float_t> miniTree_mu_phi;
   std::vector<Float_t> miniTree_mu_m;    
   std::vector<Int_t>   miniTree_mu_charge;
   std::vector<Int_t>   miniTree_mu_isIsolated_Gradient; 
   std::vector<Int_t>   miniTree_mu_Loose; 
   std::vector<Int_t>   miniTree_mu_Medium;  
   std::vector<Int_t>   miniTree_mu_Tight; 
   std::vector<Int_t>   miniTree_mu_passBaseline;  
   std::vector<Int_t>   miniTree_mu_passFilter; 

   // Readers to access the data (delete the ones you do not need).
   TTreeReaderValue<Int_t> runNumber = {fReader, "runNumber"};
   TTreeReaderValue<Long64_t> eventNumber = {fReader, "eventNumber"};
   TTreeReaderValue<Int_t> lumiBlock = {fReader, "lumiBlock"};
#ifdef IS_MC
   TTreeReaderValue<Int_t> mcEventNumber = {fReader, "mcEventNumber"};
   TTreeReaderValue<Int_t> mcChannelNumber = {fReader, "mcChannelNumber"};
   TTreeReaderValue<Float_t> mcEventWeight = {fReader, "mcEventWeight"};
   TTreeReaderValue<Int_t> rand_run_nr = {fReader, "rand_run_nr"};
   TTreeReaderValue<Int_t> rand_lumiblock_nr = {fReader, "rand_lumiblock_nr"};
#endif
   TTreeReaderValue<Int_t> NPV = {fReader, "NPV"};
   TTreeReaderValue<Float_t> actualInteractionsPerCrossing = {fReader, "actualInteractionsPerCrossing"};
   TTreeReaderValue<Float_t> averageInteractionsPerCrossing = {fReader, "averageInteractionsPerCrossing"};
   TTreeReaderValue<Float_t> weight_pileup = {fReader, "weight_pileup"};
   TTreeReaderValue<Float_t> correctedAverageMu = {fReader, "correctedAverageMu"};
   TTreeReaderValue<Float_t> correctedAndScaledAverageMu = {fReader, "correctedAndScaledAverageMu"};
   TTreeReaderValue<Float_t> correctedActualMu = {fReader, "correctedActualMu"};
   TTreeReaderValue<Float_t> correctedAndScaledActualMu = {fReader, "correctedAndScaledActualMu"};

   TTreeReaderArray<float> LLP_beta = {fReader, "LLP_beta"};
   TTreeReaderArray<float> LLP_gamma = {fReader, "LLP_gamma"};
   TTreeReaderArray<float> LLP_R = {fReader, "LLP_R"};
   TTreeReaderArray<float> LLP_x = {fReader, "LLP_x"};
   TTreeReaderArray<float> LLP_y = {fReader, "LLP_y"};
   TTreeReaderArray<float> LLP_z = {fReader, "LLP_z"};
   TTreeReaderArray<float> LLP_lifetime_lab = {fReader, "LLP_lifetime_lab"};
   TTreeReaderArray<float> LLP_lifetime_prop = {fReader, "LLP_lifetime_prop"};
   TTreeReaderArray<float> LLP_pt = {fReader, "LLP_pt"};
   TTreeReaderArray<float> LLP_eta = {fReader, "LLP_eta"};
   TTreeReaderArray<float> LLP_phi = {fReader, "LLP_phi"};
   TTreeReaderArray<float> LLP_m = {fReader, "LLP_m"};
   TTreeReaderValue<Float_t> LLP_deltaR = {fReader, "LLP_deltaR"};
   TTreeReaderValue<Float_t> PV_x = {fReader, "PV_x"};
   TTreeReaderValue<Float_t> PV_y = {fReader, "PV_y"};
   TTreeReaderValue<Float_t> PV_z = {fReader, "PV_z"};
   TTreeReaderValue<Int_t> V_Id = {fReader, "V_Id"};
   TTreeReaderValue<Float_t> V_pt = {fReader, "V_pt"};
   TTreeReaderValue<Float_t> V_eta = {fReader, "V_eta"};
   TTreeReaderValue<Float_t> V_phi = {fReader, "V_phi"};
   TTreeReaderValue<Float_t> V_m = {fReader, "V_m"};
   TTreeReaderArray<int> V_child_Id = {fReader, "V_child_Id"};
   TTreeReaderArray<float> V_child_pt = {fReader, "V_child_pt"};
   TTreeReaderArray<float> V_child_eta = {fReader, "V_child_eta"};
   TTreeReaderArray<float> V_child_phi = {fReader, "V_child_phi"};
   TTreeReaderArray<float> V_child_m = {fReader, "V_child_m"};
   TTreeReaderValue<Float_t> H_pt = {fReader, "H_pt"};
   TTreeReaderValue<Float_t> H_eta = {fReader, "H_eta"};
   TTreeReaderValue<Float_t> H_phi = {fReader, "H_phi"};
   TTreeReaderValue<Float_t> H_m = {fReader, "H_m"};
   TTreeReaderArray<int> b_LLP_index = {fReader, "b_LLP_index"};
   TTreeReaderArray<float> b_pt = {fReader, "b_pt"};
   TTreeReaderArray<float> b_eta = {fReader, "b_eta"};
   TTreeReaderArray<float> b_phi = {fReader, "b_phi"};
   TTreeReaderArray<float> b_m = {fReader, "b_m"};
   TTreeReaderValue<Int_t> passesFilter = {fReader, "passesFilter"};
   TTreeReaderValue<Int_t> passesElecFilter = {fReader, "passesElecFilter"};
   TTreeReaderValue<Int_t> passesMuonFilter = {fReader, "passesMuonFilter"};
   TTreeReaderValue<Float_t> Lep_JetMinCHF_DeltaPhi = {fReader, "Lep_JetMinCHF_DeltaPhi"};
   TTreeReaderValue<Float_t> Lep_SumLeadingJets_DeltaPhi = {fReader, "Lep_SumLeadingJets_DeltaPhi"};
   TTreeReaderValue<Float_t> weight = {fReader, "weight"};
   TTreeReaderValue<Float_t> weight_xs = {fReader, "weight_xs"};
   TTreeReaderValue<Float_t> weight_prescale = {fReader, "weight_prescale"};
   TTreeReaderValue<Float_t> weight_resonanceKFactor = {fReader, "weight_resonanceKFactor"};
   TTreeReaderValue<Int_t> passL1 = {fReader, "passL1"};
   TTreeReaderValue<Int_t> passHLT = {fReader, "passHLT"};
   TTreeReaderArray<std::string> passedTriggers = {fReader, "passedTriggers"};
   TTreeReaderArray<std::string> disabledTriggers = {fReader, "disabledTriggers"};
   TTreeReaderValue<Int_t> njet = {fReader, "njet"};
   TTreeReaderArray<float> jet_E = {fReader, "jet_E"};
   TTreeReaderArray<float> jet_pt = {fReader, "jet_pt"};
   TTreeReaderArray<float> jet_phi = {fReader, "jet_phi"};
   TTreeReaderArray<float> jet_eta = {fReader, "jet_eta"};
   TTreeReaderArray<float> jet_alpha_max = {fReader, "jet_alpha_max"};
   TTreeReaderArray<int> jet_alpha_max_pass = {fReader, "jet_alpha_max_pass"};
   TTreeReaderArray<float> jet_chf = {fReader, "jet_chf"};
   TTreeReaderArray<int> jet_chf_pass = {fReader, "jet_chf_pass"};
   TTreeReaderArray<float> jet_track_pt_max = {fReader, "jet_track_pt_max"};
   TTreeReaderArray<int> jet_ntracks = {fReader, "jet_ntracks"};
   TTreeReaderArray<float> jet_LooseDR = {fReader, "jet_LooseDR"};
   TTreeReaderArray<float> jet_MediumDR = {fReader, "jet_MediumDR"};
   TTreeReaderArray<float> jet_TightDR = {fReader, "jet_TightDR"};
   TTreeReaderArray<float> jet_LooseDPhi = {fReader, "jet_LooseDPhi"};
   TTreeReaderArray<float> jet_MediumDPhi = {fReader, "jet_MediumDPhi"};
   TTreeReaderArray<float> jet_TightDPhi = {fReader, "jet_TightDPhi"};
   //Variables added by Bing
   TTreeReaderArray<float> jet_EMFrac = {fReader, "jet_EMFrac"};
   TTreeReaderArray<float> jet_Width = {fReader, "jet_Width"};
   //TTreeReaderArray<float> jet_Jvt = {fReader, "jet_Jvt"};
   TTreeReaderArray<float> jet_DL1 = {fReader, "jet_DL1"};
#ifdef DO_TRUTH
   TTreeReaderArray<int> jet_HadronConeExclTruthLabelID = {fReader, "jet_HadronConeExclTruthLabelID"};
   TTreeReaderArray<int> jet_HadronConeExclExtendedTruthLabelID = {fReader, "jet_HadronConeExclExtendedTruthLabelID"};
   TTreeReaderArray<int> jet_PartonTruthLabelID = {fReader, "jet_PartonTruthLabelID"};
#endif   
   TTreeReaderArray<int> jet_LLP_index = {fReader, "jet_LLP_index"};
   TTreeReaderArray<int> jet_num_b = {fReader, "jet_num_b"};
   TTreeReaderArray<float> jet_nearest_b = {fReader, "jet_nearest_b"};
   TTreeReaderArray<int> jet_passBaseline = {fReader, "jet_passBaseline"};
   TTreeReaderArray<int> jet_passPresel = {fReader, "jet_passPresel"};
   TTreeReaderArray<int> jet_passFilter = {fReader, "jet_passFilter"};
   TTreeReaderValue<Int_t> nmuon = {fReader, "nmuon"};
   TTreeReaderArray<float> muon_m = {fReader, "muon_m"};
   TTreeReaderArray<float> muon_pt = {fReader, "muon_pt"};
   TTreeReaderArray<float> muon_phi = {fReader, "muon_phi"};
   TTreeReaderArray<float> muon_eta = {fReader, "muon_eta"};
   TTreeReaderArray<float> muon_charge = {fReader, "muon_charge"};
   TTreeReaderArray<int> muon_isTrigMatched = {fReader, "muon_isTrigMatched"};
   TTreeReaderArray<std::vector<int>> muon_isTrigMatchedToChain = {fReader, "muon_isTrigMatchedToChain"};
   TTreeReaderArray<std::string> muon_listTrigChains = {fReader, "muon_listTrigChains"};
   TTreeReaderArray<int> muon_isIsolated_Gradient = {fReader, "muon_isIsolated_Gradient"};
   TTreeReaderArray<int> muon_isIsolated_FCLoose = {fReader, "muon_isIsolated_FCLoose"};
   TTreeReaderArray<int> muon_isIsolated_FCTight = {fReader, "muon_isIsolated_FCTight"};
   TTreeReaderArray<int> muon_isLoose = {fReader, "muon_isLoose"};
   TTreeReaderArray<int> muon_isMedium = {fReader, "muon_isMedium"};
   TTreeReaderArray<int> muon_isTight = {fReader, "muon_isTight"};
   TTreeReaderArray<float> muon_trkd0 = {fReader, "muon_trkd0"};
   TTreeReaderArray<float> muon_trkd0sig = {fReader, "muon_trkd0sig"};
   TTreeReaderArray<float> muon_trkz0 = {fReader, "muon_trkz0"};
   TTreeReaderArray<float> muon_trkz0sintheta = {fReader, "muon_trkz0sintheta"};
   TTreeReaderArray<float> muon_trkphi0 = {fReader, "muon_trkphi0"};
   TTreeReaderArray<float> muon_trktheta = {fReader, "muon_trktheta"};
   TTreeReaderArray<float> muon_trkcharge = {fReader, "muon_trkcharge"};
   TTreeReaderArray<float> muon_trkqOverP = {fReader, "muon_trkqOverP"};
   TTreeReaderArray<int> muon_passMuonCuts = {fReader, "muon_passMuonCuts"};
   TTreeReaderArray<int> muon_isCombined = {fReader, "muon_isCombined"};
   TTreeReaderArray<int> muon_passBaseline = {fReader, "muon_passBaseline"};
   TTreeReaderArray<int> muon_passFilter = {fReader, "muon_passFilter"};
   TTreeReaderValue<Int_t> nel = {fReader, "nel"};
   TTreeReaderArray<float> el_m = {fReader, "el_m"};
   TTreeReaderArray<float> el_pt = {fReader, "el_pt"};
   TTreeReaderArray<float> el_phi = {fReader, "el_phi"};
   TTreeReaderArray<float> el_eta = {fReader, "el_eta"};
   TTreeReaderArray<float> el_caloCluster_eta = {fReader, "el_caloCluster_eta"};
   TTreeReaderArray<float> el_charge = {fReader, "el_charge"};
   TTreeReaderArray<int> el_isTrigMatched = {fReader, "el_isTrigMatched"};
   TTreeReaderArray<std::vector<int>> el_isTrigMatchedToChain = {fReader, "el_isTrigMatchedToChain"};
   TTreeReaderArray<std::string> el_listTrigChains = {fReader, "el_listTrigChains"};
   TTreeReaderArray<int> el_isIsolated_Gradient = {fReader, "el_isIsolated_Gradient"};
   TTreeReaderArray<int> el_isIsolated_FCLoose = {fReader, "el_isIsolated_FCLoose"};
   TTreeReaderArray<int> el_isIsolated_FCTight = {fReader, "el_isIsolated_FCTight"};
   TTreeReaderArray<int> el_Loose = {fReader, "el_Loose"};
   TTreeReaderArray<int> el_Medium = {fReader, "el_Medium"};
   TTreeReaderArray<int> el_Tight = {fReader, "el_Tight"};
   TTreeReaderArray<float> el_trkd0 = {fReader, "el_trkd0"};
   TTreeReaderArray<float> el_trkd0sig = {fReader, "el_trkd0sig"};
   TTreeReaderArray<float> el_trkz0 = {fReader, "el_trkz0"};
   TTreeReaderArray<float> el_trkz0sintheta = {fReader, "el_trkz0sintheta"};
   TTreeReaderArray<float> el_trkphi0 = {fReader, "el_trkphi0"};
   TTreeReaderArray<float> el_trktheta = {fReader, "el_trktheta"};
   TTreeReaderArray<float> el_trkcharge = {fReader, "el_trkcharge"};
   TTreeReaderArray<float> el_trkqOverP = {fReader, "el_trkqOverP"};
   TTreeReaderArray<int> el_isGood = {fReader, "el_isGood"};
   TTreeReaderArray<int> el_passBaseline = {fReader, "el_passBaseline"};
   TTreeReaderArray<int> el_passFilter = {fReader, "el_passFilter"};
   TTreeReaderArray<int> el_passSelection_Medium = {fReader, "el_passSelection_Medium"};
   //Variables added by Bing
#ifdef DO_PHOTON
   TTreeReaderValue<Int_t> nph = {fReader, "nph"};
   TTreeReaderArray<float> ph_m = {fReader, "ph_m"};
   TTreeReaderArray<float> ph_pt = {fReader, "ph_pt"};
   TTreeReaderArray<float> ph_phi = {fReader, "ph_phi"};
   TTreeReaderArray<float> ph_eta = {fReader, "ph_eta"};
   TTreeReaderArray<int> ph_isIsolated_Cone40CaloOnly = {fReader, "ph_isIsolated_Cone40CaloOnly"};
   TTreeReaderArray<int> ph_isIsolated_Cone40 = {fReader, "ph_isIsolated_Cone40"};
   TTreeReaderArray<int> ph_isIsolated_Cone20 = {fReader, "ph_isIsolated_Cone20"};
   TTreeReaderArray<int> ph_IsLoose = {fReader, "ph_IsLoose"};
   TTreeReaderArray<int> ph_IsMedium = {fReader, "ph_IsMedium"};
   TTreeReaderArray<int> ph_IsTight = {fReader, "ph_IsTight"};
   TTreeReaderArray<int> ph_convType = {fReader, "ph_convType"};
#endif
   TTreeReaderValue<Int_t> nsecVtxTight = {fReader, "nsecVtxTight"};
   TTreeReaderArray<int> secVtxTight_ID = {fReader, "secVtxTight_ID"};
   TTreeReaderArray<float> secVtxTight_x = {fReader, "secVtxTight_x"};
   TTreeReaderArray<float> secVtxTight_y = {fReader, "secVtxTight_y"};
   TTreeReaderArray<float> secVtxTight_z = {fReader, "secVtxTight_z"};
   TTreeReaderArray<float> secVtxTight_r = {fReader, "secVtxTight_r"};
   TTreeReaderArray<float> secVtxTight_distFromPV = {fReader, "secVtxTight_distFromPV"};
   TTreeReaderArray<float> secVtxTight_pt = {fReader, "secVtxTight_pt"};
   TTreeReaderArray<float> secVtxTight_eta = {fReader, "secVtxTight_eta"};
   TTreeReaderArray<float> secVtxTight_phi = {fReader, "secVtxTight_phi"};
   TTreeReaderArray<float> secVtxTight_mass = {fReader, "secVtxTight_mass"};
   TTreeReaderArray<float> secVtxTight_mass_nonAssoc = {fReader, "secVtxTight_mass_nonAssoc"};
   TTreeReaderArray<std::vector<float>> secVtxTight_covariance = {fReader, "secVtxTight_covariance"};
   TTreeReaderArray<float> secVtxTight_chi2 = {fReader, "secVtxTight_chi2"};
   TTreeReaderArray<float> secVtxTight_direction = {fReader, "secVtxTight_direction"};
   TTreeReaderArray<int> secVtxTight_charge = {fReader, "secVtxTight_charge"};
   TTreeReaderArray<float> secVtxTight_H = {fReader, "secVtxTight_H"};
   TTreeReaderArray<float> secVtxTight_Ht = {fReader, "secVtxTight_Ht"};
   TTreeReaderArray<float> secVtxTight_minOpAng = {fReader, "secVtxTight_minOpAng"};
   TTreeReaderArray<float> secVtxTight_maxOpAng = {fReader, "secVtxTight_maxOpAng"};
   TTreeReaderArray<float> secVtxTight_maxDR_wrtSV = {fReader, "secVtxTight_maxDR_wrtSV"};
   TTreeReaderArray<float> secVtxTight_maxDR = {fReader, "secVtxTight_maxDR"};
   TTreeReaderArray<float> secVtxTight_mind0 = {fReader, "secVtxTight_mind0"};
   TTreeReaderArray<float> secVtxTight_maxd0 = {fReader, "secVtxTight_maxd0"};
   TTreeReaderArray<float> secVtxTight_minOneTrackRemovedMass = {fReader, "secVtxTight_minOneTrackRemovedMass"};
   TTreeReaderArray<std::vector<float>> secVtxTight_twoTracksMass = {fReader, "secVtxTight_twoTracksMass"};
   TTreeReaderArray<std::vector<float>> secVtxTight_twoTracksMassRest = {fReader, "secVtxTight_twoTracksMassRest"};
   TTreeReaderArray<std::vector<int>> secVtxTight_twoTracksCharge = {fReader, "secVtxTight_twoTracksCharge"};
   TTreeReaderArray<unsigned int> secVtxTight_ntrk = {fReader, "secVtxTight_ntrk"};
   TTreeReaderArray<unsigned int> secVtxTight_ntrk_lrt = {fReader, "secVtxTight_ntrk_lrt"};
   TTreeReaderArray<unsigned int> secVtxTight_ntrk_sel = {fReader, "secVtxTight_ntrk_sel"};
   TTreeReaderArray<unsigned int> secVtxTight_ntrk_assoc = {fReader, "secVtxTight_ntrk_assoc"};
   TTreeReaderArray<unsigned int> secVtxTight_pass_mat = {fReader, "secVtxTight_pass_mat"};
   TTreeReaderArray<std::vector<float>> secVtxTight_trk_chi2 = {fReader, "secVtxTight_trk_chi2"};
   TTreeReaderArray<std::vector<float>> secVtxTight_trk_pt_wrtSV = {fReader, "secVtxTight_trk_pt_wrtSV"};
   TTreeReaderArray<std::vector<float>> secVtxTight_trk_eta_wrtSV = {fReader, "secVtxTight_trk_eta_wrtSV"};
   TTreeReaderArray<std::vector<float>> secVtxTight_trk_phi_wrtSV = {fReader, "secVtxTight_trk_phi_wrtSV"};
   TTreeReaderArray<std::vector<float>> secVtxTight_trk_d0_wrtSV = {fReader, "secVtxTight_trk_d0_wrtSV"};
   TTreeReaderArray<std::vector<float>> secVtxTight_trk_z0_wrtSV = {fReader, "secVtxTight_trk_z0_wrtSV"};
   TTreeReaderArray<std::vector<float>> secVtxTight_trk_errd0_wrtSV = {fReader, "secVtxTight_trk_errd0_wrtSV"};
   TTreeReaderArray<std::vector<float>> secVtxTight_trk_errz0_wrtSV = {fReader, "secVtxTight_trk_errz0_wrtSV"};
   TTreeReaderArray<std::vector<float>> secVtxTight_trk_chi2_toSV = {fReader, "secVtxTight_trk_chi2_toSV"};
   TTreeReaderArray<float> secVtxTight_maxlinkTruth_score = {fReader, "secVtxTight_maxlinkTruth_score"};
   TTreeReaderArray<int> secVtxTight_maxlinkTruth_ID = {fReader, "secVtxTight_maxlinkTruth_ID"};

   TTreeReaderValue<Int_t> nsecVtxMedium = {fReader, "nsecVtxMedium"};
   TTreeReaderArray<int> secVtxMedium_ID = {fReader, "secVtxMedium_ID"};
   TTreeReaderArray<float> secVtxMedium_x = {fReader, "secVtxMedium_x"};
   TTreeReaderArray<float> secVtxMedium_y = {fReader, "secVtxMedium_y"};
   TTreeReaderArray<float> secVtxMedium_z = {fReader, "secVtxMedium_z"};
   TTreeReaderArray<float> secVtxMedium_r = {fReader, "secVtxMedium_r"};
   TTreeReaderArray<float> secVtxMedium_distFromPV = {fReader, "secVtxMedium_distFromPV"};
   TTreeReaderArray<float> secVtxMedium_pt = {fReader, "secVtxMedium_pt"};
   TTreeReaderArray<float> secVtxMedium_eta = {fReader, "secVtxMedium_eta"};
   TTreeReaderArray<float> secVtxMedium_phi = {fReader, "secVtxMedium_phi"};
   TTreeReaderArray<float> secVtxMedium_mass = {fReader, "secVtxMedium_mass"};
   TTreeReaderArray<float> secVtxMedium_mass_nonAssoc = {fReader, "secVtxMedium_mass_nonAssoc"};
   TTreeReaderArray<std::vector<float>> secVtxMedium_covariance = {fReader, "secVtxMedium_covariance"};
   TTreeReaderArray<float> secVtxMedium_chi2 = {fReader, "secVtxMedium_chi2"};
   TTreeReaderArray<float> secVtxMedium_direction = {fReader, "secVtxMedium_direction"};
   TTreeReaderArray<int> secVtxMedium_charge = {fReader, "secVtxMedium_charge"};
   TTreeReaderArray<float> secVtxMedium_H = {fReader, "secVtxMedium_H"};
   TTreeReaderArray<float> secVtxMedium_Ht = {fReader, "secVtxMedium_Ht"};
   TTreeReaderArray<float> secVtxMedium_minOpAng = {fReader, "secVtxMedium_minOpAng"};
   TTreeReaderArray<float> secVtxMedium_maxOpAng = {fReader, "secVtxMedium_maxOpAng"};
   TTreeReaderArray<float> secVtxMedium_maxDR_wrtSV = {fReader, "secVtxMedium_maxDR_wrtSV"};
   TTreeReaderArray<float> secVtxMedium_maxDR = {fReader, "secVtxMedium_maxDR"};
   TTreeReaderArray<float> secVtxMedium_mind0 = {fReader, "secVtxMedium_mind0"};
   TTreeReaderArray<float> secVtxMedium_maxd0 = {fReader, "secVtxMedium_maxd0"};
   TTreeReaderArray<float> secVtxMedium_minOneTrackRemovedMass = {fReader, "secVtxMedium_minOneTrackRemovedMass"};
   TTreeReaderArray<std::vector<float>> secVtxMedium_twoTracksMass = {fReader, "secVtxMedium_twoTracksMass"};
   TTreeReaderArray<std::vector<float>> secVtxMedium_twoTracksMassRest = {fReader, "secVtxMedium_twoTracksMassRest"};
   TTreeReaderArray<std::vector<int>> secVtxMedium_twoTracksCharge = {fReader, "secVtxMedium_twoTracksCharge"};
   TTreeReaderArray<unsigned int> secVtxMedium_ntrk = {fReader, "secVtxMedium_ntrk"};
   TTreeReaderArray<unsigned int> secVtxMedium_ntrk_lrt = {fReader, "secVtxMedium_ntrk_lrt"};
   TTreeReaderArray<unsigned int> secVtxMedium_ntrk_sel = {fReader, "secVtxMedium_ntrk_sel"};
   TTreeReaderArray<unsigned int> secVtxMedium_ntrk_assoc = {fReader, "secVtxMedium_ntrk_assoc"};
   TTreeReaderArray<unsigned int> secVtxMedium_pass_mat = {fReader, "secVtxMedium_pass_mat"};
   TTreeReaderArray<std::vector<float>> secVtxMedium_trk_chi2 = {fReader, "secVtxMedium_trk_chi2"};
   TTreeReaderArray<std::vector<float>> secVtxMedium_trk_pt_wrtSV = {fReader, "secVtxMedium_trk_pt_wrtSV"};
   TTreeReaderArray<std::vector<float>> secVtxMedium_trk_eta_wrtSV = {fReader, "secVtxMedium_trk_eta_wrtSV"};
   TTreeReaderArray<std::vector<float>> secVtxMedium_trk_phi_wrtSV = {fReader, "secVtxMedium_trk_phi_wrtSV"};
   TTreeReaderArray<std::vector<float>> secVtxMedium_trk_d0_wrtSV = {fReader, "secVtxMedium_trk_d0_wrtSV"};
   TTreeReaderArray<std::vector<float>> secVtxMedium_trk_z0_wrtSV = {fReader, "secVtxMedium_trk_z0_wrtSV"};
   TTreeReaderArray<std::vector<float>> secVtxMedium_trk_errd0_wrtSV = {fReader, "secVtxMedium_trk_errd0_wrtSV"};
   TTreeReaderArray<std::vector<float>> secVtxMedium_trk_errz0_wrtSV = {fReader, "secVtxMedium_trk_errz0_wrtSV"};
   TTreeReaderArray<std::vector<float>> secVtxMedium_trk_chi2_toSV = {fReader, "secVtxMedium_trk_chi2_toSV"};
   TTreeReaderArray<float> secVtxMedium_maxlinkTruth_score = {fReader, "secVtxMedium_maxlinkTruth_score"};
   TTreeReaderArray<int> secVtxMedium_maxlinkTruth_ID = {fReader, "secVtxMedium_maxlinkTruth_ID"};

   TTreeReaderValue<Int_t> nsecVtxLoose = {fReader, "nsecVtxLoose"};
   TTreeReaderArray<int> secVtxLoose_ID = {fReader, "secVtxLoose_ID"};
   TTreeReaderArray<float> secVtxLoose_x = {fReader, "secVtxLoose_x"};
   TTreeReaderArray<float> secVtxLoose_y = {fReader, "secVtxLoose_y"};
   TTreeReaderArray<float> secVtxLoose_z = {fReader, "secVtxLoose_z"};
   TTreeReaderArray<float> secVtxLoose_r = {fReader, "secVtxLoose_r"};
   TTreeReaderArray<float> secVtxLoose_distFromPV = {fReader, "secVtxLoose_distFromPV"};
   TTreeReaderArray<float> secVtxLoose_pt = {fReader, "secVtxLoose_pt"};
   TTreeReaderArray<float> secVtxLoose_eta = {fReader, "secVtxLoose_eta"};
   TTreeReaderArray<float> secVtxLoose_phi = {fReader, "secVtxLoose_phi"};
   TTreeReaderArray<float> secVtxLoose_mass = {fReader, "secVtxLoose_mass"};
   TTreeReaderArray<float> secVtxLoose_mass_nonAssoc = {fReader, "secVtxLoose_mass_nonAssoc"};
   TTreeReaderArray<std::vector<float>> secVtxLoose_covariance = {fReader, "secVtxLoose_covariance"};
   TTreeReaderArray<float> secVtxLoose_chi2 = {fReader, "secVtxLoose_chi2"};
   TTreeReaderArray<float> secVtxLoose_direction = {fReader, "secVtxLoose_direction"};
   TTreeReaderArray<int> secVtxLoose_charge = {fReader, "secVtxLoose_charge"};
   TTreeReaderArray<float> secVtxLoose_H = {fReader, "secVtxLoose_H"};
   TTreeReaderArray<float> secVtxLoose_Ht = {fReader, "secVtxLoose_Ht"};
   TTreeReaderArray<float> secVtxLoose_minOpAng = {fReader, "secVtxLoose_minOpAng"};
   TTreeReaderArray<float> secVtxLoose_maxOpAng = {fReader, "secVtxLoose_maxOpAng"};
   TTreeReaderArray<float> secVtxLoose_maxDR_wrtSV = {fReader, "secVtxLoose_maxDR_wrtSV"};
   TTreeReaderArray<float> secVtxLoose_maxDR = {fReader, "secVtxLoose_maxDR"};
   TTreeReaderArray<float> secVtxLoose_mind0 = {fReader, "secVtxLoose_mind0"};
   TTreeReaderArray<float> secVtxLoose_maxd0 = {fReader, "secVtxLoose_maxd0"};
   TTreeReaderArray<float> secVtxLoose_minOneTrackRemovedMass = {fReader, "secVtxLoose_minOneTrackRemovedMass"};
   TTreeReaderArray<std::vector<float>> secVtxLoose_twoTracksMass = {fReader, "secVtxLoose_twoTracksMass"};
   TTreeReaderArray<std::vector<float>> secVtxLoose_twoTracksMassRest = {fReader, "secVtxLoose_twoTracksMassRest"};
   TTreeReaderArray<std::vector<int>> secVtxLoose_twoTracksCharge = {fReader, "secVtxLoose_twoTracksCharge"};
   TTreeReaderArray<unsigned int> secVtxLoose_ntrk = {fReader, "secVtxLoose_ntrk"};
   TTreeReaderArray<unsigned int> secVtxLoose_ntrk_lrt = {fReader, "secVtxLoose_ntrk_lrt"};
   TTreeReaderArray<unsigned int> secVtxLoose_ntrk_sel = {fReader, "secVtxLoose_ntrk_sel"};
   TTreeReaderArray<unsigned int> secVtxLoose_ntrk_assoc = {fReader, "secVtxLoose_ntrk_assoc"};
   TTreeReaderArray<unsigned int> secVtxLoose_pass_mat = {fReader, "secVtxLoose_pass_mat"};
   TTreeReaderArray<std::vector<float>> secVtxLoose_trk_chi2 = {fReader, "secVtxLoose_trk_chi2"};
   TTreeReaderArray<std::vector<float>> secVtxLoose_trk_pt_wrtSV = {fReader, "secVtxLoose_trk_pt_wrtSV"};
   TTreeReaderArray<std::vector<float>> secVtxLoose_trk_eta_wrtSV = {fReader, "secVtxLoose_trk_eta_wrtSV"};
   TTreeReaderArray<std::vector<float>> secVtxLoose_trk_phi_wrtSV = {fReader, "secVtxLoose_trk_phi_wrtSV"};
   TTreeReaderArray<std::vector<float>> secVtxLoose_trk_d0_wrtSV = {fReader, "secVtxLoose_trk_d0_wrtSV"};
   TTreeReaderArray<std::vector<float>> secVtxLoose_trk_z0_wrtSV = {fReader, "secVtxLoose_trk_z0_wrtSV"};
   TTreeReaderArray<std::vector<float>> secVtxLoose_trk_errd0_wrtSV = {fReader, "secVtxLoose_trk_errd0_wrtSV"};
   TTreeReaderArray<std::vector<float>> secVtxLoose_trk_errz0_wrtSV = {fReader, "secVtxLoose_trk_errz0_wrtSV"};
   TTreeReaderArray<std::vector<float>> secVtxLoose_trk_chi2_toSV = {fReader, "secVtxLoose_trk_chi2_toSV"};
   TTreeReaderArray<float> secVtxLoose_maxlinkTruth_score = {fReader, "secVtxLoose_maxlinkTruth_score"};
   TTreeReaderArray<int> secVtxLoose_maxlinkTruth_ID = {fReader, "secVtxLoose_maxlinkTruth_ID"};

   TTreeReaderValue<Int_t> ntruthVtx = {fReader, "ntruthVtx"};
   TTreeReaderArray<int> truthVtx_ID = {fReader, "truthVtx_ID"};
   TTreeReaderArray<float> truthVtx_x = {fReader, "truthVtx_x"};
   TTreeReaderArray<float> truthVtx_y = {fReader, "truthVtx_y"};
   TTreeReaderArray<float> truthVtx_z = {fReader, "truthVtx_z"};
   TTreeReaderArray<float> truthVtx_r = {fReader, "truthVtx_r"};
   TTreeReaderArray<float> truthVtx_pt = {fReader, "truthVtx_pt"};
   TTreeReaderArray<float> truthVtx_eta = {fReader, "truthVtx_eta"};
   TTreeReaderArray<float> truthVtx_phi = {fReader, "truthVtx_phi"};
   TTreeReaderArray<float> truthVtx_mass = {fReader, "truthVtx_mass"};
   TTreeReaderArray<int> truthVtx_parent_pdgId = {fReader, "truthVtx_parent_pdgId"};
   TTreeReaderArray<int> truthVtx_maxlinkedRecoVtx_ID = {fReader, "truthVtx_maxlinkedRecoVtx_ID"};
   TTreeReaderArray<float> truthVtx_maxlinkedRecoVtx_score = {fReader, "truthVtx_maxlinkedRecoVtx_score"};




   NtupleReader(TTree * /*tree*/ =0) { }
   virtual ~NtupleReader() { }
   virtual Int_t   Version() const { return 2; }
   virtual void    Begin(TTree *tree);
   virtual void    SlaveBegin(TTree *tree);
   virtual void    Init(TTree *tree);
   virtual Bool_t  Notify();
   virtual Bool_t  Process(Long64_t entry);
   virtual Int_t   GetEntry(Long64_t entry, Int_t getall = 0) { return fChain ? fChain->GetTree()->GetEntry(entry, getall) : 0; }
   virtual void    SetOption(const char *option) { fOption = option; }
   virtual void    SetObject(TObject *obj) { fObject = obj; }
   virtual void    SetInputList(TList *input) { fInput = input; }
   virtual TList  *GetOutputList() const { return fOutput; }
   virtual void    SlaveTerminate();
   virtual void    Terminate();

   ClassDef(NtupleReader,0);

};

#endif

#ifdef NtupleReader_cxx
void NtupleReader::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the reader is initialized.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   fReader.SetTree(tree);
}

Bool_t NtupleReader::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}


#endif // #ifdef NtupleReader_cxx
