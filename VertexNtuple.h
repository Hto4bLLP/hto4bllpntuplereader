//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Thu Jan  9 09:37:48 2020 by ROOT version 6.14/08
// from TTree outTree/outTree
// found on file: /gpfs3/umass/jburzyns/VH4b/run/WpH_H125_a55a55_4b_ctau10_new/merged_output.root
//////////////////////////////////////////////////////////

#ifndef VertexNtuple_h
#define VertexNtuple_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TSelector.h>
#include <TTreeReader.h>
#include <TTreeReaderValue.h>
#include <TTreeReaderArray.h>

// Headers needed by this particular selector
#include <vector>
#include <string>
#include <TProofOutputFile.h>
//#include "Hto4bParameters.h"
#include "Tools/MCConfig.h"
#include "Tools/Params.h"


class VertexNtuple : public TSelector {
private :
   std::map<TString, TH1F*> histo; //!
   TProofOutputFile *m_prooffile; //!
   TFile *m_outfile; //!
   Params *m_params; //->
   Bool_t m_isMC; //!
   Bool_t m_isSignal = true; //!
   Bool_t m_UsePROOF; //!
   TH1F *m_cutflow_weighted; //!
   TH1F *m_cutflow_raw; //!
   TTree* miniTree; //!
   //std::vector <Selection> m_selection; //!
   UInt_t m_ntrk_cut = 2;
   Float_t m_mass_cut = 3.0;

public :
   TTreeReader     fReader;  //!the tree reader
   TTree          *fChain = 0;   //!pointer to the analyzed TTree or TChain

   // Analysis Methods
   virtual void BookMiniTree();
   virtual void FillMiniTree();

   Float_t miniTree_mcEventWeight;
   Float_t miniTree_pileupEventWeight;
   Float_t miniTree_XsecEventWeight;
   Float_t miniTree_lumiEventWeight;
   Float_t miniTree_totalEventWeight;
   ////////////////////////////////////////////////////
   //////////// Vertex Level Variables ////////////////
   ////////////////////////////////////////////////////

   Float_t miniTree_secVtx_pt;
   Float_t miniTree_secVtx_eta;
   Float_t miniTree_secVtx_phi;
   Float_t miniTree_secVtx_m;
   Float_t miniTree_secVtx_m_nonAssoc;
   Float_t miniTree_secVtx_chi2;
   Float_t miniTree_secVtx_minOpAng;
   Float_t miniTree_secVtx_maxOpAng;
   Float_t miniTree_secVtx_mind0;
   Float_t miniTree_secVtx_maxd0;
   Float_t miniTree_secVtx_minOneTrackRemovedMass;
   Float_t miniTree_secVtx_r;
   Float_t miniTree_secVtx_z;
   

   Float_t miniTree_secVtx_H;
   Float_t miniTree_secVtx_Ht;
   Float_t miniTree_secVtx_charge;

   Float_t miniTree_secVtx_distFromPV;
   Int_t   miniTree_secVtx_ntrk;
   Int_t   miniTree_secVtx_ntrk_lrt;
   Int_t   miniTree_secVtx_ntrk_sel;
   Int_t   miniTree_secVtx_ntrk_assoc;
   Float_t miniTree_secVtx_maxDR;
   Float_t miniTree_secVtx_sumPt;
   Float_t miniTree_secVtx_sumPt2;
   Float_t miniTree_secVtx_maxDphi;
   Float_t miniTree_secVtx_minVtxDR;
   Float_t miniTree_secVtx_minJetDR;

   Float_t miniTree_secVtx_nPixelHits;
   Float_t miniTree_secVtx_nSCTHits;
   Float_t miniTree_secVtx_nTRTHits;
   Float_t miniTree_secVtx_nSiHits;

   // Readers to access the data (delete the ones you do not need).
   TTreeReaderValue<Int_t> runNumber = {fReader, "runNumber"};
   TTreeReaderValue<Long64_t> eventNumber = {fReader, "eventNumber"};
   TTreeReaderValue<Int_t> lumiBlock = {fReader, "lumiBlock"};
   TTreeReaderValue<UInt_t> coreFlags = {fReader, "coreFlags"};
   TTreeReaderValue<Int_t> bcid = {fReader, "bcid"};
   TTreeReaderValue<Int_t> mcEventNumber = {fReader, "mcEventNumber"};
   TTreeReaderValue<Int_t> mcChannelNumber = {fReader, "mcChannelNumber"};
   TTreeReaderValue<Float_t> mcEventWeight = {fReader, "mcEventWeight"};
   TTreeReaderValue<Int_t> NPV = {fReader, "NPV"};
   TTreeReaderValue<Float_t> actualInteractionsPerCrossing = {fReader, "actualInteractionsPerCrossing"};
   TTreeReaderValue<Float_t> averageInteractionsPerCrossing = {fReader, "averageInteractionsPerCrossing"};
   TTreeReaderValue<Float_t> weight_pileup = {fReader, "weight_pileup"};
   TTreeReaderValue<Float_t> correctedAverageMu = {fReader, "correctedAverageMu"};
   TTreeReaderValue<Float_t> correctedAndScaledAverageMu = {fReader, "correctedAndScaledAverageMu"};
   TTreeReaderValue<Float_t> correctedActualMu = {fReader, "correctedActualMu"};
   TTreeReaderValue<Float_t> correctedAndScaledActualMu = {fReader, "correctedAndScaledActualMu"};
   TTreeReaderValue<Int_t> rand_run_nr = {fReader, "rand_run_nr"};
   TTreeReaderValue<Int_t> rand_lumiblock_nr = {fReader, "rand_lumiblock_nr"};



   TTreeReaderValue<Int_t> njet = {fReader, "njet"};
   TTreeReaderArray<float> jet_E = {fReader, "jet_E"};
   TTreeReaderArray<float> jet_pt = {fReader, "jet_pt"};
   TTreeReaderArray<float> jet_phi = {fReader, "jet_phi"};
   TTreeReaderArray<float> jet_eta = {fReader, "jet_eta"};
   TTreeReaderArray<float> jet_rapidity = {fReader, "jet_rapidity"};
   TTreeReaderArray<float> jet_Timing = {fReader, "jet_Timing"};
   TTreeReaderArray<float> jet_LArQuality = {fReader, "jet_LArQuality"};
   TTreeReaderArray<float> jet_HECQuality = {fReader, "jet_HECQuality"};
   TTreeReaderArray<float> jet_NegativeE = {fReader, "jet_NegativeE"};
   TTreeReaderArray<float> jet_AverageLArQF = {fReader, "jet_AverageLArQF"};
   TTreeReaderArray<float> jet_BchCorrCell = {fReader, "jet_BchCorrCell"};
   TTreeReaderArray<float> jet_N90Constituents = {fReader, "jet_N90Constituents"};
   TTreeReaderArray<float> jet_LArBadHVEnergyFrac = {fReader, "jet_LArBadHVEnergyFrac"};
   TTreeReaderArray<int> jet_LArBadHVNCell = {fReader, "jet_LArBadHVNCell"};
   TTreeReaderArray<float> jet_OotFracClusters5 = {fReader, "jet_OotFracClusters5"};
   TTreeReaderArray<float> jet_OotFracClusters10 = {fReader, "jet_OotFracClusters10"};
   TTreeReaderArray<float> jet_LeadingClusterPt = {fReader, "jet_LeadingClusterPt"};
   TTreeReaderArray<float> jet_LeadingClusterSecondLambda = {fReader, "jet_LeadingClusterSecondLambda"};
   TTreeReaderArray<float> jet_LeadingClusterCenterLambda = {fReader, "jet_LeadingClusterCenterLambda"};
   TTreeReaderArray<float> jet_LeadingClusterSecondR = {fReader, "jet_LeadingClusterSecondR"};
   TTreeReaderArray<int> jet_clean_passLooseBadUgly = {fReader, "jet_clean_passLooseBadUgly"};
   TTreeReaderArray<int> jet_clean_passTightBadUgly = {fReader, "jet_clean_passTightBadUgly"};
   TTreeReaderArray<int> jet_clean_passLooseBad = {fReader, "jet_clean_passLooseBad"};
   TTreeReaderArray<int> jet_clean_passTightBad = {fReader, "jet_clean_passTightBad"};
   TTreeReaderArray<float> jet_alpha_max = {fReader, "jet_alpha_max"};
   TTreeReaderArray<int> jet_alpha_max_pass = {fReader, "jet_alpha_max_pass"};
   TTreeReaderArray<float> jet_chf = {fReader, "jet_chf"};
   TTreeReaderArray<int> jet_chf_pass = {fReader, "jet_chf_pass"};
   TTreeReaderArray<float> jet_track_pt_max = {fReader, "jet_track_pt_max"};
   TTreeReaderArray<int> jet_ntracks = {fReader, "jet_ntracks"};
   TTreeReaderArray<float> jet_LooseDR = {fReader, "jet_LooseDR"};
   TTreeReaderArray<float> jet_MediumDR = {fReader, "jet_MediumDR"};
   TTreeReaderArray<float> jet_TightDR = {fReader, "jet_TightDR"};
   TTreeReaderArray<float> jet_LooseDPhi = {fReader, "jet_LooseDPhi"};
   TTreeReaderArray<float> jet_MediumDPhi = {fReader, "jet_MediumDPhi"};
   TTreeReaderArray<float> jet_TightDPhi = {fReader, "jet_TightDPhi"};
   TTreeReaderArray<int> jet_LLP_index = {fReader, "jet_LLP_index"};
   TTreeReaderArray<int> jet_passBaseline = {fReader, "jet_passBaseline"};
   TTreeReaderArray<int> jet_passPresel = {fReader, "jet_passPresel"};
   TTreeReaderArray<int> jet_passFilter = {fReader, "jet_passFilter"};


   TTreeReaderArray<int> secVtx_ID = {fReader, "secVtx_ID"};
   TTreeReaderArray<float> secVtx_x = {fReader, "secVtx_x"};
   TTreeReaderArray<float> secVtx_y = {fReader, "secVtx_y"};
   TTreeReaderArray<float> secVtx_z = {fReader, "secVtx_z"};
   TTreeReaderArray<float> secVtx_r = {fReader, "secVtx_r"};
   TTreeReaderArray<float> secVtx_distFromPV = {fReader, "secVtx_distFromPV"};
   TTreeReaderArray<float> secVtx_pt = {fReader, "secVtx_pt"};
   TTreeReaderArray<float> secVtx_eta = {fReader, "secVtx_eta"};
   TTreeReaderArray<float> secVtx_phi = {fReader, "secVtx_phi"};
   TTreeReaderArray<float> secVtx_mass = {fReader, "secVtx_mass"};
   TTreeReaderArray<float> secVtx_mass_nonAssoc = {fReader, "secVtx_mass_nonAssoc"};
   TTreeReaderArray<std::vector<float>> secVtx_covariance = {fReader, "secVtx_covariance"};
   TTreeReaderArray<float> secVtx_chi2 = {fReader, "secVtx_chi2"};
   TTreeReaderArray<float> secVtx_direction = {fReader, "secVtx_direction"};
   TTreeReaderArray<int> secVtx_charge = {fReader, "secVtx_charge"};
   TTreeReaderArray<float> secVtx_H = {fReader, "secVtx_H"};
   TTreeReaderArray<float> secVtx_Ht = {fReader, "secVtx_Ht"};
   TTreeReaderArray<float> secVtx_minOpAng = {fReader, "secVtx_minOpAng"};
   TTreeReaderArray<float> secVtx_maxOpAng = {fReader, "secVtx_maxOpAng"};
   TTreeReaderArray<float> secVtx_mind0 = {fReader, "secVtx_mind0"};
   TTreeReaderArray<float> secVtx_maxd0 = {fReader, "secVtx_maxd0"};
   TTreeReaderArray<float> secVtx_minOneTrackRemovedMass = {fReader, "secVtx_minOneTrackRemovedMass"};
   TTreeReaderArray<std::vector<float>> secVtx_twoTracksMass = {fReader, "secVtx_twoTracksMass"};
   TTreeReaderArray<std::vector<float>> secVtx_twoTracksMassRest = {fReader, "secVtx_twoTracksMassRest"};
   TTreeReaderArray<std::vector<int>> secVtx_twoTracksCharge = {fReader, "secVtx_twoTracksCharge"};
   TTreeReaderArray<unsigned int> secVtx_ntrk = {fReader, "secVtx_ntrk"};
   TTreeReaderArray<unsigned int> secVtx_ntrk_lrt = {fReader, "secVtx_ntrk_lrt"};
   TTreeReaderArray<unsigned int> secVtx_ntrk_sel = {fReader, "secVtx_ntrk_sel"};
   TTreeReaderArray<unsigned int> secVtx_ntrk_assoc = {fReader, "secVtx_ntrk_assoc"};
   TTreeReaderArray<unsigned int> secVtx_pass_mat = {fReader, "secVtx_pass_mat"};
   TTreeReaderArray<std::vector<int>> secVtx_trk_ID = {fReader, "secVtx_trk_ID"};
   TTreeReaderArray<std::vector<float>> secVtx_trk_qOverP = {fReader, "secVtx_trk_qOverP"};
   TTreeReaderArray<std::vector<float>> secVtx_trk_theta = {fReader, "secVtx_trk_theta"};
   TTreeReaderArray<std::vector<float>> secVtx_trk_vz = {fReader, "secVtx_trk_vz"};
   TTreeReaderArray<std::vector<float>> secVtx_trk_E = {fReader, "secVtx_trk_E"};
   TTreeReaderArray<std::vector<float>> secVtx_trk_M = {fReader, "secVtx_trk_M"};
   TTreeReaderArray<std::vector<float>> secVtx_trk_pt = {fReader, "secVtx_trk_pt"};
   TTreeReaderArray<std::vector<float>> secVtx_trk_eta = {fReader, "secVtx_trk_eta"};
   TTreeReaderArray<std::vector<float>> secVtx_trk_phi = {fReader, "secVtx_trk_phi"};
   TTreeReaderArray<std::vector<float>> secVtx_trk_d0 = {fReader, "secVtx_trk_d0"};
   TTreeReaderArray<std::vector<float>> secVtx_trk_z0 = {fReader, "secVtx_trk_z0"};
   TTreeReaderArray<std::vector<float>> secVtx_trk_errd0 = {fReader, "secVtx_trk_errd0"};
   TTreeReaderArray<std::vector<float>> secVtx_trk_errz0 = {fReader, "secVtx_trk_errz0"};
   TTreeReaderArray<std::vector<float>> secVtx_trk_chi2 = {fReader, "secVtx_trk_chi2"};
   TTreeReaderArray<std::vector<float>> secVtx_trk_pt_wrtSV = {fReader, "secVtx_trk_pt_wrtSV"};
   TTreeReaderArray<std::vector<float>> secVtx_trk_eta_wrtSV = {fReader, "secVtx_trk_eta_wrtSV"};
   TTreeReaderArray<std::vector<float>> secVtx_trk_phi_wrtSV = {fReader, "secVtx_trk_phi_wrtSV"};
   TTreeReaderArray<std::vector<float>> secVtx_trk_d0_wrtSV = {fReader, "secVtx_trk_d0_wrtSV"};
   TTreeReaderArray<std::vector<float>> secVtx_trk_z0_wrtSV = {fReader, "secVtx_trk_z0_wrtSV"};
   TTreeReaderArray<std::vector<float>> secVtx_trk_errd0_wrtSV = {fReader, "secVtx_trk_errd0_wrtSV"};
   TTreeReaderArray<std::vector<float>> secVtx_trk_errz0_wrtSV = {fReader, "secVtx_trk_errz0_wrtSV"};
   TTreeReaderArray<std::vector<float>> secVtx_trk_chi2_toSV = {fReader, "secVtx_trk_chi2_toSV"};
   TTreeReaderArray<std::vector<int>> secVtx_trk_charge = {fReader, "secVtx_trk_charge"};
   TTreeReaderArray<std::vector<unsigned char>> secVtx_trk_isFinal = {fReader, "secVtx_trk_isFinal"};
   TTreeReaderArray<std::vector<unsigned char>> secVtx_trk_isSelected = {fReader, "secVtx_trk_isSelected"};
   TTreeReaderArray<std::vector<unsigned char>> secVtx_trk_isAssociated = {fReader, "secVtx_trk_isAssociated"};
   TTreeReaderArray<std::vector<unsigned char>> secVtx_trk_nPixelHits = {fReader, "secVtx_trk_nPixelHits"};
   TTreeReaderArray<std::vector<unsigned char>> secVtx_trk_nSCTHits = {fReader, "secVtx_trk_nSCTHits"};
   TTreeReaderArray<std::vector<unsigned char>> secVtx_trk_nTRTHits = {fReader, "secVtx_trk_nTRTHits"};
   TTreeReaderArray<std::vector<unsigned char>> secVtx_trk_nSiHits = {fReader, "secVtx_trk_nSiHits"};
   TTreeReaderArray<std::vector<unsigned char>> secVtx_trk_nPixelBarrelLayers = {fReader, "secVtx_trk_nPixelBarrelLayers"};
   TTreeReaderArray<std::vector<unsigned char>> secVtx_trk_nPixelEndCapLayers = {fReader, "secVtx_trk_nPixelEndCapLayers"};
   TTreeReaderArray<std::vector<unsigned char>> secVtx_trk_nSCTBarrelLayers = {fReader, "secVtx_trk_nSCTBarrelLayers"};
   TTreeReaderArray<std::vector<unsigned char>> secVtx_trk_nSCTEndCapLayers = {fReader, "secVtx_trk_nSCTEndCapLayers"};
   TTreeReaderArray<std::vector<unsigned int>> secVtx_trk_hitPattern = {fReader, "secVtx_trk_hitPattern"};
   TTreeReaderArray<std::vector<float>> secVtx_trk_dEdx = {fReader, "secVtx_trk_dEdx"};
   TTreeReaderArray<std::vector<float>> secVtx_trk_radiusOfFirstHit = {fReader, "secVtx_trk_radiusOfFirstHit"};
   TTreeReaderArray<std::vector<float>> secVtx_trk_truthProb = {fReader, "secVtx_trk_truthProb"};
   TTreeReaderArray<std::vector<int>> secVtx_trk_truthID = {fReader, "secVtx_trk_truthID"};
   TTreeReaderArray<std::vector<int>> secVtx_trk_truthBarcode = {fReader, "secVtx_trk_truthBarcode"};
   TTreeReaderArray<std::vector<int>> secVtx_trk_truthPdgId = {fReader, "secVtx_trk_truthPdgId"};
   TTreeReaderArray<std::vector<unsigned char>> secVtx_trk_truthPointsToClosestTV = {fReader, "secVtx_trk_truthPointsToClosestTV"};
   TTreeReaderArray<std::vector<unsigned char>> secVtx_trk_truthPointsToMaxlinkTV = {fReader, "secVtx_trk_truthPointsToMaxlinkTV"};
   TTreeReaderArray<std::vector<unsigned char>> secVtx_trk_truthParentPointsToMaxlinkPTV = {fReader, "secVtx_trk_truthParentPointsToMaxlinkPTV"};
   TTreeReaderArray<std::vector<float>> secVtx_closeTruth_distance = {fReader, "secVtx_closeTruth_distance"};
   TTreeReaderArray<std::vector<int>> secVtx_closeTruth_ID = {fReader, "secVtx_closeTruth_ID"};
   TTreeReaderArray<std::vector<int>> secVtx_closeTruth_barcode = {fReader, "secVtx_closeTruth_barcode"};
   TTreeReaderArray<float> secVtx_closestTruth_distance = {fReader, "secVtx_closestTruth_distance"};
   TTreeReaderArray<int> secVtx_closestTruth_ID = {fReader, "secVtx_closestTruth_ID"};
   TTreeReaderArray<float> secVtx_closestTruth_x = {fReader, "secVtx_closestTruth_x"};
   TTreeReaderArray<float> secVtx_closestTruth_y = {fReader, "secVtx_closestTruth_y"};
   TTreeReaderArray<float> secVtx_closestTruth_z = {fReader, "secVtx_closestTruth_z"};
   TTreeReaderArray<float> secVtx_closestTruth_r = {fReader, "secVtx_closestTruth_r"};
   TTreeReaderArray<float> secVtx_closestTruth_eta = {fReader, "secVtx_closestTruth_eta"};
   TTreeReaderArray<float> secVtx_closestTruth_phi = {fReader, "secVtx_closestTruth_phi"};
   TTreeReaderArray<int> secVtx_closestTruth_barcode = {fReader, "secVtx_closestTruth_barcode"};
   TTreeReaderArray<std::vector<float>> secVtx_linkTruth_score = {fReader, "secVtx_linkTruth_score"};
   TTreeReaderArray<std::vector<int>> secVtx_linkTruth_ID = {fReader, "secVtx_linkTruth_ID"};
   TTreeReaderArray<std::vector<int>> secVtx_linkTruth_barcode = {fReader, "secVtx_linkTruth_barcode"};
   TTreeReaderArray<float> secVtx_maxlinkTruth_score = {fReader, "secVtx_maxlinkTruth_score"};
   TTreeReaderArray<int> secVtx_maxlinkTruth_ID = {fReader, "secVtx_maxlinkTruth_ID"};
   TTreeReaderArray<float> secVtx_maxlinkTruth_x = {fReader, "secVtx_maxlinkTruth_x"};
   TTreeReaderArray<float> secVtx_maxlinkTruth_y = {fReader, "secVtx_maxlinkTruth_y"};
   TTreeReaderArray<float> secVtx_maxlinkTruth_z = {fReader, "secVtx_maxlinkTruth_z"};
   TTreeReaderArray<float> secVtx_maxlinkTruth_r = {fReader, "secVtx_maxlinkTruth_r"};
   TTreeReaderArray<float> secVtx_maxlinkTruth_eta = {fReader, "secVtx_maxlinkTruth_eta"};
   TTreeReaderArray<float> secVtx_maxlinkTruth_phi = {fReader, "secVtx_maxlinkTruth_phi"};
   TTreeReaderArray<int> secVtx_maxlinkTruth_barcode = {fReader, "secVtx_maxlinkTruth_barcode"};
   TTreeReaderValue<Int_t> ntruthVtx = {fReader, "ntruthVtx"};
   TTreeReaderArray<int> truthVtx_ID = {fReader, "truthVtx_ID"};
   TTreeReaderArray<float> truthVtx_x = {fReader, "truthVtx_x"};
   TTreeReaderArray<float> truthVtx_y = {fReader, "truthVtx_y"};
   TTreeReaderArray<float> truthVtx_z = {fReader, "truthVtx_z"};
   TTreeReaderArray<float> truthVtx_r = {fReader, "truthVtx_r"};
   TTreeReaderArray<float> truthVtx_pt = {fReader, "truthVtx_pt"};
   TTreeReaderArray<float> truthVtx_eta = {fReader, "truthVtx_eta"};
   TTreeReaderArray<float> truthVtx_phi = {fReader, "truthVtx_phi"};
   TTreeReaderArray<float> truthVtx_mass = {fReader, "truthVtx_mass"};
   TTreeReaderArray<int> truthVtx_nOutP = {fReader, "truthVtx_nOutP"};
   TTreeReaderArray<int> truthVtx_barcode = {fReader, "truthVtx_barcode"};
   TTreeReaderArray<int> truthVtx_parent_ID = {fReader, "truthVtx_parent_ID"};
   TTreeReaderArray<float> truthVtx_parent_pt = {fReader, "truthVtx_parent_pt"};
   TTreeReaderArray<float> truthVtx_parent_eta = {fReader, "truthVtx_parent_eta"};
   TTreeReaderArray<float> truthVtx_parent_phi = {fReader, "truthVtx_parent_phi"};
   TTreeReaderArray<float> truthVtx_parent_E = {fReader, "truthVtx_parent_E"};
   TTreeReaderArray<float> truthVtx_parent_M = {fReader, "truthVtx_parent_M"};
   TTreeReaderArray<float> truthVtx_parent_charge = {fReader, "truthVtx_parent_charge"};
   TTreeReaderArray<int> truthVtx_parent_pdgId = {fReader, "truthVtx_parent_pdgId"};
   TTreeReaderArray<int> truthVtx_parent_status = {fReader, "truthVtx_parent_status"};
   TTreeReaderArray<int> truthVtx_parent_barcode = {fReader, "truthVtx_parent_barcode"};
   TTreeReaderArray<int> truthVtx_parent_prod_ID = {fReader, "truthVtx_parent_prod_ID"};
   TTreeReaderArray<float> truthVtx_parent_prod_x = {fReader, "truthVtx_parent_prod_x"};
   TTreeReaderArray<float> truthVtx_parent_prod_y = {fReader, "truthVtx_parent_prod_y"};
   TTreeReaderArray<float> truthVtx_parent_prod_z = {fReader, "truthVtx_parent_prod_z"};
   TTreeReaderArray<float> truthVtx_parent_prod_r = {fReader, "truthVtx_parent_prod_r"};
   TTreeReaderArray<float> truthVtx_parent_prod_eta = {fReader, "truthVtx_parent_prod_eta"};
   TTreeReaderArray<float> truthVtx_parent_prod_phi = {fReader, "truthVtx_parent_prod_phi"};
   TTreeReaderArray<int> truthVtx_parent_prod_barcode = {fReader, "truthVtx_parent_prod_barcode"};
   TTreeReaderArray<std::vector<int>> truthVtx_outP_ID = {fReader, "truthVtx_outP_ID"};
   TTreeReaderArray<std::vector<float>> truthVtx_outP_pt = {fReader, "truthVtx_outP_pt"};
   TTreeReaderArray<std::vector<float>> truthVtx_outP_eta = {fReader, "truthVtx_outP_eta"};
   TTreeReaderArray<std::vector<float>> truthVtx_outP_phi = {fReader, "truthVtx_outP_phi"};
   TTreeReaderArray<std::vector<float>> truthVtx_outP_E = {fReader, "truthVtx_outP_E"};
   TTreeReaderArray<std::vector<float>> truthVtx_outP_M = {fReader, "truthVtx_outP_M"};
   TTreeReaderArray<std::vector<float>> truthVtx_outP_charge = {fReader, "truthVtx_outP_charge"};
   TTreeReaderArray<std::vector<int>> truthVtx_outP_pdgId = {fReader, "truthVtx_outP_pdgId"};
   TTreeReaderArray<std::vector<int>> truthVtx_outP_status = {fReader, "truthVtx_outP_status"};
   TTreeReaderArray<std::vector<int>> truthVtx_outP_barcode = {fReader, "truthVtx_outP_barcode"};
   TTreeReaderArray<std::vector<unsigned char>> truthVtx_outP_isReco = {fReader, "truthVtx_outP_isReco"};
   TTreeReaderArray<std::vector<float>> truthVtx_outP_recoProb = {fReader, "truthVtx_outP_recoProb"};
   TTreeReaderArray<std::vector<int>> truthVtx_outP_recoID = {fReader, "truthVtx_outP_recoID"};
   TTreeReaderArray<std::vector<unsigned char>> truthVtx_outP_recoIsSelected = {fReader, "truthVtx_outP_recoIsSelected"};
   TTreeReaderArray<std::vector<unsigned char>> truthVtx_outP_recoIsAssociated = {fReader, "truthVtx_outP_recoIsAssociated"};
   TTreeReaderArray<std::vector<unsigned char>> truthVtx_outP_isStable = {fReader, "truthVtx_outP_isStable"};
   TTreeReaderArray<std::vector<unsigned char>> truthVtx_outP_isInteracting = {fReader, "truthVtx_outP_isInteracting"};
   TTreeReaderArray<std::vector<unsigned char>> truthVtx_outP_isReconstructible = {fReader, "truthVtx_outP_isReconstructible"};
   TTreeReaderArray<std::vector<int>> truthVtx_linkedRecoVtx_ID = {fReader, "truthVtx_linkedRecoVtx_ID"};
   TTreeReaderArray<std::vector<float>> truthVtx_linkedRecoVtx_score = {fReader, "truthVtx_linkedRecoVtx_score"};
   TTreeReaderArray<int> truthVtx_maxlinkedRecoVtx_ID = {fReader, "truthVtx_maxlinkedRecoVtx_ID"};
   TTreeReaderArray<float> truthVtx_maxlinkedRecoVtx_score = {fReader, "truthVtx_maxlinkedRecoVtx_score"};


   VertexNtuple(TTree * /*tree*/ =0) { }
   virtual ~VertexNtuple() { }
   virtual Int_t   Version() const { return 2; }
   virtual void    Begin(TTree *tree);
   virtual void    SlaveBegin(TTree *tree);
   virtual void    Init(TTree *tree);
   virtual Bool_t  Notify();
   virtual Bool_t  Process(Long64_t entry);
   virtual Int_t   GetEntry(Long64_t entry, Int_t getall = 0) { return fChain ? fChain->GetTree()->GetEntry(entry, getall) : 0; }
   virtual void    SetOption(const char *option) { fOption = option; }
   virtual void    SetObject(TObject *obj) { fObject = obj; }
   virtual void    SetInputList(TList *input) { fInput = input; }
   virtual TList  *GetOutputList() const { return fOutput; }
   virtual void    SlaveTerminate();
   virtual void    Terminate();

   ClassDef(VertexNtuple,0);

};

#endif

#ifdef VertexNtuple_cxx
void VertexNtuple::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the reader is initialized.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   fReader.SetTree(tree);
}

Bool_t VertexNtuple::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}


#endif // #ifdef VertexNtuple_cxx
