#!/usr/bin/env python

# Select Theano as backend for Keras
from os import environ
environ['KERAS_BACKEND'] = 'theano'

# Set architecture of system (AVX instruction set is not supported on SWAN)
environ['THEANO_FLAGS'] = 'gcc.cxxflags=-march=corei7'


from ROOT import TMVA, TFile, TTree, TCut
from subprocess import call
from os.path import isfile

from keras.models import Sequential
from keras.layers.core import Dense
from keras.optimizers import Adam

# Setup TMVA
TMVA.PyMethodBase.PyInitialize()

output = TFile.Open('BinaryClassificationKeras.root', 'RECREATE')
factory = TMVA.Factory('TMVAClassification', output,
        '!V:!Silent:Color:DrawProgressBar:AnalysisType=Classification')
# Load data
sigFile = TFile.Open('/data/jburzyns/NtupleReader/VertexTreesLRTTrim/SignalAll.root')
bkgFile = TFile.Open('/data/jburzyns/NtupleReader/VertexTreesLRTTrim/ZJets2015-2017.root')
signal = sigFile.Get('tree')
background = bkgFile.Get('tree')

dataloader = TMVA.DataLoader('VertexClassifier')
dataloader.AddVariable("secVtx_r")
dataloader.AddVariable("secVtx_z")
dataloader.AddVariable("secVtx_pt")
dataloader.AddVariable("secVtx_eta")
dataloader.AddVariable("secVtx_phi")
dataloader.AddVariable("secVtx_m")
dataloader.AddVariable("secVtx_m_nonAssoc")
dataloader.AddVariable("secVtx_minOneTrackRemovedMass")
dataloader.AddVariable("secVtx_ntrk")
dataloader.AddVariable("secVtx_ntrk_lrt")
dataloader.AddVariable("secVtx_ntrk_sel")
dataloader.AddVariable("secVtx_ntrk_assoc")
dataloader.AddVariable("secVtx_chi2")
dataloader.AddVariable("secVtx_minOpAng")
dataloader.AddVariable("secVtx_maxOpAng")
dataloader.AddVariable("secVtx_mind0")
dataloader.AddVariable("secVtx_maxd0")
dataloader.AddVariable("secVtx_maxDR")
dataloader.AddVariable("secVtx_maxDphi")
dataloader.AddVariable("secVtx_sumPt2")
dataloader.AddVariable("secVtx_H")
dataloader.AddVariable("secVtx_Ht")
dataloader.AddVariable("secVtx_charge")
dataloader.AddVariable("secVtx_nPixelHits")
dataloader.AddVariable("secVtx_nSCTHits")
dataloader.AddVariable("secVtx_nTRTHits")
dataloader.AddVariable("secVtx_nSiHits")

dataloader.AddSignalTree(signal, 1.0)
dataloader.AddBackgroundTree(background, 1.0)
dataloader.PrepareTrainingAndTestTree(TCut(''),
        'nTrain_Signal=200000:nTrain_Background=500000:SplitMode=Random:NormMode=NumEvents:!V')

# Generate model

# Define model
model = Sequential()
model.add(Dense(32, init='glorot_normal', activation='relu', input_dim=27))
model.add(Dense(2, init='glorot_uniform', activation='softmax'))

# Set loss and optimizer
model.compile(loss='categorical_crossentropy', optimizer=Adam(), metrics=['accuracy',])

# Store model to file
model.save('model.h5')
model.summary()

# Book methods
factory.BookMethod(dataloader, TMVA.Types.kPyKeras, 'PyKeras',
        'FilenameModel=model.h5:NumEpochs=10:BatchSize=32')

# Run training, test and evaluation
factory.TrainAllMethods()
factory.TestAllMethods()
factory.EvaluateAllMethods()
