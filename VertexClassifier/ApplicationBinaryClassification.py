#!/usr/bin/env python

# Select Theano as backend for Keras
from os import environ
environ['KERAS_BACKEND'] = 'theano'

# Set architecture of system (AVX instruction set is not supported on SWAN)
#environ['THEANO_FLAGS'] = 'gcc.cxxflags=-march=corei7'

from ROOT import TMVA, TFile, TString, TH1F
from array import array
from subprocess import call
from os.path import isfile

from plot_classes import Hist1D, HistStack

# Setup TMVA
TMVA.Tools.Instance()
TMVA.PyMethodBase.PyInitialize()
reader = TMVA.Reader("Color:!Silent")

# Load data
ma15_ctau100 = TFile.Open('/data/jburzyns/NtupleReader/VertexTreesLRTTrim/WH_a15a15_4b_ctau100.root')
ma15_ctau10  = TFile.Open('/data/jburzyns/NtupleReader/VertexTreesLRTTrim/WH_a15a15_4b_ctau10.root')
ma15_ctau1   = TFile.Open('/data/jburzyns/NtupleReader/VertexTreesLRTTrim/WH_a15a15_4b_ctau1.root')
ma55_ctau100 = TFile.Open('/data/jburzyns/NtupleReader/VertexTreesLRTTrim/WH_a55a55_4b_ctau100.root')
ma55_ctau10  = TFile.Open('/data/jburzyns/NtupleReader/VertexTreesLRTTrim/WH_a55a55_4b_ctau10.root')
ma55_ctau1   = TFile.Open('/data/jburzyns/NtupleReader/VertexTreesLRTTrim/WH_a55a55_4b_ctau1.root')
ZJets        = TFile.Open('/data/jburzyns/NtupleReader/VertexTreesLRTTrim/ZJets2015-2017.root')

trees = [
  ZJets.Get('tree'),
  ma15_ctau100.Get('tree'),
  ma15_ctau10.Get('tree'),
  ma15_ctau1.Get('tree'),
  ma55_ctau100.Get('tree'),
  ma55_ctau10.Get('tree'),
  ma55_ctau1.Get('tree'),
]


dataloader = TMVA.DataLoader('VertexClassifier')
dataloader.AddVariable("secVtx_r")
dataloader.AddVariable("secVtx_z")
dataloader.AddVariable("secVtx_pt")
dataloader.AddVariable("secVtx_eta")
dataloader.AddVariable("secVtx_phi")
dataloader.AddVariable("secVtx_m")
dataloader.AddVariable("secVtx_m_nonAssoc")
dataloader.AddVariable("secVtx_minOneTrackRemovedMass")
dataloader.AddVariable("secVtx_ntrk")
dataloader.AddVariable("secVtx_ntrk_lrt")
dataloader.AddVariable("secVtx_ntrk_sel")
dataloader.AddVariable("secVtx_ntrk_assoc")
dataloader.AddVariable("secVtx_chi2")
dataloader.AddVariable("secVtx_minOpAng")
dataloader.AddVariable("secVtx_maxOpAng")
dataloader.AddVariable("secVtx_mind0")
dataloader.AddVariable("secVtx_maxd0")
dataloader.AddVariable("secVtx_maxDR")
dataloader.AddVariable("secVtx_maxDphi")
dataloader.AddVariable("secVtx_sumPt2")
dataloader.AddVariable("secVtx_H")
dataloader.AddVariable("secVtx_Ht")
dataloader.AddVariable("secVtx_charge")
dataloader.AddVariable("secVtx_nPixelHits")
dataloader.AddVariable("secVtx_nSCTHits")
dataloader.AddVariable("secVtx_nTRTHits")
dataloader.AddVariable("secVtx_nSiHits")

branchNames = [
  "secVtx_r",
  "secVtx_z",
  "secVtx_pt",
  "secVtx_eta",
  "secVtx_phi",
  "secVtx_m",
  "secVtx_m_nonAssoc",
  "secVtx_minOneTrackRemovedMass",
  "secVtx_ntrk",
  "secVtx_ntrk_lrt",
  "secVtx_ntrk_sel",
  "secVtx_ntrk_assoc",
  "secVtx_chi2",
  "secVtx_minOpAng",
  "secVtx_maxOpAng",
  "secVtx_mind0",
  "secVtx_maxd0",
  "secVtx_maxDR",
  "secVtx_maxDphi",
  "secVtx_sumPt2",
  "secVtx_H",
  "secVtx_Ht",
  "secVtx_charge",
  "secVtx_nPixelHits",
  "secVtx_nSCTHits",
  "secVtx_nTRTHits",
  "secVtx_nSiHits",
  "weight"
]

branches = {}
for branchName in branchNames:
    branches[branchName] = array('f', [-999])
    if branchName == "weight":
        continue
    reader.AddVariable(branchName, branches[branchName])
for tree in trees:
    for branchName in branchNames:
        tree.SetBranchAddress(branchName, branches[branchName])

# Book methods
reader.BookMVA('PyKeras', TString('VertexClassifier/weights/TMVAClassification_PyKeras.weights.xml'))

histos = [
  TH1F("h_ZJets","",100,0,1),
  TH1F("h_ma15_ctau100","",100,0,1),
  TH1F("h_ma15_ctau10","",100,0,1),
  TH1F("h_ma15_ctau1","",100,0,1),
  TH1F("h_ma55_ctau100","",100,0,1),
  TH1F("h_ma55_ctau10","",100,0,1),
  TH1F("h_ma55_ctau1","",100,0,1)
]

# Classify the training dataset and get the average response on the classes
for tree, hist in zip(trees,histos):
    for i in range(tree.GetEntries()):
        tree.GetEntry(i)
        val = reader.EvaluateMVA('PyKeras')
        hist.Fill(val,branches['weight'][0])
        #hist.Fill(val)

print(histos[0].GetMean())
print(histos[1].GetMean())
print(histos[2].GetMean())
print(histos[3].GetMean())
print(histos[4].GetMean())
print(histos[5].GetMean())
print(histos[6].GetMean())
"""
HistStack( hists = [histos[0],histos[5]],
        types = ["data",
                 "WpH_H125_a55a55_4b_ctau10"],
        legends = ["SHERPA Z+jets",
                   "m_{a}, c#tau = [55,10]"],
        name = "networkResponseStack",
        x_title = "Network response",
        x_units = "",
        x_min=0.5,
        y_max = 50000,
        y_min = 0,
        lumi_val = "80.5",
        hide_lumi = False,
        norm = True,
        rebin = 5,
        y_axis_type = "Vertices")

"""
Hist1D( hists = histos,
        types = ["data",
                 "WpH_H125_a15a15_4b_ctau100", 
                 "WpH_H125_a15a15_4b_ctau10",
                 "WpH_H125_a15a15_4b_ctau1",
                 "WpH_H125_a55a55_4b_ctau100",
                 "WpH_H125_a55a55_4b_ctau10",
                 "WpH_H125_a55a55_4b_ctau1"],
        legends = ["SHERPA Z+jets",
                   "m_{a}, c#tau = [15,100]",
                   "m_{a}, c#tau = [15,10]",
                   "m_{a}, c#tau = [15,1]",
                   "m_{a}, c#tau = [55,100]",
                   "m_{a}, c#tau = [55,10]",
                   "m_{a}, c#tau = [55,1]"],
        name = "networkResponse",
        x_title = "Network response",
        x_units = "",
        y_min = 0.00001,
        norm = False,
        log_scale_y = True,
        y_axis_type = "Vertices")
