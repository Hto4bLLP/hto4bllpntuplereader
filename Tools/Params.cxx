#include "Tools/Params.h"
#include "Tools/MCConfig.h"
#include "util/util.h"

#include <fstream>
#include <string>

Params::Params(const char *s, TString outFile, Bool_t isMC, Bool_t doTruth, Bool_t doSkim, Bool_t doPhotonSkim, Bool_t usePROOF, Bool_t unblind) : TNamed(s, s) {

  m_db = new MCConfig("mc_db","$SrcDir/hto4bllpntuplereader/data/xsec.tsv");
  m_histoMgr = new HistoMgr("histo_mgr");
  m_outFile = outFile;
  m_isMC = isMC;
  m_doTruth = doTruth;
  m_doSkim = doSkim;
  m_doPhotonSkim = doPhotonSkim;
  m_usePROOF = usePROOF;
  m_unblind = unblind;

}

MCConfig* Params::GetMCConfig()  { return m_db; }
HistoMgr* Params::GetHistoMgr()  { return m_histoMgr; }
TString Params::GetOutputFile()  { return m_outFile; }
Bool_t Params::GetIsMC()         { return m_isMC; }
Bool_t Params::GetDoTruth()      { return m_doTruth; }
Bool_t Params::GetDoSkim()       { return m_doSkim; }
Bool_t Params::GetDoPhotonSkim() { return m_doPhotonSkim; }
Bool_t Params::GetUsePROOF()  { return m_usePROOF; }
Bool_t Params::GetUnblind()      { return m_unblind; }
