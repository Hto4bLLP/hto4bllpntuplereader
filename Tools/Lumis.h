#ifndef LUMIS_H 
#define LUMIS_H 

namespace Lumis { // JB: only for testing purposes, need to update with most recent GRL
   const Double_t lumi15_16 = 3.21956+32.9653;//GoodRunsLists/data15_13TeV/20170619/data15_13TeV.periodAllYear_DetStatus-v89-pro21-02_Unknown_PHYS_StandardGRL_All_Good_25ns.xml + 
   const Double_t lumi17 = 44.3074;//GoodRunsLists/data17_13TeV/20180619/data17_13TeV.periodAllYear_DetStatus-v99-pro22-01_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml
   const Double_t lumi18 = 59.9372;//GoodRunsLists/data18_13TeV/20181111/data18_13TeV.periodAllYear_DetStatus-v102-pro22-04_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml
}

#endif /* LUMIS_H */
