#ifndef HISTOMGR_H 
#define HISTOMGR_H 

#include <TNamed.h>
#include <utility>      // std::pair
#include <TString.h>
#include <unordered_map>

class HistoMgr : public TNamed {
  public:

    HistoMgr() {};
    HistoMgr(const char *s);
    std::vector<std::pair<TString, std::vector<float>>> GetHists1D();
    std::vector<std::pair<TString, std::vector<float>>> GetHists2D();

  private:
    std::vector<std::pair<TString, std::vector<float>>> histParams1D;
    std::vector<std::pair<TString, std::vector<float>>> histParams2D;

   ClassDef(HistoMgr, 1);
};

#endif /* HISTOMGR_H */
