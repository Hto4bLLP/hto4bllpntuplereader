#ifndef VERTEX_H
#define VERTEX_H

#include <TLorentzVector.h>
#include <vector>
#include <TMath.h>
#include <TMatrixD.h>
#include <TVectorD.h>

class Vertex 
{

  public:
    std::string quality;
    TLorentzVector vec;

    Float_t x;
    Float_t y;
    Float_t z;
    Float_t Lxy;
    Int_t ntrk;
    Float_t chi2;
    Float_t pt;
    Float_t eta;
    Float_t phi;
    Float_t m;
    Float_t maxDR;
    Float_t jetDR;
    Int_t jetIndex;
    Float_t ZDR;
    Float_t PVClosestApproach;
    Bool_t pass_mat;
    Bool_t isMatched;
    Bool_t isLLPMatched;
    std::vector<float> covariance;

    void Set4Vec() {
      vec.SetPtEtaPhiM(pt,eta,phi,m);
    };

    Float_t DeltaR(Vertex v2) {
      return vec.DeltaR(v2.vec);
    };
    Float_t DeltaR(TLorentzVector v) {
      return vec.DeltaR(v);
    };
    Float_t mu() {
      return m/maxDR;
    };
    Float_t errLxy() {
        TMatrixD cov(3,3);
        TVector3 pos;

        pos[0] = x / sqrt(pow(x,2) + pow(y,2));
        pos[1] = y / sqrt(pow(x,2) + pow(y,2));
        pos[2] = 0;

        cov[0][0] = covariance.at(0);
        cov[1][0] = covariance.at(1);
        cov[0][1] = covariance.at(1);
        cov[1][1] = covariance.at(2);
        cov[2][0] = covariance.at(3);
        cov[0][2] = covariance.at(3);
        cov[2][1] = covariance.at(4);
        cov[1][2] = covariance.at(4);
        cov[2][2] = covariance.at(5);

        float errLxy = sqrt(pos.Dot(cov * pos));
        return errLxy;
    };
    Float_t LxySig() {
      return Lxy/errLxy();
    };
    Bool_t passQuality() {
      return (chi2 < 5.0);
    }
    Bool_t passFiducial() {
      return (Lxy < 300 && fabs(z) < 300);
    }
    Bool_t passMaterial() {
      return pass_mat;
    }
    Bool_t passPresel() {
      return (passQuality() && passMaterial() && passFiducial() && (LxySig() > 50) && (jetDR<0.6)); 
    };
    Bool_t passSignalIncl() { 
      return (passPresel() && (ntrk >= 3) && (mu() > 1));
    }
    Bool_t passSignalHigh() {
      return (passPresel() && (ntrk >= 3) && (mu() > 3));
    };
    Bool_t passSignalLow() {
      return (passPresel() && (ntrk >= 3) && (mu() > 1) && (mu() < 3));
    };
    

};


#endif /* VERTEX_H */
