#ifndef PARAMS_H 
#define PARAMS_H 

#include <TNamed.h>
#include <TString.h>
#include <string>
#include <unordered_map>
#include "Tools/MCConfig.h"
#include "Tools/HistoMgr.h"

class Params : public TNamed {
  private:

    HistoMgr* m_histoMgr;
    MCConfig* m_db;
    TString m_outFile;
    Bool_t m_isMC;
    Bool_t m_doTruth;
    Bool_t m_doSkim;
    Bool_t m_doPhotonSkim;
    Bool_t m_unblind;
    Bool_t m_usePROOF;
    ClassDef(Params, 1);

  public:

    Params() {};
    Params(const char* s, TString outFile, Bool_t isMC, Bool_t doTruth, Bool_t doSkim, Bool_t doPhotonSkim, Bool_t usePROOF, Bool_t unblind);
    MCConfig* GetMCConfig(); 
    HistoMgr* GetHistoMgr(); 
    TString GetOutputFile(); 
    Bool_t GetIsMC(); 
    Bool_t GetDoTruth(); 
    Bool_t GetDoSkim(); 
    Bool_t GetDoPhotonSkim(); 
    Bool_t GetUsePROOF(); 
    Bool_t GetUnblind(); 
};

#endif /* PARAMS_H */
