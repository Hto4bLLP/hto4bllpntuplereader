#include "Tools/MCConfig.h"
#include "util/util.h"
#include <wordexp.h>

#include <fstream>
#include <string>

MCConfig::MCConfig(const char *s, const char *filename) : TNamed(s, s) {

    wordexp_t wexp{};
    int code = wordexp(filename, &wexp, WRDE_SHOWERR | WRDE_UNDEF | WRDE_NOCMD);
    if (code) {
        switch (code) {
        case WRDE_BADCHAR:
            util::MSG("MCConfig", "ERROR","Invalid character in MC config file name");
            break;
        case WRDE_BADVAL:
            util::MSG("MCConfig", "ERROR","Undefined variable in MC config file name");
            break;
        case WRDE_CMDSUB:
            util::MSG("MCConfig", "ERROR","Command substitution in MC config file name");
            break;
        case WRDE_NOSPACE:
            util::MSG("MCConfig", "ERROR","No space");
            break;
        case WRDE_SYNTAX:
            util::MSG("MCConfig", "ERROR","Syntax error in MC config file name");
            break;
        }
        exit(2);
    }

    if (wexp.we_wordc != 1) {
        util::MSG("MCConfig", "ERROR","There must be exactly one MC config filename");
        exit(2);
    }

    std::ifstream in{wexp.we_wordv[0]};
    int dsid = 0;
    while (in >> dsid) {
        auto&& config_pair_result = db.try_emplace(dsid);
        auto&& config_pair = config_pair_result.first;
        bool inserted = config_pair_result.second;
        if (!inserted) {
            util::MSG("MCConfig", "WARNING", "Duplicate entry found for DSID " + std::to_string(dsid) + " when reading MC config");
        }
        Config& config = (*config_pair).second;
        in >> config.physics_short >> config.xsec >> config.gen_filt_eff >> config.k_factor
              >> config.rel_uncert_up >> config.rel_uncert_down >> config.generator;
        // DB has cross-sections in pb, not fb
        config.xsec *= 1000;

    }
}

MCConfig::Config& MCConfig::operator[](int dsid) {

    auto config = db.find(dsid);
    if (config == db.end()) {
        util::MSG("MCConfig", "ERROR", "Entry for DSID " + std::to_string(dsid) + " not found in XSec DB" );
        exit(2);
    }
    return config->second;
}

