#ifndef MCCONFIG_H 
#define MCCONFIG_H 

#include <TNamed.h>
#include <string>
#include <unordered_map>

class MCConfig : public TNamed {
  public:

    struct Config {
        std::string physics_short;
        double xsec;
        double gen_filt_eff;
        double k_factor;
        double rel_uncert_up;
        double rel_uncert_down;
        std::string generator;
        int generated_raw;
        int generated_weighted;
    };

    MCConfig() {};
    MCConfig(const char* s, const char* fileName );
    Config& operator[](int dsid);


  private:
    std::unordered_map<int, Config> db;

   ClassDef(MCConfig, 1);
};

#endif /* MGCONFIG_H */
