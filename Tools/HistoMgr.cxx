#include "Tools/HistoMgr.h"

HistoMgr::HistoMgr(const char *s) : TNamed(s, s){

  std::vector<TString> selections{"","_presel","_material","_signal","_tracks","_mass"};
  std::vector<TString> qualities{"Loose","Medium","Tight"};

  for(const auto quality : qualities) {

    for(const auto sel : selections) {
      histParams1D.push_back(std::make_pair("secVtx"+quality+"_pt"+sel,std::vector<float>{100,0,100}));
      histParams1D.push_back(std::make_pair("secVtx"+quality+"_eta"+sel,std::vector<float>{100,-5,5}));
      histParams1D.push_back(std::make_pair("secVtx"+quality+"_phi"+sel,std::vector<float>{128,-3.2,3.2}));
      histParams1D.push_back(std::make_pair("secVtx"+quality+"_m"+sel,std::vector<float>{100,0,50}));
      histParams1D.push_back(std::make_pair("secVtx"+quality+"_maxDR"+sel,std::vector<float>{100,0,5}));
      histParams1D.push_back(std::make_pair("secVtx"+quality+"_jetDR"+sel,std::vector<float>{100,0,5}));
      histParams1D.push_back(std::make_pair("secVtx"+quality+"_chi2"+sel,std::vector<float>{100,0,10}));
      histParams1D.push_back(std::make_pair("secVtx"+quality+"_mu"+sel,std::vector<float>{100,0,50}));
      histParams1D.push_back(std::make_pair("secVtx"+quality+"_Lxy"+sel,std::vector<float>{300,0,300}));
      histParams1D.push_back(std::make_pair("secVtx"+quality+"_errLxy"+sel,std::vector<float>{100,0,1}));
      histParams1D.push_back(std::make_pair("secVtx"+quality+"_LxySig"+sel,std::vector<float>{100,0,1000}));
      histParams1D.push_back(std::make_pair("secVtx"+quality+"_ntrk"+sel,std::vector<float>{20,0,20}));
    }
  }

  histParams1D.push_back(std::make_pair("eventCount",std::vector<float>{1,0,1}));
  histParams1D.push_back(std::make_pair("Z_pt",std::vector<float>{500,0,500}));
  histParams1D.push_back(std::make_pair("Z_eta",std::vector<float>{100,-5,5}));
  histParams1D.push_back(std::make_pair("Z_phi",std::vector<float>{128,-3.2,3.2}));
  histParams1D.push_back(std::make_pair("Z_m",std::vector<float>{100,50,150}));
  histParams1D.push_back(std::make_pair("Z_pt_imbalance",std::vector<float>{100,0,1}));
  histParams1D.push_back(std::make_pair("Z_jj_dPhi",std::vector<float>{160,0,3.2}));
  histParams1D.push_back(std::make_pair("l1_pt",std::vector<float>{500,0,500}));
  histParams1D.push_back(std::make_pair("l1_eta",std::vector<float>{100,-5,5}));
  histParams1D.push_back(std::make_pair("l1_phi",std::vector<float>{128,-3.2,3.2}));
  histParams1D.push_back(std::make_pair("l2_pt",std::vector<float>{500,0,500}));
  histParams1D.push_back(std::make_pair("l2_eta",std::vector<float>{100,-5,5}));
  histParams1D.push_back(std::make_pair("l2_phi",std::vector<float>{128,-3.2,3.2}));
  histParams1D.push_back(std::make_pair("j1_pt",std::vector<float>{500,0,500}));
  histParams1D.push_back(std::make_pair("j2_pt",std::vector<float>{500,0,500}));
  histParams1D.push_back(std::make_pair("jj_pt",std::vector<float>{500,0,500}));
  histParams1D.push_back(std::make_pair("mjj",std::vector<float>{500,0,500}));
  histParams1D.push_back(std::make_pair("j_minVtxDR",std::vector<float>{500,0,5}));
  histParams1D.push_back(std::make_pair("j_DL1",std::vector<float>{300,-15,15}));
  histParams1D.push_back(std::make_pair("sameVertex",std::vector<float>{2,0,2}));

  histParams2D.push_back(std::make_pair("ABCDMediummu",std::vector<float>{100,0,100,100,0,100}));
  histParams2D.push_back(std::make_pair("ABCDMediummuntrk",std::vector<float>{400,0,400,400,0,400}));
  histParams2D.push_back(std::make_pair("ABCDLoosemu",std::vector<float>{100,0,100,100,0,100}));
  histParams2D.push_back(std::make_pair("ABCDLoosemuntrk",std::vector<float>{400,0,400,400,0,400}));

  histParams2D.push_back(std::make_pair("ABCDLowPTMediummu",std::vector<float>{100,0,100,100,0,100}));
  histParams2D.push_back(std::make_pair("ABCDLowPTMediummuntrk",std::vector<float>{400,0,400,400,0,400}));
  histParams2D.push_back(std::make_pair("ABCDLowPTLoosemu",std::vector<float>{100,0,100,100,0,100}));
  histParams2D.push_back(std::make_pair("ABCDLowPTLoosemuntrk",std::vector<float>{400,0,400,400,0,400}));

  histParams2D.push_back(std::make_pair("ABCDHighPTMediummu",std::vector<float>{100,0,100,100,0,100}));
  histParams2D.push_back(std::make_pair("ABCDHighPTMediummuntrk",std::vector<float>{400,0,400,400,0,400}));
  histParams2D.push_back(std::make_pair("ABCDHighPTLoosemu",std::vector<float>{100,0,100,100,0,100}));
  histParams2D.push_back(std::make_pair("ABCDHighPTLoosemuntrk",std::vector<float>{400,0,400,400,0,400}));

}

std::vector<std::pair<TString, std::vector<float>>> HistoMgr::GetHists1D() {
  return histParams1D;
}
std::vector<std::pair<TString, std::vector<float>>> HistoMgr::GetHists2D() {
  return histParams2D;
}
