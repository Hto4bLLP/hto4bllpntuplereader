FROM atlas/analysisbase:21.2.90
USER root
ADD . /work/VH4b/src/hto4bllpntuplereader
WORKDIR /work/VH4b/build
RUN source /release_setup.sh && \
    cd /work/VH4b/src/hto4bllpntuplereader && \
    sudo cp util/CMakeLists.txt.TopLevel /work/VH4b/src/CMakeLists.txt && \
    sudo chown -R atlas /work/VH4b && \
    cd /work/VH4b/build && \
    cmake ../src && \
    make -j4 && \
    export SrcDir=/work/VH4b/src/ && \
    source x86*opt/setup.sh
    
