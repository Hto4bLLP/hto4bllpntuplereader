#include "Tools/MCConfig.h"
#include "Tools/Params.h"
#include "Tools/HistoMgr.h"
#include "Tools/Vertex.h"

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#endif

#ifdef __CINT__
#pragma link C++ class MCConfig+;
#pragma link C++ class Params+;
#pragma link C++ class HistoMgr+;
#pragma link C++ class Vertex+;
#endif

