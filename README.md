# Hto4bLLPNtupleReader

This package contains code for processing ntuples produced using the `Hto4bLLPAlgorithm` package. The workflow consists of two steps:

1) Produce mini-ntuples using the `NtupleReader` class (C++)
2) Produce histograms using `TTree::Draw()` and the histogram classes derived from `PlotBase` (python)

## !!! IMPORTANT !!!

This package assumes that the lepton preselection has already been applied by xAODAnaHelpers during the ntuple making step. Please ensure that the appropriate cuts have been applied when producing the ntuples. 

## Getting Started

### First-time Setup and Installation

Make sure you have a directory structure that works for a modern CMake setup.

```
mkdir WorkArea; cd WorkArea; 
mkdir build; 
mkdir src; 
mkdir run; 
cd src;
setupATLAS;
asetup AnalysisBase,21.2.90,here;
```

Get `NtupleReader` with:

```
lsetup git; 
git clone --recursive https://:@gitlab.cern.ch:8443/Hto4bLLP/hto4bllpntuplereader.git; 
```
And compile

```
cd ../../../../build/; 
cmake ../src/;
make; 
```

Then make sure you setup the new environment:

```
source x*-*gcc*-opt/setup.sh  # (wildcards since os and gcc versions may differ)
export SrcDir=$TestArea # (needed because the grid doesn't like the TestArea variable)
```

### Running

Go to your run directory

```
cd ../run/
run_NtupleReader --output miniNtuple.root --input_file files.txt
```

The code can also run on the grid using the Docker container that is build every push. 
An example run is
```
prun --containerImage docker://gitlab-registry.cern.ch/hto4bllp/hto4bllpntuplereader \
--exec "echo %IN | sed 's/\,/\n/g' > input.txt; pwd; ls; ls ../VH4b; source /release_setup.sh; source ../VH4b/build/x86*/setup.sh; export SrcDir=../VH4b/src; run_NtupleReader --input_files input.txt --output miniNtuple.root" \
--outputs miniNtuple.root \
--inDS user.jburzyns.data18_13TeV.00349637.physics_Main.NTUP.v1_tree.root \
--outDS user.jburzyns.data18_13TeV.00349637.physics_Main.MININTUP.v1
```
